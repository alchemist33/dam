/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package saludoromano;

import java.util.Random;

/**
 *
 * @author marco
 */
public class Romano extends Thread{
    
    String nombre;
    Saludo saludo;
    boolean Cesar;

    public Romano(String nombre, Saludo saludo, boolean Cesar) {
        this.nombre = nombre;
        this.saludo = saludo;
        this.Cesar = Cesar;
    }

    @Override
    public void run() {
        System.out.println(nombre + " disponible");
        Random rand = new Random();
        try {
            Thread.sleep(rand.nextInt(1000));
        } catch (InterruptedException e) {
            System.err.println("ERROR");
        }
        if (Cesar) {
            saludo.saludoCesar();
        }else {
            saludo.saludoRomano(nombre);
        }
       
    }

 
    
    
}
