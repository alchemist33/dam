/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import java.beans.;
import java.io.Serializable;

/**
 *
 * @author marco
 */
public class Producto implements Serializable {

    public static final String PROP_SAMPLE_PROPERTY = "sampleProperty";

    private String descripcion;
    private int idproducto;
    private int stockactual;
    private int stockminimo;
    private float pvp;

    private PropertyChangeSupport propertySupport;

    public Producto() {
        propertySupport = new PropertyChangeSupport(this);
    }

    public Producto(int idproducto,String descripcion, int stockactual, int stockminimo, float pvp, PropertyChangeSupport propertySupport) {
        this.descripcion = descripcion;
        this.idproducto = idproducto;
        this.stockactual = stockactual;
        this.stockminimo = stockminimo;
        this.pvp = pvp;
        this.propertySupport = propertySupport;
    }
        
   public Producto(int idproducto,String descripcion, int stockactual, int stockminimo, float pvp) {
        this.descripcion = descripcion;
        this.idproducto = idproducto;
        this.stockactual = stockactual;
        this.stockminimo = stockminimo;
        this.pvp = pvp;

    }
        
   


    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getIdproducto() {
        return idproducto;
    }

    public void setIdproducto(int idproducto) {
        this.idproducto = idproducto;
    }

    public int getStockactual() {
        return stockactual;
    }

    public void setStockactual(int valorNuevo) {
        int valorAnterior = this.stockactual;
        this.stockactual = valorNuevo;

        if (this.stockactual < getStockminimo()) {
            propertySupport.firePropertyChange("stockactual", valorAnterior, this.stockactual);
            this.stockactual = valorAnterior;
        }
    }

    public int getStockminimo() {
        return stockminimo;
    }

    public void setStockminimo(int stockminimo) {
        this.stockminimo = stockminimo;
    }

    public float getPvp() {
        return pvp;
    }

    public void setPvp(float pvp) {
        this.pvp = pvp;
    }

    public PropertyChangeSupport getPropertySupport() {
        return propertySupport;
    }

    public void setPropertySupport(PropertyChangeSupport propertySupport) {
        this.propertySupport = propertySupport;
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        propertySupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        propertySupport.removePropertyChangeListener(listener);
    }

}
