/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package accesooid;

import java.math.BigDecimal;
import java.math.BigInteger;
import org.neodatis.odb.ODB;
import org.neodatis.odb.ODBServer;
import org.neodatis.odb.ODBFactory;
import org.neodatis.odb.OID;
import org.neodatis.odb.ObjectValues;
import org.neodatis.odb.Objects;
import org.neodatis.odb.Values;
import org.neodatis.odb.core.oid.OIDFactory;
import org.neodatis.odb.core.query.IQuery;
import org.neodatis.odb.core.query.criteria.ICriterion;
import org.neodatis.odb.core.query.criteria.Where;
import org.neodatis.odb.core.query.criteria.And;
import org.neodatis.odb.core.query.criteria.Or;
import org.neodatis.odb.core.query.criteria.Not;
import org.neodatis.odb.impl.core.query.criteria.CriteriaQuery;
import org.neodatis.odb.impl.core.query.values.ValuesCriteriaQuery;

/**
 *
 * @author marco
 */
public class AccesoOID {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        //insertaObejtos();
        //muestraObjeto();
//        getObjeto();
        //updateObjeto();
//        borraObjeto();
//        listaObjetosCriterio();
//        listaObjetosCriterio2();
        // listaObjetosCriterio3();
        //listaObjetosCriterio4();
        //listaObjetosCriterio5();
        //listaObjetosCriterioSuma();
        //listaObjetosCriterioCount();
        //listaObjetosCriterioMedia();
        //listaObjetosCriterioMaxMin();
        //listaObjetosCriterioMediaError();
        //listaObjetosCriterioGroup();
        //listaObjetosCriterioRelaciones();
        //listaObjetosCriterioRelaciones2();
        //listaObjetosCriterioRelaciones3();
        NeoServer();

    }
    public static final ODB ODB = ODBFactory.open("C:\\NeoDatis ODB/neodatis.test");

    public static void insertaObejtos() {

        Jugadores jugador1 = new Jugadores("Marcos", "golf", "Zaragoza", 14);
        Jugadores jugador2 = new Jugadores("Teresa", "balonmano", "Pamplona", 14);
        Jugadores jugador3 = new Jugadores("Mario", "futbol", "Tenerife", 15);
        Jugadores jugador4 = new Jugadores("Patricia", "natacion", "Madrid", 14);
        Paises pais1 = new Paises(1, "España");
        Paises pais2 = new Paises(1, "Cuba");
        ODB.store(jugador1);
        ODB.store(jugador2);
        ODB.store(jugador3);
        ODB.store(jugador4);
        ODB.store(pais1);
        ODB.store(pais2);

        ODB.close();
    }

    public static void muestraObjeto() {

        OID oid = OIDFactory.buildObjectOID(2);
        Jugadores jug = (Jugadores) ODB.getObjectFromId(oid);
        System.out.printf("%s, %s, %s, %d %n", jug.getNombre(), jug.getDeporte(), jug.getCiudad(), jug.getEdad());
        ODB.close();
    }

    public static void getObjeto() {

        IQuery query = new CriteriaQuery(Jugadores.class, Where.equal("deporte", "golf"));
        query.orderByAsc("nombre,edad");
        Objects<Jugadores> lista = ODB.getObjects(query);
        Jugadores jugador1a = (Jugadores) ODB.getObjects(query).getFirst();
        ODB.close();
    }

    public static void updateObjeto() {
        Paises pais = new Paises();

        IQuery query = new CriteriaQuery(Jugadores.class, Where.equal("nombre", "Patricia"));
        Jugadores jugador2a = (Jugadores) ODB.getObjects(query).getFirst();
        jugador2a.setDeporte("patinaje");
        jugador2a.setPais(pais);
        ODB.store(jugador2a);
        ODB.commit();
        ODB.close();
    }

    public static void borraObjeto() {

        IQuery query = new CriteriaQuery(Jugadores.class, Where.equal("nombre", "Patricia"));
        Jugadores jugador2a = (Jugadores) ODB.getObjects(query).getFirst();
        ODB.delete(jugador2a);
        ODB.close();
    }

    public static void listaObjetosCriterio() {

        try {

            ICriterion criterio = Where.equal("edad", 14);
            //ICriterion criterio = Where.gt("edad", 14);  //para mayor 14
            //ICriterion criterio = Where.ge("edad", 14); //para mayor o igual que 14
            //ICriterion criterio = Where.lt("edad", 14); //para menor 14
            //ICriterion criterio = Where.le("edad", 14); //para menor o igual que 14
            //ICriterion criterio = Where.contain("nombrearray", valor); // Para comprobar si un array tiene un valor determinado
            //ICriterion criterio = Where.isNull("atributo"); // para comprobar si un atributo es nulo
            //ICriterion criterio = Where.isNotNull("atributo"); // para comprobar si un atributo no es nulo
            CriteriaQuery query3 = new CriteriaQuery(Jugadores.class, criterio);
            Objects<Jugadores> lista2 = ODB.getObjects(query3);
            System.out.println("-------------------------");
            while (lista2.hasNext()) {
                Jugadores jugador = lista2.next();

                System.out.printf("%s, %s, %s, %d %n", jugador.getNombre(), jugador.getDeporte(), jugador.getCiudad(), jugador.getEdad());
            }

        } catch (IndexOutOfBoundsException InOutBnEx) {
            System.err.println("No se ha encontrado el jugador");
            System.err.println(InOutBnEx.getMessage());
        }

        ODB.close();
    }

    public static void listaObjetosCriterio2() {

        try {

            ICriterion criterio = new And().add(Where.equal("ciudad", "Madrid")).add(Where.equal("edad", 15));
            ICriterion criterio2 = new Or().add(Where.equal("ciudad", "Madrid")).add(Where.ge("edad", 15));

            ICriterion criterio3 = Where.not(Where.like("nombre", "M%"));

            ICriterion criterio4 = Where.like("nombre", "M%");
            ICriterion criterio5 = Where.not(criterio4);

            //-----------------------------------------------//
            CriteriaQuery query = new CriteriaQuery(Jugadores.class, criterio);
            Objects<Jugadores> lista = ODB.getObjects(query);

            while (lista.hasNext()) {
                Jugadores jugador = lista.next();

                System.out.printf("%s, %s, %s, %d %n", jugador.getNombre(), jugador.getDeporte(), jugador.getCiudad(), jugador.getEdad());
            }

        } catch (IndexOutOfBoundsException InOutBnEx) {

            System.err.println(InOutBnEx.getMessage());
        }

        ODB.close();
    }

    public static void listaObjetosCriterio3() {

        try {

            ICriterion criterio = new And().add(Where.equal("edad", 14)).
                    add(new Or().
                            add(Where.equal("ciudad", "Madrid"))
                            .add(Where.equal("ciudad", "Zaragoza"))
                            .add(Where.equal("ciudad", "Pamplona")));

            IQuery query = new CriteriaQuery(Jugadores.class, criterio);
            Objects<Jugadores> lista = ODB.getObjects(query);
            if (lista.size() == 0) {
                System.out.println("No se ha econtrado ningun jugador");
            } else {
                Jugadores jug;
                System.out.println("Jugadores de 14 años de Madrid, Zaragoza y Pamplona");
                while (lista.hasNext()) {
                    jug = (Jugadores) lista.next();

                    System.out.printf("Nombre: %s, Edad: %d, Ciudad: %s %n", jug.getNombre(), jug.getEdad(), jug.getCiudad());
                }

            }

        } catch (IndexOutOfBoundsException InOutBnEx) {

            System.err.println(InOutBnEx.getMessage());
        }

        ODB.close();
    }

    private static void listaObjetosCriterio4() {
        Values valores = ODB.getValues(new ValuesCriteriaQuery(Jugadores.class).field("nombre").field("ciudad"));
        while (valores.hasNext()) {
            ObjectValues objectValues = (ObjectValues) valores.next();
            System.out.printf("%s, Ciudad: %s %n", objectValues.getByAlias("nombre"), objectValues.getByIndex(1));

        }
    }

    private static void listaObjetosCriterio5() {
        Values valores = ODB.getValues(new ValuesCriteriaQuery(Jugadores.class).field("nombre", "alias").field("ciudad"));
        while (valores.hasNext()) {
            ObjectValues objectValues = (ObjectValues) valores.next();
            System.out.printf("%s, Ciudad: %s %n", objectValues.getByAlias("alias"), objectValues.getByIndex(1));

        }
        ODB.close();
    }

    public static void listaObjetosCriterioSuma() {
        Values val = ODB.getValues(new ValuesCriteriaQuery(Jugadores.class).sum("edad"));
        ObjectValues ov = val.nextValues();
        BigDecimal value = (BigDecimal) ov.getByAlias("edad");

        System.out.printf("Suma de edades de los jugadores: %d %n", value.longValue());
        System.out.println("-------------------------------------------------------------");
        System.out.printf("Suma de edades de los jugadores: %.2f %n", ov.getByAlias("edad"));
        ODB.close();
    }

    private static void listaObjetosCriterioCount() {

        Values val = ODB.getValues(new ValuesCriteriaQuery(Jugadores.class).count("nombre"));

        ObjectValues ov = val.nextValues();
        BigInteger value = (BigInteger) ov.getByAlias("nombre");
        System.out.printf("Numero de jugadores: %d %n", value.intValue());

        ODB.close();
    }

    private static void listaObjetosCriterioMedia() {

        Values val = ODB.getValues(new ValuesCriteriaQuery(Jugadores.class).avg("edad"));

        ObjectValues ov = val.nextValues();
        BigDecimal value = (BigDecimal) ov.getByAlias("edad");
        System.out.printf("Edad Media de jugadores: %.2f %n", value.floatValue());

        ODB.close();
    }

    private static void listaObjetosCriterioMaxMin() {

        Values val = ODB.getValues(new ValuesCriteriaQuery(Jugadores.class).max("edad", "edad_max"));
        Values val2 = ODB.getValues(new ValuesCriteriaQuery(Jugadores.class).min("edad", "edad_min"));

        ObjectValues ov = val.nextValues();
        ObjectValues ov2 = val2.nextValues();
        BigDecimal max = (BigDecimal) ov.getByAlias("edad_max");
        BigDecimal min = (BigDecimal) ov2.getByAlias("edad_min");
        System.out.printf("Edad maxima: %d, Edad minima: %d %n", max.intValue(), min.intValue());

        ODB.close();
    }

    private static void listaObjetosCriterioMediaError() {
        try {
            Values val = ODB.getValues(new ValuesCriteriaQuery(Jugadores.class).avg("edad"));
            ObjectValues ov = val.nextValues();
            System.out.printf("AVG: La media de ead es : %f %n", ov.getByIndex(0));

        } catch (ArithmeticException ArhEx) {

            System.out.println(ArhEx.getMessage());

            Values val2 = ODB.getValues(new ValuesCriteriaQuery(Jugadores.class).sum("edad").count("edad"));
            ObjectValues ov2 = val2.nextValues();
            float media;
            BigDecimal sumaEdad = (BigDecimal) ov2.getByIndex(0);
            BigInteger cuenta = (BigInteger) ov2.getByIndex(1);
            media = sumaEdad.floatValue() / cuenta.floatValue();

            System.out.printf("La media de ead es : %.2f Contador = %d suma = %.2f %n", media, cuenta, sumaEdad);
        }

        ODB.close();
    }

    private static void listaObjetosCriterioGroup() {

        Values groupby = ODB.getValues(new ValuesCriteriaQuery(Jugadores.class).field("ciudad").count("edad").groupBy("ciudad"));

        while (groupby.hasNext()) {

            ObjectValues objetos = (ObjectValues) groupby.next();
            System.out.printf("%s, %d %n", objetos.getByAlias("ciudad"), objetos.getByIndex(1));
        }

    }

    private static void listaObjetosCriterioRelaciones() {

        Values valores = ODB.getValues(new ValuesCriteriaQuery(Jugadores.class).field("nombre").field("edad").field("pais.nombrepais"));

        while (valores.hasNext()) {

            ObjectValues objectsValues = (ObjectValues) valores.next();
            System.out.printf("Nombre: %s, Edad: %d, Pais: %s %n", objectsValues.getByAlias("nombre"), objectsValues.getByIndex(1), objectsValues.getByIndex(2));
        }

    }

    private static void listaObjetosCriterioRelaciones2() {

        Values valores2 = ODB.getValues(new ValuesCriteriaQuery(Jugadores.class,
                new And().add(Where.equal("pais.nombrepais", "ESPAÑA")).add(Where.equal("edad", 15))).field("nombre").field("ciudad"));

        while (valores2.hasNext()) {

            ObjectValues objectsValues = (ObjectValues) valores2.next();
            System.out.printf("Nombre: %s, Ciudad: %s, %n", objectsValues.getByAlias("nombre"), objectsValues.getByIndex(1));
        }
    }

    private static void listaObjetosCriterioRelaciones3() {

        Values groupby = ODB.getValues(new ValuesCriteriaQuery(Jugadores.class,
                Where.isNotNull("pais.nombrepais")).
                field("pais.nombrepais").
                count("nombre").
                max("edad").
                sum("edad").
                groupBy("pais.nombrepais"));

        if (groupby.size() == 0) {
            System.out.println("La consulta no devuelve datos");
        } else {
            while (groupby.hasNext()) {

                ObjectValues objetos = (ObjectValues) groupby.next();
                float media = ((BigInteger) objetos.getByIndex(1)).floatValue();
                System.out.printf("Pais: %-8s Num jugadores: %d, Edad Máxima: %.0f, Suma de Edad: %.0f, Edad media: %.2f %n",
                        objetos.getByAlias("pais.nombrepais"), objetos.getByIndex(1), objetos.getByIndex(2), objetos.getByIndex(3), media);
            }
        }

    }
    
    private static void NeoServer(){
       ODBServer server = (ODBServer) ODBFactory.openServer(8000);
        server.addBase("neo1","C:\\NeoDatis ODB/neodatisSrv.test" );
         server.openClient("neo1");
   
        server.startServer(true);
        
       
    }
    
    
    private static void NeoClient() {
         ODBServer client = (ODBServer) ODBFactory.openClient("localhost", 8000, "neo1");
   
    }
}
