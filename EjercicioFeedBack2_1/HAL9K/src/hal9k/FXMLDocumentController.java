/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hal9k;

//import java.awt.Color;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.Background;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.BorderWidths;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.stage.Modality;
import javafx.stage.Popup;
import javafx.stage.Stage;

import javax.swing.BorderFactory;

/**
 *
 * @author marco
 */
public class FXMLDocumentController implements Initializable {
// <editor-fold defaultstate="collapsed" desc="Referencias FXML">

    @FXML
    private TextField nombre_producto;
    @FXML
    private ComboBox categoria;
    @FXML
    private DatePicker fechaAdqui;
    @FXML
    private TextField codProv;
    @FXML
    private TextField iva;
    @FXML
    private TextField pvp;
    @FXML
    private RadioButton viaCompra1;
    @FXML
    private RadioButton viaCompra2;
    @FXML
    private TextField uDispo;
    @FXML
    private TextArea descripcion;

// </editor-fold>  
    @FXML
    private void agregarProducto(ActionEvent event) {

        if (validaDatos()) {
            popUp("Su producto se ha agregado correctamente");
       

        } else {
             popUp("Hay campos pendientes de rellenar");
      

        }

    }

    @FXML
    private void cancelar(ActionEvent event) {
        System.exit(0);

    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    public boolean validaDatos() {
        boolean dataOk = false;
        int fallos = 0;
        Border errBorder = (new Border(new BorderStroke(Color.RED, BorderStrokeStyle.SOLID, new CornerRadii(3), new BorderWidths(1))));
        Border blueBorder = (new Border(new BorderStroke(Color.BLUE, BorderStrokeStyle.SOLID, new CornerRadii(3), new BorderWidths(0.5))));
       
        try {
            if (nombre_producto.getText().isEmpty()) {
                nombre_producto.setBorder(errBorder);
                fallos++;
            } else {
                nombre_producto.setBorder(blueBorder);
            }
            if (categoria.getValue() == null) {
                categoria.setBorder(errBorder);
                fallos++;
            } else {
                 categoria.setBorder(blueBorder);
            }

            if (fechaAdqui.getValue() == null) {
                fechaAdqui.setBorder(errBorder);
                fallos++;
            } else {
                fechaAdqui.setBorder(blueBorder);
            }
            if (codProv.getText().isEmpty()) {
                codProv.setBorder(errBorder);
                fallos++;
            }else {
                codProv.setBorder(blueBorder);
            }
            if (iva.getText().isEmpty()) {
                iva.setBorder(errBorder);
                fallos++;
            } else {
                iva.setBorder(blueBorder);
            }
            if (pvp.getText().isEmpty()) {
                pvp.setBorder(errBorder);
                fallos++;
            } else {
                 pvp.setBorder(blueBorder);
            }

            if (!viaCompra1.isSelected() && !viaCompra2.isSelected()) {
                viaCompra1.setBorder(errBorder);
                viaCompra2.setBorder(errBorder);
                fallos++;
            }else {
                 pvp.setBorder(blueBorder);
            }
            
            if (uDispo.getText().isEmpty()) {
                uDispo.setBorder(errBorder);
                fallos++;
            } else {
                 uDispo.setBorder(blueBorder);
            }
            if (descripcion.getText().isEmpty()) {
                descripcion.setBorder(errBorder);
                fallos++;

            } else {
                 uDispo.setBorder(blueBorder);
            }

            if (fallos > 0) {
                dataOk = false;
            } else {
                dataOk = true;
            }

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        return dataOk;
    }

    public void popUp(String mensaje ) {
        Stage popupwindow = new Stage();

        popupwindow.initModality(Modality.APPLICATION_MODAL);
        popupwindow.setTitle("HAL9K-message");

        Label label1 = new Label(mensaje);

        Button button1 = new Button("Cerrar");

        button1.setOnAction(e -> popupwindow.close());

        VBox layout = new VBox(10);

        layout.getChildren().addAll(label1, button1);

        layout.setAlignment(Pos.CENTER);

        Scene scene1 = new Scene(layout, 250, 100);

        popupwindow.setScene(scene1);

        popupwindow.showAndWait();

    }
}

