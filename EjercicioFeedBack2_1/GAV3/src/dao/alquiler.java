/**
 *  paquete de objetos dao para mapeos de obejtos Reservas
 */
package dao;

import java.util.Date;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Clase auxiliar para la genracion del fochero XML
 * Esta clase se encarga de cada elemento obtenido por medio del metodo getAllReservas para procesar 
 * cada una de sus propiedadesa y exponerlas en el XML segun el orden establecido en la referencia @XmlType(propOrder
 * @see Controller.GestorReservas#getAllReservas() 
 * @author Marco Memba aka "El aqluimista C-147"
 * @version 3.0
 * @since 2020-01-14
 */
@XmlRootElement(name = "reserva")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {"id_reserva", "fecha_inicio", "fecha_devolucion", "precio_total", "litros", "id_cliente", "id_coche"})
public class alquiler {

    private byte id_reserva;
    private Date fecha_inicio;
    private Date fecha_devolucion;
    private float precio_total;
    private float litros;
    private byte id_cliente;
    private byte id_coche;

    public alquiler() {
    }

    public alquiler(byte id_reserva, Date fecha_inicio, Date fecha_devolucion, float precio_total, float litros, byte id_cliente, byte id_coche) {
        this.id_reserva = id_reserva;
        this.fecha_inicio = fecha_inicio;
        this.fecha_devolucion = fecha_devolucion;
        this.precio_total = precio_total;
        this.litros = litros;
        this.id_cliente = id_cliente;
        this.id_coche = id_coche;
    }

    public byte getId_reserva() {
        return id_reserva;
    }

    public void setId_reserva(byte id_reserva) {
        this.id_reserva = id_reserva;
    }

    public Date getFecha_inicio() {
        return fecha_inicio;
    }

    public void setFecha_inicio(Date fecha_inicio) {
        this.fecha_inicio = fecha_inicio;
    }

    public Date getFecha_devolucion() {
        return fecha_devolucion;
    }

    public void setFecha_devolucion(Date fecha_devolucion) {
        this.fecha_devolucion = fecha_devolucion;
    }

    public float getPrecio_total() {
        return precio_total;
    }

    public void setPrecio_total(float precio_total) {
        this.precio_total = precio_total;
    }

    public float getLitros() {
        return litros;
    }

    public void setLitros(float litros) {
        this.litros = litros;
    }

    public byte getId_cliente() {
        return id_cliente;
    }

    public void setId_cliente(byte id_cliente) {
        this.id_cliente = id_cliente;
    }

    public byte getId_coche() {
        return id_coche;
    }

    public void setId_coche(byte id_coche) {
        this.id_coche = id_coche;
    }
}
