package dao;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Clase auxiliar para la genracion del fochero XML
 * 
 *
 * @author Marco Memba aka "El aqluimista C-147"
 * @version 3.0
 * @since 2020-01-14
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class alquileres {

    @XmlElementWrapper(name = "listaReservas")
    @XmlElement(name = "reserva")
    private List<alquiler> listaReservas;

    public List<alquiler> getListaReservas() {
        return listaReservas;
    }

    public void setListaReservas(List<alquiler> listaReservas) {
        this.listaReservas = listaReservas;
    }

}
