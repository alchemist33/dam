package feedback;
// Generated 06-ene-2020 13:08:06 by Hibernate Tools 4.3.1

import java.util.ArrayList;
import java.util.Date;


/**
 * Reservas generated by hbm2java
 *
 *
 */


public class Reservas implements java.io.Serializable {

      

    private byte idReserva;
    private Clientes clientes;
    private Coches coches;
    private int idCliente;
    private int idCoche;
    private Date fechaInicio;
    private Date fechaDevolucion;
    private Float precioTotal;
    private Float litros;

    public Reservas() {
    }

    public Reservas(Clientes clientes, Coches coches, int idCliente, int idCoche, Date fechaInicio, Date fechaDevolucion) {
        this.clientes = clientes;
        this.coches = coches;
        this.idCliente = idCliente;
        this.idCoche = idCoche;
        this.fechaInicio = fechaInicio;
        this.fechaDevolucion = fechaDevolucion;
    }

    public Reservas(Clientes clientes, Coches coches, int idCliente, int idCoche, Date fechaInicio, Date fechaDevolucion, Float precioTotal, Float litros) {
        this.clientes = clientes;
        this.coches = coches;
        this.idCliente = idCliente;
        this.idCoche = idCoche;
        this.fechaInicio = fechaInicio;
        this.fechaDevolucion = fechaDevolucion;
        this.precioTotal = precioTotal;
        this.litros = litros;
    }

    
    public byte getIdReserva() {
        return this.idReserva;
    }

    public void setIdReserva(byte idReserva) {
        this.idReserva = idReserva;
    }

    public Clientes getClientes() {
        return this.clientes;
    }

    public void setClientes(Clientes clientes) {
        this.clientes = clientes;
    }

    public Coches getCoches() {
        return this.coches;
    }

    public void setCoches(Coches coches) {
        this.coches = coches;
    }

    public int getIdCliente() {
        return this.idCliente;
    }

    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }

    public int getIdCoche() {
        return this.idCoche;
    }

    public void setIdCoche(int idCoche) {
        this.idCoche = idCoche;
    }

    public Date getFechaInicio() {
        return this.fechaInicio;
    }

    
    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaDevolucion() {
        return this.fechaDevolucion;
    }

    public void setFechaDevolucion(Date fechaDevolucion) {
        this.fechaDevolucion = fechaDevolucion;
    }

    public Float getPrecioTotal() {
        return this.precioTotal;
    }

    public void setPrecioTotal(Float precioTotal) {
        this.precioTotal = precioTotal;
    }

    public Float getLitros() {
        return this.litros;
    }

    public void setLitros(Float litros) {
        this.litros = litros;
    }

}
