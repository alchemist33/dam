/**
 * Feedback es el paquete encargada de mapear los objetos de la base de datos
 * Permite un trabajo sobre la base de datos rlacionas con objetos mediante una capa de abstracion
 * La clase Hibernete Util permite la creacion de sesiones para la obtencion de datos
 * 
 * @see feedback.HibernateUtil#buildSessionFactory()
 * 
 */

package feedback;
