package Controller;

import Controller.utils.GavUtils;
import feedback.Clientes;
import feedback.Coches;

import feedback.HibernateUtil;
import feedback.Reservas;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.HibernateException;
import org.hibernate.ObjectNotFoundException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.TransientPropertyValueException;
import org.hibernate.exception.ConstraintViolationException;

/**
 * Clase para la gestion de Clientes
 *
 *
 * @author Marco Memba aka "El aqluimista C-147"
 * @version 3.0
 * @since 2020-01-14
 */
public class GestorClientes {

    /**
     *
     */
    private static final GestorClientes GS_CLI = new GestorClientes();
    SessionFactory factory = HibernateUtil.getSessionFactory();
    private static final GavUtils GV_UT = new GavUtils();

    /**
     * Enumerado para selector de tipo de consulta Segun el valor escogido se
     * realiza una consulta por dicho criterio seleccionado en la tabla de
     * Clientes
     */
    public enum ConsultasClientes {

        NOMBRE,
        APELLIDOS

    }

    /**
     * Obtencion de un cliente concreto segun el numero de Id
     *
     * @param IdCli id del cliente que se obtendran los datos
     * @return retorna un objeto cliente completo
     */
    public Clientes getClienteById(byte IdCli) {
        Session sesion = factory.openSession();
        Clientes cli = new Clientes();

        String hql = "from Clientes where idCliente = :cliente";

        try {

            Query q = sesion.createQuery(hql);
            q.setParameter("cliente", (byte) IdCli);
            cli = (Clientes) q.uniqueResult();

        } catch (HibernateException Hbe) {
            System.out.println("Mensaje-Error Hibernete(cli-01):" + Hbe.getMessage());

        } catch (Exception ex) {
            System.out.println("Mensaje-Error (cli-01):" + ex.getMessage());
        }
        return cli;

    }

    /**
     * Metodo para la obtencion del ultmido id de cliente.
     *
     * @return devuevle el valor maximo encontrado en el campo IdCliente
     */
    public byte ultimoCliente() {

        byte ultimoCliente = 0;

        Session sesion = factory.openSession();
        String hql = "select max(cli.idCliente) from Clientes as cli";
        try {

            Query q = sesion.createQuery(hql);
            ultimoCliente = (byte) ((byte) q.uniqueResult() + 1);

        } catch (Exception e) {
            System.out.println("Mensaje-Error cliente:" + e.getMessage());

        }

        sesion.close();

        return ultimoCliente;
    }

    /**
     * Metodo para la inserccion de un cliente mediante el metodo save del
     * obejto sesion. El commit tiene sus posibles expeciones controladas y
     * dispone de un rollback enel bloque catch.
     *
     * @param cliente objeto cliente que se insertara en la base de datos
     */
    public void insertCliente(Clientes cliente) {

        Session sesion = factory.openSession();
        try {

            Transaction transaction = sesion.beginTransaction();

            System.out.printf("Insertamos una fila en la tabla Clientes...%n");

            try {
                sesion.save(cliente);
                transaction.commit();
                System.out.printf("Insert finalizado.%nCommit Succes!! %n");

            } catch (ConstraintViolationException CnEx) {
                System.out.println("Error: Realizando RollBack....");
                transaction.rollback();
                System.out.println("Error:  RollBack realizado con exito");
                System.out.println("Error: Cliente duplicado");
                System.out.println("Mensaje: " + CnEx.getMessage());
                System.out.println("Codigo de error: " + CnEx.getErrorCode());
                System.out.println("SQL: " + CnEx.getSQLException().getMessage());
                System.out.println("=========================================");
                //Logger.getLogger(GestorClientes.class.getName()).log(Level.SEVERE, null, CnEx);

            }
            sesion.close();
            //System.exit(0);
        } catch (TransientPropertyValueException Ex) {
            System.out.println("Error: El cliente no existe");
            //Logger.getLogger(ResultSetInfo.class.getName()).log(Level.SEVERE, null, Ex);
        }
    }

    /**
     * Metodo para borrar un cliente de la base de datos. El commit tiene sus
     * posibles expeciones controladas y dispone de un rollback enel bloque
     * catch.
     *
     * @param cliente objeto cliente que represenata el cliente a eliminar de la
     * base de datos
     */
    public void borraCliente(Clientes cliente) {

        Session sesion = factory.openSession();
        Transaction transaction = sesion.beginTransaction();
        try {

            cliente = getClienteById((byte) cliente.getIdCliente());

            if (cliente == null) {
                System.out.println("El cliente que desea eliminar no existe");
            } else {
                sesion.delete(cliente);
                transaction.commit();
                System.out.println("cliente eliminado");
            }

        } catch (ObjectNotFoundException ObEx) {
            transaction.rollback();
            System.out.println("No exite el cliente");
        } catch (ConstraintViolationException CnEx) {
            transaction.rollback();
            System.out.println("Error: No  se puede borrar, el cliente tiene reservas");
        } catch (Exception ex) {
            transaction.rollback();
            System.err.println("ERROR:");
            ex.printStackTrace();
        }
        sesion.close();

    }

    /**
     * Metodo para modificar un cliente de la base de datos. El commit tiene sus
     * posibles expeciones controladas y dispone de un rollback enel bloque
     * catch.
     *
     * @param cliente objeto cliente que represenata el cliente a modificar de
     * la base de datos
     */
    public void modificaCliente(Clientes cliente) {

        Session sesion = factory.openSession();
        Transaction tr = sesion.beginTransaction();

        String hql = "update Clientes set dni = :dni,"
                + "                       nombre = :nombre,"
                + "                       apellidos = :apellidos,"
                + "                       direccion = :direccion,"
                + "                       telefono = :telefono"
                + "                       where ID_CLIENTE = :id_cliente";
        try {
            Query q = sesion.createQuery(hql);
            q.setParameter("dni", cliente.getDni());
            q.setParameter("nombre", cliente.getNombre());
            q.setParameter("apellidos", cliente.getApellidos());
            q.setParameter("direccion", cliente.getDireccion());
            q.setParameter("telefono", cliente.getTelefono());
            q.setParameter("id_cliente", cliente.getIdCliente());
            int filaModificadas = q.executeUpdate();
            tr.commit();
            System.out.println("Filas modificadas: " + filaModificadas);
        } catch (Exception e) {
            tr.rollback();
            System.out.println("Mensaje-Error:" + e.getMessage());
        }

        sesion.close();
        System.exit(0);
    }

    /**
     * Metodo que obtiene todos los id de cliente
     *
     * @return devuelve un Array de tipo byte
     */
    public ArrayList<Byte> getIdAllClientes() {
        Session sesion = factory.openSession();
        ArrayList<Byte> lstIdsCclientes = new ArrayList<>();

        String hql = "select id from Clientes";
        try {
            Query q = sesion.createQuery(hql);
            lstIdsCclientes = (ArrayList<Byte>) q.list();
        } catch (Exception e) {
            System.out.println("Mensaje-Error:" + e.getMessage());
        }
        sesion.close();
        return lstIdsCclientes;
    }

    /**
     * Metodo de consulta Tipo. Segun el selector seleccionado en el enum
     * "ConsultasClientes" se realizara una consulta hql utilizando el elemento
     * seleccionado como criterio en la consulta
     *
     * @param cli datos de objeto cliente
     * @param tipoConsulta valor de critiero de consulta
     * @see ConsultasClientes
     */
    public void getClientes(Clientes cli, ConsultasClientes tipoConsulta) {
        Session sesion = factory.openSession();
        ArrayList<Clientes> lstClientes = new ArrayList<>();
        switch (tipoConsulta) {

            case NOMBRE:
                String hqlNombre = "from Clientes where NOMBRE like :nombre";

                try {

                    Query q = sesion.createQuery(hqlNombre);
                    q.setParameter("nombre", "%" + cli.getNombre() + "%");
                    lstClientes = (ArrayList<Clientes>) q.list();
                    Iterator it = q.iterate();
                    System.out.println("CLIENTES POR NOMBRE");
                    System.out.println("========================");
                    while (it.hasNext()) {
                        Clientes client = (Clientes) it.next();
                        System.out.printf("IdCliente: %d, Nombre: %s %n", client.getIdCliente(), client.getNombre());

                    }
                    System.out.println("");

                } catch (Exception e) {
                    System.out.println("Mensaje-Error:" + e.getMessage());

                }
                break;
            case APELLIDOS:
                String hqlApellido = "from Clientes where APELLIDOS like :apellido";

                try {

                    Query q = sesion.createQuery(hqlApellido);
                    q.setParameter("apellido", "%" + cli.getApellidos() + "%");
                    lstClientes = (ArrayList<Clientes>) q.list();
                    Iterator it = q.iterate();
                    System.out.println("CLIENTES POR APELLIDOS");
                    System.out.println("========================");
                    while (it.hasNext()) {
                        Clientes client = (Clientes) it.next();
                        System.out.printf("IdCliente: %d, Apellidos: %s %n", client.getIdCliente(), client.getApellidos());
                    }

                } catch (Exception e) {
                    System.out.println("Mensaje-Error:" + e.getMessage());

                }

                break;
        }

    }

    /**
     * Metodo para obtener todas las reservas pendientes de clientes. Computaran
     * todas aquellas que su fecha de inicio sea superior a la fecha de hoy
     *
     * @return retorna un Array de tipo de Clientes
     */
    public ArrayList<Clientes> ReservasPendientesCLI() {
        Session sesion = factory.openSession();
        ArrayList<Clientes> lstClientes = new ArrayList<>();

        String hql = "from Reservas where fechaInicio > :hoy ";

        try {

            String dateFormat = "dd/MM/yyyy";
            SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
            Date hoy = new Date();

            hoy = sdf.parse("06/01/2020");

            Query q = sesion.createQuery(hql);
            q.setParameter("hoy", hoy);

            lstClientes = (ArrayList<Clientes>) q.list();

            Iterator it = q.iterate();
            System.out.printf("RESERVAS PENDIENTES: " + lstClientes.size()
                    + "%n====================%n");
            while (it.hasNext()) {
                Reservas res = (Reservas) it.next();
                Clientes cli = new Clientes();
                cli = GS_CLI.getClienteById((byte) res.getIdCliente());
                System.out.printf("Id Reserva: %d, Id Cliente: %d, DNI: %s, Nombre: %s, Apellidos: %s, Direccion: %s, Telefono: %s %n",
                        res.getIdReserva(), res.getIdCliente(), cli.getDni(), cli.getNombre(), cli.getApellidos(), cli.getDireccion(), cli.getTelefono());
            }

        } catch (ParseException e) {
            System.out.println("Mensaje-Error:" + e.getMessage());

        }
        return lstClientes;
    }

    /**
     * Metodo para comprobar si existe un determinado cliente
     *
     * @param cli objeto cliente con los datos para la consulta
     * @return un valor booleano que valida en el metodo de inserccion
     * de reservas
     * @see gav3CLI.GAV3#insertaReserva()
     */
    public boolean existeCliente(Clientes cli) {
        ArrayList<Byte> lstIdClientes = new ArrayList<>();
        boolean existeCliente = false;

        try {
            lstIdClientes = getIdAllClientes();
            for (int idCliente : lstIdClientes) {
                if (cli.getIdCliente() == idCliente) {
                    existeCliente = true;
                    cli.setIdCliente(cli.getIdCliente());
                    break;
                } else {
                    existeCliente = false;
                }
            }
        } catch (NullPointerException NpEx) {
            System.out.printf("Mensaje error: " + NpEx.getMessage() + "%n");

        }
        return existeCliente;
    }

}
