package Controller.utils;

import Controller.GestorClientes;
import Controller.GestorCoches;
import Controller.GestorReservas;
import dao.alquiler;
import dao.alquileres;
import feedback.Clientes;
import feedback.Coches;
import feedback.HibernateUtil;
import feedback.Reservas;
import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 * Clase de apoyo a la funcionalidad. Carga de datos, borrado de datos, generacion de fichero XML
 *
 * @author Marco Memba aka "El aqluimista C-147"
 * @version 3.0
 * @since 2020-01-14
 */
public class GavUtils {

    private static final GestorClientes GS_CLI = new GestorClientes();
    private static final GestorCoches GS_CAR = new GestorCoches();
    SessionFactory factory = HibernateUtil.getSessionFactory();
    alquileres alquis = new alquileres();

    /**
     * Carga de varios clientes por medio del metodo insertCliente - 10
     * registros
     *
     * @see Controller.GestorClientes#insertCliente(feedback.Clientes)
     */
    public void cargaClientes() {

        String[] dnis = new String[]{"123456X", "457489F", "9658745D", "5879512G", "1245789Q", "7845912H", "0124785M", "9585632V", "2587419A", "6541237J"};
        String[] nombres = new String[]{"Marco", "Alex", "Julia", "Alberto", "Isarael", "Quique", "Adrian", "Laura", "Maria", "Eva"};
        String[] apellidos = new String[]{"Memba", "Helios", "Memba", "Garcia", "Lopez", "Fuentes", "Fernandez", "Fuente", "Sanchez", "Torres"};
        String[] direcciones = new String[]{"Calle 33", "Calle 25", "Calle 17", "Calle 29", "Calle 45", "Calle 5", "Calle 21", "Calle 39", "Calle 9", "Calle 36"};
        String[] telefonos = new String[]{"987654321", "912547895", "912365287", "942365874", "935257489", "989652478", "912354531", "911474959", "923012478", "910245407"};

        for (int i = 0; i < dnis.length; i++) {
            Reservas reserva = new Reservas();
            Clientes cli = new Clientes(GS_CLI.ultimoCliente(), dnis[i], nombres[i], apellidos[i], direcciones[i], telefonos[i], reserva);

            GS_CLI.insertCliente(cli);

        }
    }

     /**
     * Carga de varios coches por medio del metodo insertCliente - 5
     * registros
     *
     * @see Controller.GestorCoches#insertCoche(feedback.Coches) 
     */
    public void cargaCoches() {
        Date today = Calendar.getInstance().getTime();
        String[] matriculas = new String[]{"0044BWL", "4821HGF", "2196VCA", "0245HJQ", "9798JJB"};
        float precios[] = new float[]{2500.55f, 5f, 3f, 2f, 9000.85f};
        String[] colores = new String[]{"Rojo", "Negro", "Azul", "Verde", "Naranja"};
        String[] marcas = new String[]{"Seat", "Peugeot", "Citroen", "Nisan", "Audi"};
        Date[] fechasMat = new Date[]{today, today, today, today, today};

        for (int i = 0; i < matriculas.length; i++) {
            Reservas reserva = new Reservas();
            Coches car = new Coches(GS_CAR.ultimoCoche(), matriculas[i], precios[i], colores[i], marcas[i], fechasMat[i], reserva);
            GS_CAR.insertCoche(car);

        }
    }

    /**
     *  Borrado de la tabla indicada. Desde el metodo de entrada se llama una vez por cada tabla para borrar todas las tablas en una llamada
     * @param tabla recibe la tabla que se desea borrar
     * @see gav3CLI.GAV3#deleteAllData() 
     */
    public void deleteAllTableData(String tabla) {
        Session sesion = factory.openSession();
        Transaction transaction = sesion.beginTransaction();

        String hql = "delete from ";
        hql = hql.concat(tabla);
        try {

            Query q = sesion.createQuery(hql);
            //q.setParameter("tabla", tabla);

            System.out.printf("Numero de registros borrados: %d %n", q.executeUpdate());
            transaction.commit();
        } catch (Exception e) {
            System.out.println("Mensaje-Error coche:" + e.getMessage());

        }
        sesion.close();
    }

    /**
     * Metodo que genera un fichero XML mediante las librerias de JAXB, se apoya
     * en el metodo getAllReservas para obtener todas las reservas existentes.
     * Realiza los mapeos de los datos con las clases auxiliares alquier y
     * alquileres
     *
     * @see Controller.GestorReservas#getAllReservas()
     * @see dao.alquiler
     * @see dao.alquileres
     */
    public void reservasToXML() {
        ArrayList<Reservas> lstReservas = new ArrayList<>();
        GestorReservas gsRes = new GestorReservas();
        alquis.setListaReservas(new ArrayList<>());

        try {
            lstReservas = gsRes.getAllReservas();
            for (Reservas res : lstReservas) {
                alquiler alq = new alquiler(res.getIdReserva(), res.getFechaInicio(), res.getFechaDevolucion(),
                        res.getPrecioTotal(), res.getLitros(), (byte) res.getIdCliente(), (byte) res.getIdCoche());
                alquis.getListaReservas().add(alq);
            }

            JAXBContext context;
            context = JAXBContext.newInstance(alquileres.class);
            Marshaller m = context.createMarshaller();
            m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

            m.marshal(alquis, System.out);
            m.marshal(alquis, new File("./GAV3.xml"));
        } catch (JAXBException ex) {
            Logger.getLogger(GavUtils.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
