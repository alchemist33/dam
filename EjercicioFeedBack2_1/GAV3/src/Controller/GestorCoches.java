package Controller;

import Controller.GestorCoches.ConsultasCoches;
import Controller.utils.GavUtils;
import feedback.Coches;
import feedback.HibernateUtil;

import java.util.ArrayList;
import java.util.Iterator;

import org.hibernate.ObjectNotFoundException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.TransientPropertyValueException;
import org.hibernate.exception.ConstraintViolationException;

/**
 * Clase para la gestion de Coches
 *
 *
 * @author Marco Memba aka "El aqluimista C-147"
 * @version 3.0
 * @since 2020-01-14
 */
public class GestorCoches {

    private static final SessionFactory factory = HibernateUtil.getSessionFactory();
    private static final GavUtils GV_UT = new GavUtils();

    /**
     * Enumerado para selector de tipo de consulta Segun el valor escogido se
     * realiza una consulta por dicho criterio seleccionado en la tabla de
     * Coches
     */
    public enum ConsultasCoches {
        MATRICULA,
        MARCA

    }

    /**
     * Obtencion de un coche concreto segun el numero de Id
     *
     * @param IdCar id del coche que se obtendran los datos
     * @return retorna un objeto coche completo
     */
    public Coches getCocheById(byte IdCar) {
        Session sesion = factory.openSession();
        Coches car = new Coches();

        String hql = "from Coches where idCoche = :coche";

        try {

            Query q = sesion.createQuery(hql);
            q.setParameter("coche", (byte) IdCar);
            car = (Coches) q.uniqueResult();

        } catch (Exception Ex) {

            System.out.println("Mensaje-Error coche:" + Ex.getMessage());

        }

        return car;

    }

    /**
     * Metodo para la obtencion del ultmido id de coche.
     *
     * @return devuevle el valor maximo encontrado en el campo IdCoche
     */
    public byte ultimoCoche() {
        byte ultimoCoche = 0;
        //SessionFactory factory = HibernateUtil.getSessionFactory();
        Session sesion = factory.openSession();
        String hql = "select max(car.idCoche) from Coches as car";
        try {

            Query q = sesion.createQuery(hql);
            ultimoCoche = (byte) ((byte) q.uniqueResult() + 1);

        } catch (Exception e) {
            System.out.println("Mensaje-Error coches:" + e.getMessage());

        }

        sesion.close();
        //System.exit(0);

        return ultimoCoche;
    }

    /**
     * Metodo para la inserccion de un coche mediante el metodo save del obejto
     * sesion. El commit tiene sus posibles expeciones controladas y dispone de
     * un rollback enel bloque catch.
     *
     * @param coche objeto coche que se insertara en la base de datos
     */
    public void insertCoche(Coches coche) {

        Session sesion = factory.openSession();
        try {

            Transaction transaction = sesion.beginTransaction();

            System.out.printf("Insertamos una fila en la tabla Coches...%n");

            try {
                sesion.save(coche);
                transaction.commit();
                System.out.printf("Insert finalizado.%nCommit Succes!! %n");

            } catch (ConstraintViolationException CnEx) {
                transaction.rollback();
                System.out.println("Error: Coche duplicado");
                System.out.println("Mensaje: " + CnEx.getMessage());
                System.out.println("Codigo de error: " + CnEx.getErrorCode());
                System.out.println("SQL: " + CnEx.getSQLException().getMessage());
                System.out.println("=========================================");
                //Logger.getLogger(ResultSetInfo.class.getName()).log(Level.SEVERE, null, CnEx);
            }
            sesion.close();
            //System.exit(0);
        } catch (TransientPropertyValueException Ex) {
            System.out.println("Error: El coche no existe");
            //Logger.getLogger(ResultSetInfo.class.getName()).log(Level.SEVERE, null, Ex);
        }
    }

    /**
     * Metodo para borrar un coche de la base de datos. El commit tiene sus
     * posibles expeciones controladas y dispone de un rollback enel bloque
     * catch.
     *
     * @param coche objeto coche que represenata el coche a eliminar de la base
     * de datos
     */
    public void borraCoche(Coches coche) {

        Session sesion = factory.openSession();
        try {
            Transaction transaction = sesion.beginTransaction();

            coche = getCocheById((byte) coche.getIdCoche());

            if (coche == null) {
                System.out.println("El coche que desea eliminar no existe");
            } else {
                sesion.delete(coche);
                transaction.commit();
                System.out.println("Coche eliminado");
            }

        } catch (ObjectNotFoundException ObEx) {
            System.out.println("No exite el Coche");
        } catch (ConstraintViolationException CnEx) {
            System.out.println("Error: No  se puede borrar, el coche tiene reservas");
        } catch (Exception ex) {
            System.err.println("ERROR:");
            ex.printStackTrace();
        }
        sesion.close();
        System.exit(0);
    }

    /**
     * Metodo para modificar un coche de la base de datos. El commit tiene sus
     * posibles expeciones controladas y dispone de un rollback enel bloque
     * catch.
     *
     * @param coche objeto coche que represenata el coche a modificar de la base
     * de datos
     */
    public void modificaCoche(Coches coche) {

        Session sesion = factory.openSession();
        Transaction tr = sesion.beginTransaction();

        String hql = "update Coches set matricula = :matricula,"
                + "                       precio = :precio,"
                + "                       color = :color,"
                + "                       marca = :marca,"
                + "                       fecha_matriculacion = :fecha_matriculacion"
                + "                       where ID_COCHE = :id_coche";
        try {
            Query q = sesion.createQuery(hql);
            q.setParameter("matricula", coche.getMatricula());
            q.setParameter("precio", coche.getPrecio());
            q.setParameter("color", coche.getColor());
            q.setParameter("marca", coche.getMarca());
            q.setParameter("fecha_matriculacion", coche.getFechaMatriculacion());
            q.setParameter("id_coche", coche.getIdCoche());
            int filaModificadas = q.executeUpdate();
            tr.commit();
            System.out.println("Filas modificadas: " + filaModificadas);
        } catch (Exception e) {
            tr.rollback();
            System.out.println("Mensaje-Error:" + e.getMessage());
        }

        sesion.close();
        System.exit(0);

    }

    /**
     * Metodo que obtiene todos los id de coche
     *
     * @return evuelve un Array de tipo byte
     */
    public ArrayList<Byte> getIdAllCars() {
        Session sesion = factory.openSession();
        ArrayList<Byte> lstIdsCoches = new ArrayList<>();

        String hql = "select id from Coches";
        try {
            Query q = sesion.createQuery(hql);
            lstIdsCoches = (ArrayList<Byte>) q.list();
        } catch (Exception e) {
            System.out.println("Mensaje-Error:" + e.getMessage());
        }
        sesion.close();
        return lstIdsCoches;
    }

    /**
     * Metodo de consulta Tipo. Segun el selector seleccionado en el enum
     * "ConsultasCoches" se realizara una consulta hql utilizando el elemento
     * seleccionado como criterio en la consulta
     *
     * @param coche datos de objeto coche
     * @param tipoConsulta
     * @see ConsultasCoches
     */
    public void getCoches(Coches coche, ConsultasCoches tipoConsulta) {
        Session sesion = factory.openSession();
        ArrayList<Coches> lstCoches = new ArrayList<>();
        switch (tipoConsulta) {

            case MATRICULA:

                String hqlMatricula = "from Coches where matricula like :matricula";

                try {

                    Query q = sesion.createQuery(hqlMatricula);
                    q.setParameter("matricula", "%" + coche.getMatricula() + "%");
                    lstCoches = (ArrayList<Coches>) q.list();
                    Iterator it = q.iterate();
                    System.out.println("COCHES POR MATRICULA");
                    System.out.println("========================");
                    while (it.hasNext()) {
                        Coches car = (Coches) it.next();
                        System.out.printf("IdCoche: %d, Matricula: %s %n", car.getIdCoche(), car.getMatricula());

                    }
                    System.out.println("");
                } catch (Exception e) {
                    System.out.println("Mensaje-Error:" + e.getMessage());
                }

                break;
            case MARCA:
                String hqlMarca = "from Coches where MARCA like :marca";

                try {

                    Query q = sesion.createQuery(hqlMarca);
                    q.setParameter("marca", "%" + coche.getMarca() + "%");
                    lstCoches = (ArrayList<Coches>) q.list();
                    Iterator it = q.iterate();
                    System.out.println("COCHES POR MARCA");
                    System.out.println("========================");
                    while (it.hasNext()) {
                        Coches car = (Coches) it.next();
                        System.out.printf("IdCoche: %d, Marca: %s %n", car.getIdCoche(), car.getMarca());
                    }
                    System.out.println("");
                } catch (Exception e) {
                    System.out.println("Mensaje-Error:" + e.getMessage());

                }
                break;
        }
        sesion.close();
    }

    /**
     * Metodo para comprobar si existe un determinado coche
     *
     * @param car objeto cliente con los datos para la consulta
     * @return un valor booleano que valida en el metodo de inserccion
     * de reservas
     * @see gav3CLI.GAV3#insertaReserva()
     */
    public boolean existeCoche(Coches car) {
        ArrayList<Byte> lstIdCoches = new ArrayList<>();
        boolean existeCoche = false;

        try {
            lstIdCoches = getIdAllCars();
            for (int idCoche : lstIdCoches) {
                if (car.getIdCoche() == idCoche) {
                    existeCoche = true;
                    car.setIdCoche(car.getIdCoche());
                    break;
                } else {
                    existeCoche = false;
                }
            }
        } catch (NullPointerException NpEx) {

            System.out.printf("Mensaje error: " + NpEx.getMessage() + "%n");
        }
        return existeCoche;
    }
}
