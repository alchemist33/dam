package Controller;

import feedback.Clientes;
import feedback.Coches;
import feedback.HibernateUtil;
import feedback.Reservas;
import Controller.utils.GavUtils;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.HibernateException;
import org.hibernate.ObjectNotFoundException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.TransientPropertyValueException;
import org.hibernate.exception.ConstraintViolationException;

/**
 * Clase para la gestion de reservas
 *
 * @author Marco Memba aka "El aqluimista C-147"
 * @version 3.0
 * @since 2020-01-14
 */
public class GestorReservas {

    GavUtils gavUtil = new GavUtils();
    SessionFactory factory = HibernateUtil.getSessionFactory();
    private static final GestorCoches GS_CAR = new GestorCoches();
    private static final GestorClientes GS_CLI = new GestorClientes();

    /**
     *
     * Metodo para introducir una nueva reserva. Se realiza una transaccion
     * ("transaction"). Se realiza una transaccion controlada en caso de error
     * controlada. A este metodo solo deben llegar objtos validados
     *
     *
     *
     * @param reserva se solicita una objeto Reservas para incluirlo
     * directamente mediante un objeto Session.
     *
     * @exception TransientPropertyValueException Control de errores de
     * transiciones Hibernate
     * @exception ConstraintViolationException controla la correcta inserccion
     * de datos segun las restricciones de la tabla, evitando la parada del
     * programa
     *
     */
    public void NuevaReserva(Reservas reserva) {

        Session sesion = factory.openSession();
        try {

            Transaction transaction = sesion.beginTransaction();

            System.out.printf("Insertamos una fila en la tabla Reservas...%n");

            try {
                sesion.save(reserva);
                transaction.commit();
                System.out.printf("Nueva reserva insertada!.%nCommit Succes!! %n");

            } catch (ConstraintViolationException CnEx) {
                transaction.rollback();
                System.out.println("Error: Reserva duplicada");
                System.out.println("Mensaje: " + CnEx.getMessage());
                System.out.println("Codigo de error: " + CnEx.getErrorCode());
                System.out.println("SQL: " + CnEx.getSQLException().getMessage());
                System.out.println("=========================================");
                Logger.getLogger(GestorReservas.class.getName()).log(Level.SEVERE, null, CnEx);
            } catch (Exception Ex) {
                transaction.rollback();
                System.out.println("Mensaje: " + Ex.getMessage());
                Logger.getLogger(GestorReservas.class.getName()).log(Level.SEVERE, null, Ex);
            }
            sesion.close();
            //System.exit(0);
        } catch (TransientPropertyValueException TrPrVaEx) {
            System.out.println("Error: no se puede insertar la reserva");
            Logger.getLogger(GestorReservas.class.getName()).log(Level.SEVERE, null, TrPrVaEx);
        }
    }

    /**
     * Metodo que permite saber la ultima reserva existente en la base de datos,
     * se realiza una consulta agrupada de Hibernate "select max(idReserva)"
     *
     * @return tipo byte con el numero id de la reserva
     */
    public byte ultimaReserva() {
        byte ultimaReserva = 0;

        Session sesion = factory.openSession();
        String hql = "select max(res.idReserva) from Reservas as res";
        try {

            Query q = sesion.createQuery(hql);
            ultimaReserva = (byte) ((byte) q.uniqueResult() + 1);

        } catch (Exception e) {
            System.out.println("Mensaje-Error reserva:" + e.getMessage());

        }

        sesion.close();
        //System.exit(0);

        return ultimaReserva;
    }

    /**
     * Metodo para la obtencion de todas las reservas que se encuentran en curso
     * en el dia de hoy para un coche concreto. Computan todas aquellas reservas
     * que tengan como fecha inicio un valor anterior al dia de hoy y como fecha
     * de devolucion un valor mayor al día de hoy
     *
     * @param coche Objeto Coches que se usa para la obtencion del id y el
     * numero de matricula
     * @return se retorna un array del tipo Reservas con todos los registros que
     * cumplan las clausulas
     */
    public ArrayList<Reservas> getReservasHoy(Coches coche) {
        Session sesion = factory.openSession();
        ArrayList<Reservas> lstReservas = new ArrayList<>();
        Date today = Calendar.getInstance().getTime();

        String hql = "from Reservas where fechaInicio <= :hoy and fechaDevolucion >= :hoy and idCoche = :idCoche";

        try {

            Query q = sesion.createQuery(hql);
            q.setParameter("hoy", today);
            q.setParameter("idCoche", (int) coche.getIdCoche());

            lstReservas = (ArrayList<Reservas>) q.list();
            Iterator it = q.iterate();
            System.out.println("Reservas activas hoy: " + lstReservas.size());

            while (it.hasNext()) {
                Reservas res = (Reservas) it.next();
                String matricula = GS_CAR.getCocheById((byte) res.getIdCoche()).getMatricula();
                System.out.printf("Reserva nº: %d, Matricula reservada: %s %n", res.getIdReserva(), matricula);
            }
        } catch (HibernateException Hbex) {
            System.out.println("Mensaje-Error-Hibernate:" + Hbex.getMessage());

        } catch (Exception ex) {
            System.out.println("Mensaje-Error:" + ex.getMessage());
        }
        return lstReservas;
    }

    /**
     * Meotodo para verficar si un coche en concreto esta reservado en las
     * fechas que se quiera introducir
     *
     * @param res se recibe un objeto de tipo Reservas con los datos que se
     * desean incluir en la tabla Reservas
     * @param IdCoche se obtiene el id del Coche que se desea reservar
     * @return retorna un valor booleano que valida en el metodo de inserccion
     * de reservas
     * @see gav3CLI.GAV3#insertaReserva()
     */
    public boolean reservaExiste(Reservas res, byte IdCoche) {
        Session sesion = factory.openSession();
        ArrayList<Reservas> lstReservas = new ArrayList<>();
        boolean resExiste = false;

        String hql = "from Reservas where idCoche = :IdCar";

        try {
            Query q = sesion.createQuery(hql);
            q.setParameter("IdCar", IdCoche);
            lstReservas = (ArrayList<Reservas>) q.list();

            for (Reservas reserva : lstReservas) {
                if (res.getFechaInicio().compareTo(reserva.getFechaInicio()) < 0) {
                    if (res.getFechaInicio().compareTo(reserva.getFechaDevolucion()) > 0) {
                        if (res.getFechaDevolucion().compareTo(reserva.getFechaInicio()) < 0) {
                            if (res.getFechaDevolucion().compareTo(reserva.getFechaDevolucion()) > 0) {
                                resExiste = false;

                            } else {
                                System.out.println("Este coche ya esta reservado en esta fecha ");
                                resExiste = true;
                            }

                        } else {
                            System.out.println("Este coche ya esta reservado en esta fecha ");
                            resExiste = true;
                        }

                    } else {
                        System.out.println("Este coche ya esta reservado en esta fecha ");
                        resExiste = true;
                    }

                } else {
                    System.out.println("Este coche ya esta reservado en esta fecha ");
                    resExiste = true;

                }
            }
        } catch (HibernateException Hbex) {
            System.out.println("Mensaje-Error-Hibernate:" + Hbex.getMessage());

        } catch (NullPointerException NpEx) {
            System.out.println("Null-Pointer:" + NpEx.getMessage());
            System.out.println("Null-Pointer:" + NpEx.toString());
        } catch (Exception ex) {
            System.out.println("Mensaje-Error:" + ex.getMessage());
        }

        return resExiste;

    }

    /**
     * Metodo que obtiene todas las reservas existentes en la base de datos.
     *
     * @return Array de tipo Resrervas
     * @see Controller.utils.GavUtils#reservasToXML()
     */
    public ArrayList<Reservas> getAllReservas() {
        Session sesion = factory.openSession();
        ArrayList<Reservas> lstReservas = new ArrayList<>();

        String hql = "from Reservas";
        try {
            Query q = sesion.createQuery(hql);
            lstReservas = (ArrayList<Reservas>) q.list();

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        return lstReservas;
    }

    /**
     * Metodo para la obtencion del numero de reservas que ha tenido un coche en
     * concreto. La consula HQL recupera un numero que representa la cantidad de
     * reservas de ese coche. A continuacion se obtiene el coche mediante el
     * paramentro solicitado y se exponen los datos
     *
     * @param IdCar id del coche que se desea consultar sobre cuantas veces a sido reservado
     * @see Controller.GestorCoches#getCocheById(byte)
     */
    public void getReservasPorCoche(byte IdCar) {
        Session sesion = factory.openSession();
        long NumRes = 0;

        String hql = "select count(idReserva)from Reservas where idCoche = :IdCoche";

        try {

            Query q = sesion.createQuery(hql);
            q.setParameter("IdCoche", (int) IdCar);
            NumRes = (long) q.uniqueResult();
            Coches car = new Coches();
            car = GS_CAR.getCocheById(IdCar);
            System.out.printf("El coche con matricula: %s, se ha reservado %d veces. %n", car.getMatricula(), NumRes);
        } catch (Exception e) {
            System.out.println("Mensaje-Error reserva:" + e.getMessage());

        }

    }

    /**
     * Metodo para la obtencion de los coches distintos que ha reservado un
     * cliente en concreto. Se utiliza la funcion de agrupacion hql "select
     * distinct" para descartar los registros que tengan el mismo id de coche.
     * Se obtiene un Array de tipo Coches, pero no se recorre, se utiliza para
     * contabiliar el numero de coches distitnos. Los datos se exponen con los 
     * datos del cliente
     *
     * @param IdCli id de cliente que se quiere consultar sobre cuantos coches distitnos a reservado
     */
    public void getCochesPorCliente(byte IdCli) {
        Session sesion = factory.openSession();
        ArrayList<Integer> lstCoches = new ArrayList<>();

        String hql = "select distinct idCoche from Reservas where idCliente = :idCli";

        try {
            Clientes cli = new Clientes();
            cli = GS_CLI.getClienteById(IdCli);

            Query q = sesion.createQuery(hql);
            q.setParameter("idCli", (int) IdCli);
            lstCoches = (ArrayList<Integer>) q.list();
            System.out.printf("Numero de coches diferentes reservados al cliente: %s, %s: %d %n", cli.getApellidos(), cli.getNombre(), lstCoches.size());

        } catch (Exception e) {
            System.out.println("Mensaje-Error coche1:" + e.getMessage());

        }
        sesion.close();

    }

}
