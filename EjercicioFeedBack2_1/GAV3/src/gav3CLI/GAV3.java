package gav3CLI;

import Controller.*;
import Controller.GestorClientes.ConsultasClientes;
import Controller.GestorCoches.ConsultasCoches;
import Controller.utils.GavUtils;
import feedback.Clientes;
import feedback.Coches;
import feedback.Reservas;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Calse de entrada al programa GAV (Gestion de Alquiler de Vehículos)
 *
 *
 * @author Marco Memba aka "El aqluimista C-147"
 * @version 3.0
 * @since 2020-01-14
 */
public class GAV3 {

    // <editor-fold defaultstate="collapsed" desc="Controller">
    private static final GestorClientes GS_CLI = new GestorClientes();
    private static final GestorCoches GS_CAR = new GestorCoches();
    private static final GestorReservas GS_RES = new GestorReservas();
    private static final GavUtils GV_UT = new GavUtils();
    private static Clientes CLI = new Clientes();
    private static Coches CAR = new Coches();
    private static final Reservas RES = new Reservas();
    // </editor-fold>  

    /**
     * Metodo principal de llamada a metodos de la misma clase. En cada metodo
     * se realizan las declaraciones de varibales y toma de datos aleatorios
     * necesarios para poder llevar a cabo ese metodo. Los datos de esas
     * varibles y los datos son modificables y no deberian afectar al
     * funcionamiento del resto del programa.
     *
     * @param args
     */
    public static void main(String[] args) {

        //deleteAllData();
        //cargaDatos();
        //insertaCliente();
        //borrarCliente();
        //modificaCliente();
        //insertaCoche();
        //borrarCoche();
        //modificaCoche();
        //insertaReserva();
        //getCoches();
        //getClientes();
        //getReservasHoy();
        //getReservasPendientes();
        //cochesReservadosCliente();
        //reservasPorCoche();
        //reservasToXML();
        System.exit(0);
    }

    // <editor-fold defaultstate="collapsed" desc="Extras">
    public static void cargaDatos() {
        GV_UT.cargaClientes();
        GV_UT.cargaCoches();
    }

    public static void deleteAllData() {
        GV_UT.deleteAllTableData("Reservas");
        GV_UT.deleteAllTableData("Coches");
        GV_UT.deleteAllTableData("Clientes");
    }
    // </editor-fold>  

    // <editor-fold defaultstate="collapsed" desc="Operaciones con clientes">
    public static void insertaCliente() {

        CLI.setIdCliente(GS_CLI.ultimoCliente());
        CLI.setDni("123456x");
        CLI.setNombre("Marco");
        CLI.setApellidos("Memba");
        CLI.setDireccion("Calle5");
        CLI.setTelefono("123456789");

        GS_CLI.insertCliente(CLI);
    }

    public static void borrarCliente() {
        CLI.setIdCliente((byte) 47);

        GS_CLI.borraCliente(CLI);

    }

    public static void modificaCliente() {
        byte idCli = 47;
        String DNI = "987654";
        String nombre = "Manuel";
        String Apellidos = "Lopez";
        String Direccion = "Calle555";
        String Telefono = "555666777";
        CLI.setIdCliente(idCli);
        CLI.setDni(DNI);
        CLI.setNombre(nombre);
        CLI.setApellidos(Apellidos);
        CLI.setDireccion(Direccion);
        CLI.setTelefono(Telefono);

        GS_CLI.modificaCliente(CLI);
    }

    public static void getClientes() {
        CLI = GS_CLI.getClienteById((byte) 0);

        GS_CLI.getClientes(CLI, ConsultasClientes.NOMBRE);
        GS_CLI.getClientes(CLI, ConsultasClientes.APELLIDOS);

    }
// </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Operaciones con coches">
    public static void insertaCoche() {
        Date today = Calendar.getInstance().getTime();

        CAR.setIdCoche(GS_CAR.ultimoCoche());
        CAR.setMatricula("0044BWL");
        CAR.setPrecio((float) 2500.55);
        CAR.setColor("Rojo");
        CAR.setMarca("Seat");
        CAR.setFechaMatriculacion(today);

        GS_CAR.insertCoche(CAR);
    }

    public static void borrarCoche() {
        CAR.setIdCoche((byte) 2);
        GS_CAR.borraCoche(CAR);
    }

    public static void modificaCoche() {
        byte idCar = 2;
        String Matricula = "4574DKG";
        float Precio = (float) 3500.75;
        String Color = "Lopez";
        String Marca = "Calle555";
        Date FechaMatriculacion = new Date(198, 5, 3);
        CAR.setIdCoche(idCar);
        CAR.setMatricula(Matricula);
        CAR.setPrecio(Precio);
        CAR.setColor(Color);
        CAR.setMarca(Marca);
        CAR.setFechaMatriculacion(FechaMatriculacion);

        GS_CAR.modificaCoche(CAR);
    }

    public static void getCoches() {
        CAR = GS_CAR.getCocheById((byte) 1);

        GS_CAR.getCoches(CAR, ConsultasCoches.MATRICULA);
        GS_CAR.getCoches(CAR, ConsultasCoches.MARCA);

    }

    public static void cochesReservadosCliente() {

        CLI = GS_CLI.getClienteById((byte) 1);
        GS_RES.getCochesPorCliente(CLI.getIdCliente());
    }

    public static void reservasPorCoche() {
        CAR = GS_CAR.getCocheById((byte) 2);
        GS_RES.getReservasPorCoche(CAR.getIdCoche());
    }
// </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Operaciones con reservas">
    public static void insertaReserva() {

        try {

            String dateFormat = "dd/MM/yyyy";
            SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
            Date fechaI = new Date();
            Date fechaF = new Date();

            fechaI = sdf.parse("05/02/2020");
            fechaF = sdf.parse("07/02/2020");

            CLI = GS_CLI.getClienteById((byte) 1);
            CAR = GS_CAR.getCocheById((byte) 2);
            RES.setIdReserva(GS_RES.ultimaReserva());

            RES.setCoches(CAR);
            RES.setClientes(CLI);
            RES.setFechaInicio(fechaI);
            RES.setFechaDevolucion(fechaF);

            RES.setLitros((float) 0.55f);
            RES.setPrecioTotal((float) 22.35f);

            //CAR.setReservas(RES);
            //CLI.setReservas(RES);
            // -------------------- COMPROBACIONES ----------------------
            if (GS_CAR.existeCoche(CAR)) { // Comprobacion coche existe
                if (GS_CLI.existeCliente(CLI)) {  // Comprobacion cliente existe
                    if (GS_RES.reservaExiste(RES, CAR.getIdCoche())) { // Comprobacion de fecha reserva coche
                        GS_RES.NuevaReserva(RES);
                    } else {

                    }
                } else {
                    System.out.println("El cliente no existe");
                }
            } else {
                System.out.println("El coche no existe");
            }

        } catch (ParseException ex) {
            Logger.getLogger(GAV3.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void getReservasHoy() {

        CAR = GS_CAR.getCocheById((byte) 2);
        GS_RES.getReservasHoy(CAR);
    }

    public static void getReservasPendientes() {

        GS_CLI.ReservasPendientesCLI();
    }
// </editor-fold>

    // Todas las reservas a fichero XML
    public static void reservasToXML() {

        GV_UT.reservasToXML();
    }
}
