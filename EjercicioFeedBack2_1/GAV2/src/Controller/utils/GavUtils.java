/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller.utils;

import feedback.HibernateUtil;
import java.util.Iterator;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

/**
 *
 * @author marco
 */
public class GavUtils {

    public byte ultimaReserva() {
        byte ultimaReserva = 0;
        SessionFactory factory = HibernateUtil.getSessionFactory();
        Session sesion = factory.openSession();
        String hql = "select max(res.idReserva) from Reservas as res";
        try {

            Query q = sesion.createQuery(hql);
            ultimaReserva = (byte) ((byte) q.uniqueResult() + 1);

        } catch (Exception e) {
            System.out.println("Mensaje-Error Reserva:" + e.getMessage());

        }

        sesion.close();
        //System.exit(0);

        return ultimaReserva;
    }

    public byte ultimoCliente() {

        byte ultimoCliente = 0;
        SessionFactory factory = HibernateUtil.getSessionFactory();
        Session sesion = factory.openSession();
        String hql = "select max(cli.idCliente) from Clientes as cli";
        try {

            Query q = sesion.createQuery(hql);
            ultimoCliente = (byte) ((byte) q.uniqueResult() + 1);

        } catch (Exception e) {
            System.out.println("Mensaje-Error Cliente:" + e.getMessage());

        }

        sesion.close();
        //System.exit(0);

        return ultimoCliente;
    }
}
