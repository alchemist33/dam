
package Controller;

import feedback.Clientes;
import feedback.Coches;
import feedback.HibernateUtil;
import feedback.Reservas;
import Controller.utils.GavUtils;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.TransientPropertyValueException;
import org.hibernate.exception.ConstraintViolationException;

/**
 *
 * @author marco
 */
public class GestorReservas {
    GavUtils gavUtil = new GavUtils();
    
    public void NuevaReserva(Clientes cliente, Coches coche,Reservas reserva) {
        SessionFactory factory = HibernateUtil.getSessionFactory();
        Session sesion = factory.openSession();
        try {
            
            Transaction transaction = sesion.beginTransaction();
            
            reserva.setIdReserva((byte) gavUtil.ultimaReserva());
            reserva.setCoches(coche);
            reserva.setClientes(cliente);
            reserva.setFechaInicio(reserva.getFechaInicio());
            reserva.setFechaDevolucion(reserva.getFechaDevolucion());
            System.out.printf("Insertamos una fila en la tabla Reservas...%n");
            
            sesion.save(reserva);
            try {
                transaction.commit();
                System.out.printf("Insert finalizado.%nCommit Succes!! %n");
                
            } catch (ConstraintViolationException CnEx) {
                transaction.rollback();
                System.out.println("Error: Reserva duplicada");
                System.out.println("Mensaje: " + CnEx.getMessage());
                System.out.println("Codigo de error: " + CnEx.getErrorCode());
                System.out.println("SQL: " + CnEx.getSQLException().getMessage());
                System.out.println("=========================================");
                //Logger.getLogger(ResultSetInfo.class.getName()).log(Level.SEVERE, null, CnEx);
            }
            sesion.close();
            System.exit(0);
        } catch (TransientPropertyValueException Ex) {
            System.out.println("Error: El departamento no existe");
            //Logger.getLogger(ResultSetInfo.class.getName()).log(Level.SEVERE, null, Ex);
        }
    }
    
//    public void borraReserva(Clientes cliente, Coches coche) {
//        SessionFactory factory = HibernateUtil.getSessionFactory();
//        Session sesion = factory.openSession();
//        try {
//            Transaction transaction = sesion.beginTransaction();
//            //Departamentos departamento = (Departamentos) sesion.load(Departamentos.class, (byte) 10);
//
//            sesion.delete(cliente);
//            transaction.commit();
//            System.out.println("cliente eliminado");
//            
//        } catch (ObjectNotFoundException ObEx) {
//            System.out.println("No exite el cliente");
//        } catch (ConstraintViolationException CnEx) {
//            System.out.println("Error: No  se puede borrar, el cliente tiene reservas");
//        } catch (Exception ex) {
//            System.err.println("ERROR:");
//            ex.printStackTrace();
//        }
//        sesion.close();
//        System.exit(0);
//    }
//    
//    public void modificaReserva(Clientes cliente, Coches coche) {
//        SessionFactory factory = HibernateUtil.getSessionFactory();
//        Session sesion = factory.openSession();
//        Transaction tr = sesion.beginTransaction();
//        
//        String hql = "update Clientes set dni = :dni"
//                + "                       nombre = :nombre,"
//                + "                       apellidos = :apellidos,"
//                + "                       direccion = :direccion,"
//                + "                       telefono = :telefono"
//                + "                       where ID_CLIENTE = :id_cliente";
//        try {
//            Query q = sesion.createQuery(hql);
//            q.setParameter("dni", cliente.getDni());
//            q.setParameter("nombre", cliente.getNombre());
//            q.setParameter("apellidos", cliente.getApellidos());
//            q.setParameter("direccion", cliente.getDireccion());
//            q.setParameter("telefono", cliente.getTelefono());
//            q.setParameter("id_cliente", cliente.getIdCliente());
//            int filaModificadas = q.executeUpdate();
//            tr.commit();
//            System.out.println("Filas modificadas: " + filaModificadas);
//        } catch (Exception e) {
//            tr.rollback();
//            System.out.println("Mensaje-Error:" + e.getMessage());
//        }
//        
//        sesion.close();
//        System.exit(0);
//    }
    
}
