/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import feedback.Clientes;
import feedback.HibernateUtil;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.ObjectNotFoundException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.TransientPropertyValueException;
import org.hibernate.exception.ConstraintViolationException;

/**
 *
 * @author marco
 */
public class GestorClientes {

    public void insertCliente(Clientes cliente) {
        SessionFactory factory = HibernateUtil.getSessionFactory();
        Session sesion = factory.openSession();
        try {

            Transaction transaction = sesion.beginTransaction();

            System.out.printf("Insertamos una fila en la tabla Clientes...%n");

            
            try {
                sesion.save(cliente);
                transaction.commit();
                System.out.printf("Insert finalizado.%nCommit Succes!! %n");

            } catch (ConstraintViolationException CnEx) {
                System.out.println("Error: Realizando RollBack....");
                transaction.rollback();
                System.out.println("Error:  RollBack realizado con exito");
                System.out.println("Error: Cliente duplicado");
                System.out.println("Mensaje: " + CnEx.getMessage());
                System.out.println("Codigo de error: " + CnEx.getErrorCode());
                System.out.println("SQL: " + CnEx.getSQLException().getMessage());
                System.out.println("=========================================");
                //Logger.getLogger(GestorClientes.class.getName()).log(Level.SEVERE, null, CnEx);
            }
            
            sesion.close();
            //System.exit(0);
        } catch (TransientPropertyValueException Ex) {
            System.out.println("Error: El cliente no existe");
            //Logger.getLogger(ResultSetInfo.class.getName()).log(Level.SEVERE, null, Ex);
        }
    }

    public void borraCliente(Clientes cliente) {
        SessionFactory factory = HibernateUtil.getSessionFactory();
        Session sesion = factory.openSession();
        try {
            Transaction transaction = sesion.beginTransaction();
            //Departamentos departamento = (Departamentos) sesion.load(Departamentos.class, (byte) 10);

            sesion.delete(cliente);
            transaction.commit();
            System.out.println("cliente eliminado");

        } catch (ObjectNotFoundException ObEx) {
            System.out.println("No exite el cliente");
        } catch (ConstraintViolationException CnEx) {
            System.out.println("Error: No  se puede borrar, el cliente tiene reservas");
        } catch (Exception ex) {
            System.err.println("ERROR:");
            ex.printStackTrace();
        }
        sesion.close();
        //System.exit(0);
    }

    public void modificaCliente(Clientes cliente) {
        SessionFactory factory = HibernateUtil.getSessionFactory();
        Session sesion = factory.openSession();
        Transaction tr = sesion.beginTransaction();

        String hql = "update Clientes set dni = :dni"
                + "                       nombre = :nombre,"
                + "                       apellidos = :apellidos,"
                + "                       direccion = :direccion,"
                + "                       telefono = :telefono"
                + "                       where ID_CLIENTE = :id_cliente";
        try {
            Query q = sesion.createQuery(hql);
            q.setParameter("dni", cliente.getDni());
            q.setParameter("nombre", cliente.getNombre());
            q.setParameter("apellidos", cliente.getApellidos());
            q.setParameter("direccion", cliente.getDireccion());
            q.setParameter("telefono", cliente.getTelefono());
            q.setParameter("id_cliente", cliente.getIdCliente());
            int filaModificadas = q.executeUpdate();
            tr.commit();
            System.out.println("Filas modificadas: " + filaModificadas);
        } catch (Exception e) {
            tr.rollback();
            System.out.println("Mensaje-Error:" + e.getMessage());
        }

        sesion.close();
        System.exit(0);
    }

}
