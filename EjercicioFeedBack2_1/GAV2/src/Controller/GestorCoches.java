/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import feedback.Coches;
import feedback.HibernateUtil;
import org.hibernate.ObjectNotFoundException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.TransientPropertyValueException;
import org.hibernate.exception.ConstraintViolationException;

/**
 *
 * @author marco
 */
public class GestorCoches {

    public void insertCoche(Coches coche) {
        SessionFactory factory = HibernateUtil.getSessionFactory();
        Session sesion = factory.openSession();
        try {

            Transaction transaction = sesion.beginTransaction();

            System.out.printf("Insertamos una fila en la tabla Coches...%n");

            sesion.save(coche);
            try {
                transaction.commit();
                System.out.printf("Insert finalizado.%nCommit Succes!! %n");

            } catch (ConstraintViolationException CnEx) {
                transaction.rollback();
                System.out.println("Error: Coche duplicado");
                System.out.println("Mensaje: " + CnEx.getMessage());
                System.out.println("Codigo de error: " + CnEx.getErrorCode());
                System.out.println("SQL: " + CnEx.getSQLException().getMessage());
                System.out.println("=========================================");
                //Logger.getLogger(ResultSetInfo.class.getName()).log(Level.SEVERE, null, CnEx);
            }
            sesion.close();
            System.exit(0);
        } catch (TransientPropertyValueException Ex) {
            System.out.println("Error: El coche no existe");
            //Logger.getLogger(ResultSetInfo.class.getName()).log(Level.SEVERE, null, Ex);
        }
    }

    public void borraCoche(Coches coche) {
        SessionFactory factory = HibernateUtil.getSessionFactory();
        Session sesion = factory.openSession();
        try {
            Transaction transaction = sesion.beginTransaction();
            //Departamentos departamento = (Departamentos) sesion.load(Departamentos.class, (byte) 10);

            sesion.delete(coche);
            transaction.commit();
            System.out.println("Coche eliminado");

        } catch (ObjectNotFoundException ObEx) {
            System.out.println("No exite el Coche");
        } catch (ConstraintViolationException CnEx) {
            System.out.println("Error: No  se puede borrar, el coche tiene reservas");
        } catch (Exception ex) {
            System.err.println("ERROR:");
            ex.printStackTrace();
        }
        sesion.close();
        System.exit(0);
    }

    public void modificaCoche(Coches coche) {
        SessionFactory factory = HibernateUtil.getSessionFactory();
        Session sesion = factory.openSession();
        Transaction tr = sesion.beginTransaction();

        String hql = "update Coches set matricula = :matricula,"
                + "                       precio = :precio,"
                + "                       color = :color,"
                + "                       marca = :marca,"
                + "                       fecha_matriculacion = :fecha_matriculacion"
                + "                       where ID_COCHE = :id_coche";
        try {
            Query q = sesion.createQuery(hql);
            q.setParameter("matricula", coche.getMatricula());
            q.setParameter("precio", coche.getPrecio());
            q.setParameter("color", coche.getColor());
            q.setParameter("marca", coche.getMarca());
            q.setParameter("fecha_matriculacion", coche.getFechaMatriculacion());
            q.setParameter("id_coche", coche.getIdCoche());
            int filaModificadas = q.executeUpdate();
            tr.commit();
            System.out.println("Filas modificadas: " + filaModificadas);
        } catch (Exception e) {
            tr.rollback();
            System.out.println("Mensaje-Error:" + e.getMessage());
        }

        sesion.close();
        System.exit(0);
    }

}
