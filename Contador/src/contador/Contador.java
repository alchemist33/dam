/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package contador;

/**
 *
 * @author marco
 */
public class Contador {

    private int c = 0;

    public int Valor() {
        return c;
    }
//
//    public void incrementar() {
//        c = c + 1;
//    }
//    public void decrementar() {
//        c = c - 1;
//    }

// el syncronized crea la zona de exclusion
      public synchronized int[] incrementar() {
        int[] array = new int[2];
        array[0] = Valor();
        c = array[0] + 1;
        array[1] = Valor();
        return array;
    }
    public synchronized int[] decrementar() {
        int[] array = new int[2];
        array[0] = Valor();
        c = array[0] - 1;
        array[1] = Valor();
        return array;
    }
    
}
