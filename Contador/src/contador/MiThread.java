/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package contador;

import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author marco
 */
public class MiThread extends Thread {

    Contador contador;

    public MiThread(Contador contador) {
        this.contador = contador;
    }

//    @Override
//    public void run() {
//        contador.incrementar();
//        System.out.println("El valor es: " + contador.Valor());
//        try {
//            sleep(1000);
//        } catch (InterruptedException ex) {
//            Logger.getLogger(MiThread.class.getName()).log(Level.SEVERE, null, ex);
//        }
//    }
    @Override
    public void run() {
        int[] arrayIncrementado = contador.incrementar();
        
        System.out.println("AUM - El valor anterior es: " + arrayIncrementado[0] + " | El valor actual es: " + arrayIncrementado[1]);
        try {
            Random rand = new Random();
            Thread.sleep(rand.nextInt(10));
        } catch (InterruptedException ex) {
            Logger.getLogger(MiThread.class.getName()).log(Level.SEVERE, null, ex);
        }
        int[] arrayDecrementado = contador.decrementar();
        
        System.out.println("DEC - El valor anterior es: " + arrayDecrementado[0] + " | El valor actual es: " + arrayDecrementado[1]);
    }

}
