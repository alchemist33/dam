/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crearneodatis;


import org.neodatis.odb.ODB;
import org.neodatis.odb.ODBFactory;
import org.neodatis.odb.Objects;

/**
 *
 * @author marco
 */
public class CrearNeoDatis {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        Jugadores jugador1 = new Jugadores("Marcos", "golf", "Zaragoza", 14);
        Jugadores jugador2 = new Jugadores("Teresa", "balonmano", "Pamplona", 14);
        Jugadores jugador3 = new Jugadores("Mario", "futbol", "Tenerife", 15);
        Jugadores jugador4 = new Jugadores("Patricia", "natacion", "Madrid",14);
        
        
        ODB odb = ODBFactory.open("C:\\NeoDatis ODB/neodatis.test");
        
        odb.store(jugador1);
        odb.store(jugador2);
        odb.store(jugador3);
        odb.store(jugador4);
        
        Objects<Jugadores> listaJugadores = odb.getObjects(Jugadores.class);
        int i = 1;
        
        while (listaJugadores.hasNext()){
            Jugadores jugador = listaJugadores.next();
            System.out.printf("%d: %s, %s, %s %n",i++,jugador.getNombre(), jugador.getDeporte(),jugador.getCiudad(),jugador.getEdad());
        }
        odb.close();

    }

}
