
public class EncriptorTool {
	private String alfabetoMAY = "ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ";
	private String alfabetoMIN = "abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz";

	private String alfabetoMAY_C = "ZYXWVUTSRQPONMLKJIHGFEDCBAZYXWVUTSRQPONMLKJIHGFEDCBA";
	private String alfabetoMIN_C = "zyxwvutsrqponmlkjihgfedcbazyxwvutsrqponmlkjihgfedcba";

	public String Cifrar(String texto, int movs) {

		char[] result = new char[texto.length()];

		for (int i = 0; i < texto.length(); i++) {

			for (int e = 0; e < alfabetoMAY.length(); e++) {

				if (texto.charAt(i) == alfabetoMAY.charAt(e)) {
					result[i] = moverPosicionMAY(e, movs, "encrypt");
					break;
				}
			}
		}

		for (int i = 0; i < texto.length(); i++) {

			for (int e = 0; e < alfabetoMIN.length(); e++) {

				if (texto.charAt(i) == alfabetoMIN.charAt(e)) {
					result[i] = moverPosicionMIN(e, movs, "encrypt");
					break;
				}
			}
		}

		String cadResultado = String.copyValueOf(result);
		return cadResultado;

	}

	public String Descifrar(String texto, int movs) {
		char[] result = new char[texto.length()];

		for (int i = 0; i < texto.length(); i++) {

			for (int e = 0; e < alfabetoMAY_C.length(); e++) {

				if (texto.charAt(i) == alfabetoMAY_C.charAt(e)) {
					result[i] = moverPosicionMAY(e, movs, "decrypt");
					break;
				}
			}

		}

		for (int i = 0; i < texto.length(); i++) {

			for (int e = 0; e < alfabetoMIN_C.length(); e++) {

				if (texto.charAt(i) == alfabetoMIN_C.charAt(e)) {
					result[i] = moverPosicionMIN(e, movs, "decrypt");
					break;
				}
			}
		}

		String cadResultado = String.copyValueOf(result);
		return cadResultado;

	}

	public char moverPosicionMAY(int pos, int movs, String operacion) {

		char resultado = 0;
		int posParaExtraer = 0;
		posParaExtraer = (pos + movs);
		switch (operacion) {
		case "encrypt":

			resultado = alfabetoMAY.charAt(posParaExtraer);
			break;

		case "decrypt":
			resultado = alfabetoMAY_C.charAt(posParaExtraer);
			break;
		}

		return resultado;
	}

	public char moverPosicionMIN(int pos, int movs, String operacion) {
		char resultado = 0;
		int posParaExtraer = 0;
		posParaExtraer = (pos + movs);
		switch (operacion) {
		case "encrypt":

			resultado = alfabetoMIN.charAt(posParaExtraer);
			break;

		case "decrypt":
			resultado = alfabetoMIN_C.charAt(posParaExtraer);
			break;
		}

		return resultado;
	}

}
