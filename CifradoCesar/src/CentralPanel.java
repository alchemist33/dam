import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JSplitPane;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

public class CentralPanel {

	private JFrame frmCifradoCesar;
	private JTextField textNormal;
	private JTextField textCifrado;
	private JTextField textNormal_res;
	private JTextField textCifrado_res;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CentralPanel window = new CentralPanel();
					window.frmCifradoCesar.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public CentralPanel() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmCifradoCesar = new JFrame();
		frmCifradoCesar.setTitle("Cifrado Cesar");
		frmCifradoCesar.setBounds(100, 100, 619, 172);
		frmCifradoCesar.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmCifradoCesar.getContentPane().setLayout(null);

		textNormal = new JTextField();
		textNormal.setBounds(101, 43, 152, 20);
		frmCifradoCesar.getContentPane().add(textNormal);
		textNormal.setColumns(10);

		JLabel lblNewLabel = new JLabel("Texto sin cifrar");
		lblNewLabel.setFont(new Font("Calibri", Font.PLAIN, 11));
		lblNewLabel.setBounds(12, 46, 79, 14);
		frmCifradoCesar.getContentPane().add(lblNewLabel);

		JLabel lblTextoCifrado = new JLabel("Texto cifrado");
		lblTextoCifrado.setFont(new Font("Calibri", Font.PLAIN, 11));
		lblTextoCifrado.setBounds(12, 92, 69, 14);
		frmCifradoCesar.getContentPane().add(lblTextoCifrado);

		textCifrado = new JTextField();
		textCifrado.setColumns(10);
		textCifrado.setBounds(101, 89, 152, 20);
		frmCifradoCesar.getContentPane().add(textCifrado);

		JSpinner movimientos = new JSpinner();
		movimientos.setModel(new SpinnerNumberModel(0, 0, 25, 1));
		movimientos.setBounds(553, 11, 40, 20);
		frmCifradoCesar.getContentPane().add(movimientos);

		JLabel lblNmeroDeMovimientos = new JLabel("Numero de movimientos\r\n");
		lblNmeroDeMovimientos.setFont(new Font("Calibri", Font.PLAIN, 11));
		lblNmeroDeMovimientos.setBounds(420, 15, 123, 14);
		frmCifradoCesar.getContentPane().add(lblNmeroDeMovimientos);

		JButton btnCifrar = new JButton("Cifrar");
		btnCifrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				EncriptorTool et = new EncriptorTool();
				textNormal_res.setText(et.Cifrar(textNormal.getText(), Integer.parseInt(movimientos.getValue().toString())));

			}
		});
		btnCifrar.setFont(new Font("Calibri", Font.ITALIC, 12));
		btnCifrar.setBounds(263, 42, 89, 23);
		frmCifradoCesar.getContentPane().add(btnCifrar);

		JButton btnDescrifrar = new JButton("Descrifrar");
		btnDescrifrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				EncriptorTool et = new EncriptorTool();
				textCifrado_res.setText(et.Descifrar(textCifrado.getText(), Integer.parseInt(movimientos.getValue().toString())));

			}
		});
		btnDescrifrar.setFont(new Font("Calibri", Font.ITALIC, 12));
		btnDescrifrar.setBounds(263, 88, 89, 23);
		frmCifradoCesar.getContentPane().add(btnDescrifrar);

		JLabel label = new JLabel("Texto cifrado");
		label.setFont(new Font("Calibri", Font.PLAIN, 11));
		label.setBounds(362, 46, 69, 14);
		frmCifradoCesar.getContentPane().add(label);

		JLabel label_1 = new JLabel("Texto sin cifrar");
		label_1.setFont(new Font("Calibri", Font.PLAIN, 11));
		label_1.setBounds(362, 92, 76, 14);
		frmCifradoCesar.getContentPane().add(label_1);

		textNormal_res = new JTextField();
		textNormal_res.setEditable(false);
		textNormal_res.setColumns(10);
		textNormal_res.setBounds(441, 43, 152, 20);
		frmCifradoCesar.getContentPane().add(textNormal_res);

		textCifrado_res = new JTextField();
		textCifrado_res.setEditable(false);
		textCifrado_res.setColumns(10);
		textCifrado_res.setBounds(441, 89, 152, 20);
		frmCifradoCesar.getContentPane().add(textCifrado_res);

	}
}
