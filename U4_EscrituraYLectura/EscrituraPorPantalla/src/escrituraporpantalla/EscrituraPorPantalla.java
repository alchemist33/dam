/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package escrituraporpantalla;

import java.io.IOException;

/**
 *
 * @author marco
 */
public class EscrituraPorPantalla {

    /**
     * @param args the command line arguments
     * @throws java.io.IOException
     */
    public static void main(String[] args) throws IOException {
        // TODO code application logic here
        int c;
        int contador=0;
        while((c=System.in.read()) != '\n'){
            contador++;
            System.out.print((char)c);
        }
         System.out.println();
         System.err.println("Contados " + contador + " bytes en total");
    }
    
}
