/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package escrituraarchivobyteabyte;


import java.io.FileInputStream;
//import java.io.FileOutputStream;
import java.io.IOException;

/**
 *
 * @author marco
 */
public class EscrituraArchivoByteAByte {

    /**
     * @param args the command line arguments
     * @throws java.io.IOException
     */
    public static void main(String[] args) throws IOException {
        // TODO code application logic here
        FileInputStream in = null;
//        FileOutputStream out = null;
        try{
            in = new FileInputStream("datos.dat");
            //out = new FileOutputStream("copiadatos.dat");
            int c;
            while ((c=in.read()) != -1){
                System.out.print((char)c);
               
            }System.out.println();
        }finally{
            if(in != null){
                in.close();
            }
//            if(out != null){
//                out.close();
//            }
        }
    }
    
}
