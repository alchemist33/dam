/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lecturadesdeteclado;

import java.io.IOException;

/**
 *
 * @author marco
 */
public class LecturaDesdeTeclado {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        int byteLeido1,byteLeido2;
        char charLeido1,charLeido2;
        try {
             byteLeido1 = System.in.read();
             byteLeido2 = System.in.read();
             System.out.println("El byte1 leido: " + byteLeido1);
             charLeido1 = (char) byteLeido1;
             System.out.println("El char1 leido: " + charLeido1);
             System.out.println("El byte2 leido: " + byteLeido2);
             charLeido2 = (char) byteLeido2;
             System.out.println("El char2 leido: " + charLeido2);
             
        }catch(IOException ex){
            System.out.println("Ha ocurrido un error:");
            System.out.println(ex.getMessage());
        }
       
        
    }
    
}
