/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package accesobasesdedatos;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author marco
 */
public class ModificacionDatos {

    //PoolConnection pc = new PoolConnection();
    //Connection con = pc.crearConexion();
    //Connection con = pc.conexion;
    public void modificaTablas() {

        try {
            Connection conex = (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/ejmplo", "ejemplo", "ejemplo");

            Statement sta = conex.createStatement();
            //sta.executeUpdate("ALTER TABLE departamentos ADD djefe VARCHAR (20)");
            sta.executeUpdate("DROP TABLE prueba");
            sta.close();
            conex.close();

        } catch (SQLException ex) {
            Logger.getLogger(ResultSetInfo.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void Insertar() {
        try {
            Connection conex = (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/ejemplo", "ejemplo", "ejemplo");

            Statement sta = conex.createStatement();

            sta.executeUpdate("INSERT INTO departamentos VALUES(40,'AT. CLIENTE','SEVILLA','')");
            sta.close();
            conex.close();

        } catch (SQLException ex) {
            System.out.println("Error al insertar departamento");
            Logger.getLogger(ResultSetInfo.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void ExcuteSelect() {
        try {
            Connection conex = (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/ejemplo", "ejemplo", "ejemplo");

            Statement sta = conex.createStatement();
            String query = "SELECT * FROM departamentos";
            boolean valor = sta.execute(query);

            if (valor) {
                ResultSet rs = sta.getResultSet();
                while (rs.next()) {
                    System.out.printf("%d, %s, %s %n", rs.getInt(1), rs.getString(2), rs.getString(3));
                }
                rs.close();
            } else {
                int f = sta.getUpdateCount();
                System.out.printf("Filas afectadas: %d", f);
            }
            sta.close();
            conex.close();
        } catch (SQLException SQLex) {
            Logger.getLogger(ResultSetInfo.class.getName()).log(Level.SEVERE, null, SQLex);
        }
    }

    public void ExcuteUpdate() {
        try {
            Connection conex = (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/ejemplo", "ejemplo", "ejemplo");

            Statement sta = conex.createStatement();
            //String query = "UPDATE departamentos SET dnombre = 'Ventas' WHERE dept_no =10";
            String query = "UPDATE departamentos SET dnombre = 'VENTAS' WHERE dept_no =10";
            boolean valor = sta.execute(query);

            if (valor) {
                ResultSet rs = sta.getResultSet();
                while (rs.next()) {
                    System.out.printf("%d, %s, %s %n", rs.getInt(1), rs.getString(2), rs.getString(3));
                }
                rs.close();
            } else {
                int f = sta.getUpdateCount();
                System.out.printf("Filas afectadas: %d %n", f);
            }
            sta.close();
            conex.close();
        } catch (SQLException SQLex) {
            Logger.getLogger(ResultSetInfo.class.getName()).log(Level.SEVERE, null, SQLex);
        }
    }

    public void ejecutarScritpMySQL() {
        File scriptFile = new File("./src/script/scriptmysql.sql");
        System.out.println("---------------------------------------------");
        System.out.println("\n\nLeyendo script: " + scriptFile.getName());

        BufferedReader lector = null;
        try {
            lector = new BufferedReader(new FileReader(scriptFile));
        } catch (FileNotFoundException fnex) {
            Logger.getLogger(ResultSetInfo.class.getName()).log(Level.SEVERE, null, fnex);
        }

        String linea = null;
        StringBuilder stringBuilder = new StringBuilder();
        String saltolinea = System.getProperty("line.separator");
        try {
            while ((linea = lector.readLine()) != null) {
                stringBuilder.append(linea);
                stringBuilder.append(saltolinea);
            }
        } catch (IOException ioex) {
            Logger.getLogger(ResultSetInfo.class.getName()).log(Level.SEVERE, null, ioex);
        }

        String consulta = stringBuilder.toString();

        try {
            Connection conex = (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/ejemplo?allowMultiQueries=true", "ejemplo", "ejemplo");

            Statement sta = conex.createStatement();
            int result = sta.executeUpdate(consulta);

            System.out.println("---------------------------------------------");
            conex.close();
            sta.close();
            System.out.println("Script ejecutado, resultado:" + result);
        } catch (SQLException ioex) {
            System.err.printf("Error, no se pudo ejecutar el script %n" + ioex.getMessage());
            Logger.getLogger(ResultSetInfo.class.getName()).log(Level.SEVERE, null, ioex);
        }
    }

    public void Consulta_preparedStatement() {
        PoolConnection pc = new PoolConnection();
        Connection con = pc.crearConexion();
        String query = "SELECT * FROM departamentos WHERE dnombre like ?";

        try {
            PreparedStatement pst = con.prepareStatement(query);
            pst.setString(1, "A%");
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                System.out.printf("Nº departamento:  %d, Nombre: %s, Localidad; %s %n",
                        rs.getInt("dept_no"), rs.getString("dnombre"), rs.getString("loc"));
            }
            System.out.println("---------------------------------------------");
            pst.setString(1, "P%");
            rs = pst.executeQuery();
            while (rs.next()) {
                System.out.printf("Nº departamento:  %d, Nombre: %s, Localidad; %s %n",
                        rs.getInt("dept_no"), rs.getString("dnombre"), rs.getString("loc"));
            }
            rs.close();
            pst.close();
            con.close();

        } catch (SQLException ex) {
            Logger.getLogger(ModificacionDatos.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void Consulta_InsertdStatement() {

        PoolConnection pc = new PoolConnection();
        Connection con = pc.crearConexion();

        PreparedStatement pst;
        try {
            String query = "INSERT INTO departamentos VALUES (?,?,?,?)";
            String args[] = {"50", "DISTRIBUCION", "MADRID", null};
            String dept_no = args[0];
            String nombre = args[1];
            String loc = args[2];

            pst = con.prepareStatement(query);

            pst.setInt(1, Integer.parseInt(dept_no));
            pst.setString(2, nombre);
            pst.setString(3, loc);
            pst.setNull(4, 1);

            int filas;
            filas = pst.executeUpdate();
            System.out.printf("Filas Insertadas: %d %n", filas);

            pst.close();
            con.close();
        } catch (SQLException e) {
            Logger.getLogger(ModificacionDatos.class.getName()).log(Level.SEVERE, null, e);
        }

    }

    public void procedimientoCallableStatement() {
        PoolConnection pc = new PoolConnection();
        Connection con = pc.crearConexion();
        CallableStatement cst;
        try {
            cst = con.prepareCall("CALL BUSCA_DEPARTAMENTOS(?)");
            cst.setString(1, "A%");

            ResultSet rs = cst.executeQuery();

            while (rs.next()) {
                System.out.printf("Nº departamento:  %d, Nombre: %s, Localidad; %s %n",
                        rs.getInt("dept_no"), rs.getString("dnombre"), rs.getString("loc"));
            }
            rs.close();
            cst.close();
            con.close();

        } catch (SQLException ex) {
            Logger.getLogger(ModificacionDatos.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void funcionCallableStatement() {

        try {
            PoolConnection pc = new PoolConnection();
            Connection con = pc.crearConexion();
            int num1 = 4;
            int num2 = 9;
            String sql = "{? = call SUMAR(?,?)}";
            CallableStatement cst = con.prepareCall(sql);

            cst.registerOutParameter(1, Types.VARCHAR);

            cst.setInt("NUM1", num1);
            cst.setInt("NUM2", num2);

            cst.executeUpdate();

            System.out.println("Resultado de la suma: " + cst.getInt(1));

            cst.close();
            con.close();

        } catch (SQLException ex) {
            Logger.getLogger(ModificacionDatos.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void insertarCommit() {
        PoolConnection pc = new PoolConnection();
        Connection con = pc.crearConexion();
        try {

            con.setAutoCommit(false);
            Statement st;
            st = con.createStatement();
            st.executeUpdate("INSERT INTO departamentos VALUES(60,'RRHH','VALLADOLID',null) ");
            st.executeUpdate("INSERT INTO departamentos VALUES(70,'DIRECION','ALICANTE',null) ");
            con.commit();
            System.out.println("Commit success!");
        } catch (SQLException Sqlex) {
            System.out.println("Commit error!");
            Logger.getLogger(ModificacionDatos.class.getName()).log(Level.SEVERE, null, Sqlex);
            try {
                if (con != null) {
                     
                    con.rollback();
                    System.out.println("RollBack success!");
                    con.close();
                }
            } catch (SQLException Sqlex2) {
                System.out.println("RollBack error!");
                  Logger.getLogger(ModificacionDatos.class.getName()).log(Level.SEVERE, null, Sqlex2);
            }
        }
    }
}
