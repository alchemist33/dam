package accesobasesdedatos;

import java.sql.Connection;
import java.sql.SQLException;
import org.apache.commons.dbcp.BasicDataSource;

/**
 *
 * @author marco
 */
public class PoolConnection {

    public Connection crearConexion() {

        Connection conexion = null;

        BasicDataSource bdSource = new BasicDataSource();
        //bdSource.setDriverClassName("com.mysql.jdbc.Driver");
        
        
        bdSource.setUrl("jdbc:mysql://localhost:3306/ejemplo");
        //bdSource.setUrl("jdbc:mysql://localhost:3306/ejemplo/AllowMultiQueries=true");
        bdSource.setUsername("ejemplo");
        bdSource.setPassword("ejemplo");
        
        
        Connection con = null;
        try {
            if (conexion != null) {
                System.err.println("Error : No se puede crear una nueva conexion");
            } else {
                con = bdSource.getConnection();
                System.out.println("Conexion Creada!");
            }
        } catch (SQLException e) {
            System.err.println("Error: " + e.toString());
        } 

        return con;
    }

}


