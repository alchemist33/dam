package accesobasesdedatos;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import hibernate.HibernateOps;

/**
 *
 * @author marco
 */
public class AccesoBasesDeDatos {

    /**
     * @param args the command line arguments
     */
    private static final DbInfo dbInfo = new DbInfo();
    private static final ResultSetInfo RsInfo = new ResultSetInfo();
    private static final ModificacionDatos ModData = new ModificacionDatos();
    private static final HibernateOps HbOps = new HibernateOps();

    public static void main(String[] args) {
        //consultaDepartamentos();
        //NumRegsDepartamentos();
        //accesoJDBC_ODBC();
        //accesoJDBC_SQLITE();
        //accesoJDBC_ApacheDerby();
        //accesoJDBC_ORACLE();
        //dbInfo.obtenInfoDb();
        //ResultInfo();
        //ModificaDatos();
        HibernateOps();
    }

    private static void consultaDepartamentos() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection conex = DriverManager.getConnection("jdbc:mysql://localhost:3306/ejemplo", "ejemplo", "ejemplo");
            Statement sentencia = conex.createStatement();
            String sql = "SELECT * FROM departamentos";
            ResultSet resul = sentencia.executeQuery(sql);

            while (resul.next()) {
                System.out.printf("%d, %s, %s, %n", resul.getInt(1), resul.getString(2), resul.getString(3));
            }
            resul.close();
            sentencia.close();
            conex.close();
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(AccesoBasesDeDatos.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private static void NumRegsDepartamentos() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection conex = DriverManager.getConnection("jdbc:mysql://localhost:3306/ejemplo", "ejemplo", "ejemplo");
            Statement sentencia = conex.createStatement();
            String sql = "SELECT * FROM departamentos";
            ResultSet resul = sentencia.executeQuery(sql);

            resul.last();
            System.out.println("Numero de filas: " + resul.getRow());

            resul.beforeFirst();
            while (resul.next()) {
                System.out.printf("Fila: %d, Nºdeparatamento: %d, Nombre departamento: %s, Ciudad departamento:%s, %n",
                        resul.getRow(), resul.getInt(1), resul.getString(2), resul.getString(3));
            }

            resul.close();
            sentencia.close();
            conex.close();
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(AccesoBasesDeDatos.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private static void accesoJDBC_ODBC() {

        try {
            Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
            Connection conex = DriverManager.getConnection("jdbc:odbc:MySQL-ODBC");
            Statement sentencia = conex.createStatement();
            String sql = "SELECT * FROM departamentos";
            ResultSet resul = sentencia.executeQuery(sql);

            while (resul.next()) {
                System.out.printf("%d, %s, %s, %n", resul.getInt(1), resul.getString(2), resul.getString(3));
            }
            resul.close();
            sentencia.close();
            conex.close();
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(AccesoBasesDeDatos.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private static void accesoJDBC_SQLITE() {

        try {
            Class.forName("org.sqlite.JDBC");
            Connection conex = DriverManager.getConnection("jdbc:sqlite:D:/DB/SQLITE/ejemplo.db");
            Statement sentencia = conex.createStatement();
            String sql = "SELECT * FROM departamentos";
            ResultSet resul = sentencia.executeQuery(sql);

            while (resul.next()) {
                System.out.printf("%d, %s, %s, %n", resul.getInt(1), resul.getString(2), resul.getString(3));
            }
            resul.close();
            sentencia.close();
            conex.close();
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(AccesoBasesDeDatos.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private static void accesoJDBC_ApacheDerby() {
        try {
            Class.forName("org.apache.derby.jdbc.EmbeddedDriver");
            Connection conex = DriverManager.getConnection("jdbc:derby:D:/Software/db-derby-10.14.2.0-bin/db-derby-10.14.2.0-bin/bin/ejemplo");
            Statement sentencia = conex.createStatement();
            String sql = "SELECT * FROM departamentos";
            ResultSet resul = sentencia.executeQuery(sql);

            while (resul.next()) {
                System.out.printf("%d, %s, %s, %n", resul.getInt(1), resul.getString(2), resul.getString(3));
            }
            resul.close();
            sentencia.close();
            conex.close();
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(AccesoBasesDeDatos.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private static void accesoJDBC_ORACLE() {
        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
            Connection conex = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1522:DAM", "system", "Madrid.15");
            Statement sentencia = conex.createStatement();
            String sql = "SELECT * FROM Table1";
            ResultSet resul = sentencia.executeQuery(sql);

            while (resul.next()) {
                System.out.printf("%s %n", resul.getString(1));
            }
            resul.close();
            sentencia.close();
            conex.close();
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(AccesoBasesDeDatos.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private static void ResultInfo() {
        //RsInfo.ResultInfo();

    }

    private static void ModificaDatos() {

        //ModData.modificaTablas();
        //ModData.Insertar();
        //ModData.ExcuteSelect();
        //ModData.ExcuteUpdate();
        //ModData.ejecutarScritpMySQL();
        //ModData.Consulta_preparedStatement();
        //ModData.Consulta_InsertdStatement();
        //ModData.procedimientoCallableStatement();
        //ModData.funcionCallableStatement();
        //ModData.insertarCommit();
    }

    private static void HibernateOps() {
       //HbOps.getDepartamento();
       //HbOps.getDepartamentoV2();
       //HbOps.getEmpleadosDep();
       //HbOps.insertaDepartamento();
       //HbOps.insertaEmpleado();
       //HbOps.updateEmpleado();
       //HbOps.borraDepartamento();
       //HbOps.HQL_getDepartamentos();
       //HbOps.HQL_getDepartamentosV2();
       //HbOps.HQL_getEmpleadpsDep();
       //HbOps.HQL_uniqueResult();
       //HbOps.HQL_consultaConParametros();
       //HbOps.HQL_consultaConParametrosR2();
       //HbOps.HQL_consultaConParametrosPosicionales();
       //HbOps.HQL_consultaConParametrosPosicionalesR2();
       //HbOps.HQL_funcionesDeGrupo();
       //HbOps.HQL_funcionesDeGrupoR2();
       //HbOps.HQL_ObjectosEnConsultas();
       //HbOps.HQL_ObjectosEnConsultasR2();
       //HbOps.HQL_Update();
       //HbOps.HQL_Insert();
       //HbOps.HQL_UnionesYAsociaciones();
       HbOps.HQL_UnionesYAsociacionesR2();
    }
}
