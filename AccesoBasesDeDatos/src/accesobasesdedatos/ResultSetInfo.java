package accesobasesdedatos;

/**
 *
 * @author marco
 */
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ResultSetInfo {

    

    public void ResultInfo() throws ClassNotFoundException {
        PoolConnection pc = new PoolConnection();
    Connection con = pc.crearConexion();
        try {
            Statement sentencia = con.createStatement();
            ResultSet rs = sentencia.executeQuery("SELECT * FROM departamentos");
            ResultSetMetaData rsmd = rs.getMetaData();

            int nColumnas = rsmd.getColumnCount();
            String nula;
            System.out.printf("Numero de columnas recuperadas: %d %n", nColumnas);

            for (int i = 1; i <= nColumnas; i++) {
                System.out.printf("-Columna %d %n", i);
                System.out.printf(" ·Nombre: %s %n ·Tipo: %s %n", rsmd.getColumnName(i), rsmd.getColumnTypeName(i));

                if (rsmd.isNullable(i) == 0) {
                    nula = "NO";
                } else {
                    nula = "SI";
                }
                System.out.printf(" ·Puede ser nula %s %n", nula);
                System.out.printf(" ·Maximo ancho de columna %d %n", rsmd.getColumnDisplaySize(i));
            }
            sentencia.close();
            rs.close();
            con.close();

        } catch (SQLException ex) {
            Logger.getLogger(ResultSetInfo.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
