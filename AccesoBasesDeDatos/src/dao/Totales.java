/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

/**
 *
 * @author marco
 */
public class Totales {

    private Long cuenta; // numero de empleados
    private Byte numero; // numero de departamento
    private Double media; // media de salario
    private String nombre; // nombre de departamento

    public Totales() {
    }

    public Totales(Byte numero, Long cuenta, Double media, String nombre) {
        this.cuenta = cuenta;
        this.numero = numero;
        this.media = media;
        this.nombre = nombre;
    }

    public Long getCuenta() {
        return cuenta;
    }

    public void setCuenta(Long cuenta) {
        this.cuenta = cuenta;
    }

    public Byte getNumero() {
        return numero;
    }

    public void setNumero(Byte numero) {
        this.numero = numero;
    }

    public Double getMedia() {
        return media;
    }

    public void setMedia(Double media) {
        this.media = media;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

}
