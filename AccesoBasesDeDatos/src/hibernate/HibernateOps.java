package hibernate;

import accesobasesdedatos.ResultSetInfo;
import ejemplo.Departamentos;
import ejemplo.Empleados;
import ejemplo.HibernateUtil;
import dao.Totales;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.HibernateException;
import org.hibernate.ObjectNotFoundException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.TransientPropertyValueException;
import org.hibernate.exception.ConstraintViolationException;

/**
 *
 * @author marco
 */
public class HibernateOps {

    public void getDepartamento() {
        SessionFactory factory = HibernateUtil.getSessionFactory();
        Session sesion = factory.openSession();

        Departamentos dep = new Departamentos();

        try {

            //dep = (Departamentos) sesion.load(Departamentos.class, (byte) 10);
            dep = (Departamentos) sesion.load("ejemplo.Departamentos", (byte) 10);
            System.out.printf("Nombre: %s%n", dep.getDnombre());
            System.out.printf("Localidad: %s%n", dep.getLoc());
        } catch (ObjectNotFoundException e) {
            System.err.println("Error: No existe el departamento");
        }
        sesion.close();
        System.exit(0);

    }

    public void getDepartamentoV2() {
        SessionFactory factory = HibernateUtil.getSessionFactory();
        Session sesion = factory.openSession();

        Departamentos dep = new Departamentos();

        try {

            //dep = (Departamentos) sesion.load(Departamentos.class, (byte) 10);
            dep = (Departamentos) sesion.load("ejemplo.Departamentos", (byte) 20);

            if (dep == null) {
                System.err.println("El departamento no existe");
            } else {
                System.out.printf("Nombre: %s%n", dep.getDnombre());
                System.out.printf("Localidad: %s%n", dep.getLoc());
            }

            dep = (Departamentos) sesion.load("ejemplo.Departamentos", (byte) 11);

            if (dep == null) {
                System.err.println("El departamento no existe");
            } else {
                System.out.printf("Nombre: %s%n", dep.getDnombre());
                System.out.printf("Localidad: %s%n", dep.getLoc());
            }

        } catch (ObjectNotFoundException e) {
            System.err.println("Error: No existe el departamento");
        }
        sesion.close();
        System.exit(0);

    }

    public void getEmpleadosDep() {
        try {
            SessionFactory factory = HibernateUtil.getSessionFactory();
            Session sesion = factory.openSession();

            System.out.println("=========================================");
            Departamentos departamento = new Departamentos();

            departamento = (Departamentos) sesion.load(Departamentos.class, (byte) 20);
            System.out.println("Nombre:" + departamento.getDnombre());
            System.out.println("Localidad:" + departamento.getLoc());
            System.out.println("=========================================");
            System.out.println("EMPLEADOS DEL DEPARTAMENTO");

            Set<Empleados> listaEmpleados = departamento.getEmpleadoses();
            Iterator<Empleados> it = listaEmpleados.iterator();
            System.out.printf("Numero de empleados: %d %n", listaEmpleados.size());
            while (it.hasNext()) {
                Empleados empleado = it.next();
                System.out.printf("Puesto: %s, Salario: %.2f %n", empleado.getPuesto(), empleado.getSalario());
            }
            System.out.println("=========================================");
            sesion.close();
            System.exit(0);

        } catch (HibernateException HbEx) {
            Logger.getLogger(ResultSetInfo.class.getName()).log(Level.SEVERE, null, HbEx);
        }

    }

    public void insertaDepartamento() {
        SessionFactory factory = HibernateUtil.getSessionFactory();
        Session sesion = factory.openSession();
        try {

            Transaction transaction = sesion.beginTransaction();

            System.out.printf("Insertamos una fila en la tabla DEPARTAMENTOS...%n");

            Departamentos departamento = new Departamentos();
            departamento.setDeptNo((byte) 11);
            departamento.setDnombre("COMERCIAL");
            departamento.setLoc("VALENCIA");
            departamento.setDjefe(null);

            sesion.save(departamento);
            transaction.commit();
            System.out.printf("Insert finalizado %nCommit Succes!! %n");

        } catch (ConstraintViolationException HbEx) {

            System.out.println("Error: El deparamento ya exite");
            System.out.println("Mensaje: " + HbEx.getMessage());
            System.out.println("Codigo de error: " + HbEx.getErrorCode());
            System.out.println("SQL: " + HbEx.getSQLException().getMessage());
            System.out.println("=========================================");
            Logger.getLogger(ResultSetInfo.class.getName()).log(Level.SEVERE, null, HbEx);

        }
        sesion.close();
        System.exit(0);
    }

    public void insertaEmpleado() {
        SessionFactory factory = HibernateUtil.getSessionFactory();
        Session sesion = factory.openSession();
        try {

            Transaction transaction = sesion.beginTransaction();

            System.out.printf("Insertamos una fila en la tabla Empleados...%n");

            Empleados em = new Empleados();
            em.setEmpNo((byte) 11);
            em.setPuesto("COMERCIAL");
            Float salario = new Float(1700);
            em.setSalario(salario);

            Departamentos d = new Departamentos();
            d.setDeptNo((byte) 12);
            em.setDepartamentos(d);

            sesion.save(em);
            try {
                transaction.commit();
                System.out.printf("Insert finalizado %nCommit Succes!! %n");

            } catch (ConstraintViolationException CnEx) {

                System.out.println("Error: Empleado duplicado");
                System.out.println("Mensaje: " + CnEx.getMessage());
                System.out.println("Codigo de error: " + CnEx.getErrorCode());
                System.out.println("SQL: " + CnEx.getSQLException().getMessage());
                System.out.println("=========================================");
                Logger.getLogger(ResultSetInfo.class.getName()).log(Level.SEVERE, null, CnEx);
            }

        } catch (TransientPropertyValueException Ex) {
            System.out.println("Error: El departamento no existe");
            Logger.getLogger(ResultSetInfo.class.getName()).log(Level.SEVERE, null, Ex);
        }
        sesion.close();
        System.exit(0);
    }

    public void updateEmpleado() {

        SessionFactory factory = HibernateUtil.getSessionFactory();
        Session sesion = factory.openSession();
        try {

            Transaction transaction = sesion.beginTransaction();

            System.out.printf("Actualizamos una fila en la tabla Empleados...%n");

            Empleados em = new Empleados();

            em = (Empleados) sesion.load(Empleados.class, (short) 1);
            System.out.printf("Modificamos el empleado: %d %n", em.getEmpNo());
            System.out.printf("Salario anterior: %.2f %n", em.getSalario());
            System.out.printf("Departamento anterior: %s %n", em.getDepartamentos().getDnombre());

            float nuevoSalario = em.getSalario() + 0;
            em.setSalario(nuevoSalario);

            Departamentos dep = (Departamentos) sesion.get(Departamentos.class, (byte) 11);
            if (dep == null) {
                System.out.printf("El departamento no existe");
            } else {
                em.setDepartamentos(dep);
                sesion.update(em);
            }
            try {
                transaction.commit();
                System.out.printf("Insert finalizado %nCommit Succes!! %n");
                System.out.println("Salario nuevo:" + em.getSalario());
                System.out.println("Departamento nuevo: " + em.getDepartamentos().getDnombre());

            } catch (ConstraintViolationException CnEx) {

                System.out.println("Error: No existe el departamento");
                Logger.getLogger(ResultSetInfo.class.getName()).log(Level.SEVERE, null, CnEx);
            }

        } catch (ObjectNotFoundException ObEx) {
            System.out.println("Error: No existe el empleado");
            Logger.getLogger(ResultSetInfo.class.getName()).log(Level.SEVERE, null, ObEx);
        } catch (Exception ex) {
            Logger.getLogger(ResultSetInfo.class.getName()).log(Level.SEVERE, null, ex);
        }
        sesion.close();
        System.exit(0);

    }

    public void borraDepartamento() {

        SessionFactory factory = HibernateUtil.getSessionFactory();
        Session sesion = factory.openSession();
        try {
            Transaction transaction = sesion.beginTransaction();
            Departamentos departamento = (Departamentos) sesion.load(Departamentos.class, (byte) 10);

            sesion.delete(departamento);
            transaction.commit();
            System.out.println("Departamento eliminado");

        } catch (ObjectNotFoundException ObEx) {
            System.out.println("No exite el departamento");
        } catch (ConstraintViolationException CnEx) {
            System.out.println("Error: No  se puede borrar, el departamento tiene empleados");
        } catch (Exception ex) {
            System.err.println("ERROR:");
            ex.printStackTrace();
        }
        sesion.close();
        System.exit(0);
    }

    public void HQL_getDepartamentos() {
        SessionFactory factory = HibernateUtil.getSessionFactory();
        Session sesion = factory.openSession();
        try {

            Query q = sesion.createQuery("from Departamentos");
            List<Departamentos> lista = q.list();

            Iterator<Departamentos> iterator = lista.iterator();

            System.out.printf("Numero de regsitros: %d %n", lista.size());
            while (iterator.hasNext()) {
                Departamentos departamento = (Departamentos) iterator.next();
                System.out.printf("%d- %s, %s %n", departamento.getDeptNo(), departamento.getDnombre(), departamento.getLoc());
            }

        } catch (HibernateException e) {
            sesion.close();
            System.exit(0);
            e.getMessage();
        }
        sesion.close();
        System.exit(0);

    }

    public void HQL_getDepartamentosV2() {
        SessionFactory factory = HibernateUtil.getSessionFactory();
        Session sesion = factory.openSession();
        try {

            Query q = sesion.createQuery("from Departamentos");
            q.setFetchSize(1);

            Iterator iterator = q.iterate();

            while (iterator.hasNext()) {

                Departamentos departamento = (Departamentos) iterator.next();

                System.out.printf("%d- %s, %s %n", departamento.getDeptNo(), departamento.getDnombre(), departamento.getLoc());
            }

        } catch (HibernateException e) {
            sesion.close();
            System.exit(0);
            e.getMessage();
        }
        sesion.close();
        System.exit(0);

    }

    public void HQL_getEmpleadpsDep() {
        SessionFactory factory = HibernateUtil.getSessionFactory();
        Session sesion = factory.openSession();
        try {

            Query q = sesion.createQuery("from Empleados as e where e.departamentos.deptNo = 11");
            List<Empleados> lista = q.list();

            Iterator<Empleados> iterator = lista.iterator();

            System.out.printf("Numero de regsitros: %d %n", lista.size());
            while (iterator.hasNext()) {
                Empleados emp = (Empleados) iterator.next();
                System.out.printf("%s, %s %n", emp.getPuesto(), emp.getSalario());
            }

        } catch (HibernateException e) {
            sesion.close();
            System.exit(0);
            e.getMessage();
        }
        sesion.close();
        System.exit(0);

    }

    public void HQL_uniqueResult() {

        SessionFactory factory = HibernateUtil.getSessionFactory();
        Session sesion = factory.openSession();
        String hql = "from Departamentos as dep where dep.deptNo = 20";
        try {

            Query consulta = sesion.createQuery(hql);

            Departamentos departamento = (Departamentos) consulta.uniqueResult();
            System.out.printf("%d- %s, %s %n", departamento.getDeptNo(), departamento.getDnombre(), departamento.getLoc());

            hql = "from Departamentos as dep where dep.dnombre = 'COMERCIAL'";
            consulta = sesion.createQuery(hql);

            departamento = (Departamentos) consulta.uniqueResult();
            System.out.printf("%d - %s, %s %n", departamento.getDeptNo(), departamento.getDnombre(), departamento.getLoc());

        } catch (Exception e) {

            System.out.println("Mensaje-Error:" + e.getMessage());
        }
        sesion.close();
        System.exit(0);
    }

    public void HQL_consultaConParametros() {

        SessionFactory factory = HibernateUtil.getSessionFactory();
        Session sesion = factory.openSession();
        String hql = "from Empleados where empNo = :numero";
        try {
            Query q = sesion.createQuery(hql);
            q.setParameter("numero", (short) 2);

            Empleados emp = (Empleados) q.uniqueResult();
            System.out.printf("%s, %s %n", emp.getPuesto(), emp.getSalario());
        } catch (Exception e) {
            System.out.println("Mensaje-Error:" + e.getMessage());
        }
        sesion.close();
        System.exit(0);
    }

    public void HQL_consultaConParametrosR2() {

        SessionFactory factory = HibernateUtil.getSessionFactory();
        Session sesion = factory.openSession();
        String hql = "from Empleados as emp where emp.departamentos.deptNo = :numero and emp.puesto = :puesto";
        try {
            Query q = sesion.createQuery(hql);
            q.setParameter("numero", (byte) 11);
            q.setParameter("puesto", "COMERCIAL");

            List<Empleados> LstEmp = q.list();
            Iterator iteraror = LstEmp.iterator();
            while (iteraror.hasNext()) {
                Empleados emp = (Empleados) iteraror.next();
                System.out.printf("%s, %s %n", emp.getEmpNo(), emp.getPuesto());
            }

        } catch (Exception e) {
            System.out.println("Mensaje-Error:" + e.getMessage());
        }
        sesion.close();
        System.exit(0);
    }

    public void HQL_consultaConParametrosPosicionales() {

        SessionFactory factory = HibernateUtil.getSessionFactory();
        Session sesion = factory.openSession();
        String hql = "from Empleados as emp where emp.departamentos.deptNo = ? and emp.puesto = ?";
        try {
            Query q = sesion.createQuery(hql);

            //q.setParameter(0, (byte) 11);
            //q.setParameter(1, "COMERCIAL");
            q.setInteger(0, 11);
            q.setString(1, "COMERCIAL");

            List<Empleados> LstEmp = q.list();
            Iterator iteraror = LstEmp.iterator();
            while (iteraror.hasNext()) {
                Empleados emp = (Empleados) iteraror.next();
                System.out.printf("%s, %s %n", emp.getEmpNo(), emp.getPuesto());
            }

        } catch (Exception e) {
            System.out.println("Mensaje-Error:" + e.getMessage());
        }
        sesion.close();
        System.exit(0);
    }

    public void HQL_consultaConParametrosPosicionalesR2() {

        SessionFactory factory = HibernateUtil.getSessionFactory();
        Session sesion = factory.openSession();
        List<Byte> numeros = new ArrayList<Byte>();
        numeros.add((byte) 11);
        numeros.add((byte) 20);
        numeros.add((byte) 30);
        String hql = "from Empleados as emp where emp.departamentos.deptNo in (:listaDep) order by emp.departamentos.deptNo";
        try {
            Query q = sesion.createQuery(hql);
            q.setParameterList("listaDep", numeros);
            List<Empleados> LstEmp = q.list();
            Iterator iteraror = LstEmp.iterator();
            while (iteraror.hasNext()) {
                Empleados emp = (Empleados) iteraror.next();
                System.out.printf("%d, %d, %s %n", emp.getDepartamentos().getDeptNo(), emp.getEmpNo(), emp.getPuesto());
            }

        } catch (Exception e) {
            System.out.println("Mensaje-Error:" + e.getMessage());
        }
        sesion.close();
        System.exit(0);
    }

    public void HQL_funcionesDeGrupo() {

        SessionFactory factory = HibernateUtil.getSessionFactory();
        Session sesion = factory.openSession();
        String hql = "select avg(emp.salario) from Empleados as emp";
        Query q = sesion.createQuery(hql);
        Double media = (Double) q.uniqueResult();
        System.out.printf("Salario medio: %.2f %n", media);

        sesion.close();
        System.exit(0);
    }

    public void HQL_funcionesDeGrupoR2() {

        SessionFactory factory = HibernateUtil.getSessionFactory();
        Session sesion = factory.openSession();
        String hql = "select emp.departamentos.deptNo, avg(salario), count(empNo) from Empleados as emp group by emp.departamentos.deptNo";
        try {
            Query q = sesion.createQuery(hql);

            Iterator iteraror = q.iterate();
            while (iteraror.hasNext()) {
                Object[] par = (Object[]) iteraror.next();
                Byte depart = (Byte) par[0];
                Double media = (Double) par[1];
                Long cuenta = (Long) par[2];
                System.out.printf("Dep: %d, Media: %.2f, Num. emp.:, %d %n", depart, media, cuenta);
            }

        } catch (Exception e) {
            System.out.println("Mensaje-Error:" + e.getMessage());

        }

        sesion.close();
        System.exit(0);
    }

    public void HQL_ObjectosEnConsultas() {

        SessionFactory factory = HibernateUtil.getSessionFactory();
        Session sesion = factory.openSession();
        String hql = "select dept.deptNo, count(emp.empNo), coalesce (avg(emp.salario),0), dept.dnombre from Empleados as emp right join emp.departamentos as dept "
                + "group by dept.deptNo, dept.dnombre";
        try {
            Query q = sesion.createQuery(hql);
            List<Object[]> filas = q.list();
            for (int i = 0; i < filas.size(); i++) {
                Object[] filaActual = filas.get(i);
                System.out.printf("Numero Dep: %d, Nombre: %s, Salario Medio: %.2f, num emple: %d %n",
                        filaActual[0], filaActual[3], filaActual[2], filaActual[1]);

            }
        } catch (Exception e) {
            System.out.println("Mensaje-Error:" + e.getMessage());
        }

        sesion.close();
        System.exit(0);

    }

    public void HQL_ObjectosEnConsultasR2() {

        SessionFactory factory = HibernateUtil.getSessionFactory();
        Session sesion = factory.openSession();
        String hql = "select new ejemplo.Totales(dept.deptNo, count(emp.empNo), coalesce (avg(emp.salario),0), dept.dnombre) from Empleados as emp right join emp.departamentos as dept "
                + "group by dept.deptNo, dept.dnombre";
        try {
            Query q = sesion.createQuery(hql);
            Iterator iterator = q.iterate();
            while (iterator.hasNext()) {
                Totales tot = (Totales) iterator.next();
                System.out.printf("Numero Dep: %d, Nombre: %s, Salario Medio: %.2f, num emple: %d %n",
                        tot.getNumero(), tot.getNombre(), tot.getMedia(), tot.getCuenta());
            }

        } catch (Exception e) {
            System.out.println("Mensaje-Error:" + e.getMessage());
        }

        sesion.close();
        System.exit(0);

    }

    public void HQL_Update() {

        SessionFactory factory = HibernateUtil.getSessionFactory();
        Session sesion = factory.openSession();
        Transaction tr = sesion.beginTransaction();

        String hql = "update Empleados set salario = :nuevoSal where puesto = :puesto";
        try {
            Query q = sesion.createQuery(hql);
            q.setParameter("nuevoSal", (float) 2500.34);
            q.setParameter("puesto", "COMERCIAL");
            int filaModificadas = q.executeUpdate();
            tr.commit();
            System.out.println("Filas modificadas: " + filaModificadas);
        } catch (Exception e) {
            tr.rollback();
            System.out.println("Mensaje-Error:" + e.getMessage());
        }

        sesion.close();
        System.exit(0);
    }

    public void HQL_Insert() {

        SessionFactory factory = HibernateUtil.getSessionFactory();
        Session sesion = factory.openSession();
        Transaction tr = sesion.beginTransaction();
        String hql = "insert into NuevosDepartamentos (deptNo, dnombre, loc) select n.deptNo, n.dnombre, n.loc from Departamentos as n";
        try {
            Query q = sesion.createQuery(hql);
            int filasCreadas = q.executeUpdate();
            tr.commit();
            System.out.println("Filas insertadas: " + filasCreadas);

        } catch (Exception e) {
            tr.rollback();
            System.out.println("Mensaje-Error:" + e.getMessage());
        }
        sesion.close();
        System.exit(0);
    }

    public void HQL_UnionesYAsociaciones() {
        SessionFactory factory = HibernateUtil.getSessionFactory();
        Session sesion = factory.openSession();
        String hql = "from Empleados as emp right join emp.empleacargo order by emp.empNo";
        try {
            Query q = sesion.createQuery(hql);
            Iterator iterator = q.iterate();
            while (iterator.hasNext()) {
                Object[] par = (Object[]) iterator.next();
                Empleados director = (Empleados) par[0];
                Empleados empleado = (Empleados) par[1];
                if (director != null) {
                    System.out.printf("Empleado: %d, %s, DIRECTOR: %d, %s %n", empleado.getEmpNo(), empleado.getPuesto(), director.getEmpNo(), director.getPuesto());
                } else {
                    System.out.printf("Empleado: %d, %s, SIN DIRECTOR: %n", empleado.getEmpNo(), empleado.getPuesto());
                }

            }
        } catch (Exception e) {
            System.out.println("Mensaje-Error:" + e.getMessage());
        }
        sesion.close();
        System.exit(0);
    }

    public void HQL_UnionesYAsociacionesR2() {
        SessionFactory factory = HibernateUtil.getSessionFactory();
        Session sesion = factory.openSession();
        String hql = "from Empleados";
        try {
            Query q = sesion.createQuery(hql);
            List<Empleados> lstEmp = q.list();
            Iterator iterator = lstEmp.iterator();

            while (iterator.hasNext()) {

                Empleados emp = (Empleados) iterator.next();

                if (emp != null) {
                    Set acargo = emp.getEmpleacargo();
                    if (acargo.size() == 0) {
                        System.out.printf("EMPLEADO: %d, %s %n", emp.getEmpNo(), emp.getPuesto());
                        System.out.println("======================================================");
                    } else {
                        System.out.printf("DIRECTOR: %d, %s %n", emp.getEmpNo(), emp.getPuesto());
                        System.out.println("A cargo:" + acargo.size());
                        
                        Iterator it = acargo.iterator();
                        while (it.hasNext()){
                            Empleados em = (Empleados) it.next();
                            System.out.printf("\t %d, %s %n",em.getEmpNo(),em.getPuesto());
                        }
                        System.out.println("======================================================");
                    }
                }

            }
        } catch (Exception e) {
            System.out.println("Mensaje-Error:" + e.getMessage());
        }
        sesion.close();
        System.exit(0);
    }

}
