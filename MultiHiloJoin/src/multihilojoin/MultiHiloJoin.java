/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package multihilojoin;

/**
 *
 * @author marco
 */
public class MultiHiloJoin {

    /**
     * @param args the command line arguments
     * @throws java.lang.InterruptedException
     */
    public static void main(String[] args) throws InterruptedException {
        // TODO code application logic here
          HiloPrimario hp1 = new HiloPrimario(10);
          HiloPrimario hp2 = new HiloPrimario(10);
          HiloPrimario hp3 = new HiloPrimario(10);
          
          hp1.start();
          hp2.start();
          hp3.start();
          hp1.join();
          hp2.join();
          hp3.join();
          System.out.println("Fin del programa prinicipal");
    }
    
}
