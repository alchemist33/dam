/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sockets;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

/**
 *
 * @author marco
 */
public class Sockets {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        //Server Socket
        try {
            ServerSocket skServidor = new ServerSocket(5000);
            Socket skCliente = skServidor.accept();
            
            //flujo entrada
            InputStream aux = skCliente.getInputStream();
            DataInputStream flujo_entrada = new DataInputStream(aux);
            
            //flujo salida
            OutputStream aux2 = skCliente.getOutputStream();
            DataOutputStream flujo_salida = new DataOutputStream(aux2);
            
            flujo_salida.close();
            flujo_entrada.close();
            skCliente.close();
            
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        
        
        //Client Socket
        
        try {
            Socket skCliente = new Socket("127.0.0.1",5000);
            
            //flujo entrada
            InputStream aux = skCliente.getInputStream();
            DataInputStream flujo_entrada = new DataInputStream(aux);
            
            //flujo salida
            OutputStream aux2 = skCliente.getOutputStream();
            DataOutputStream flujo_salida = new DataOutputStream(aux2);
            
            flujo_salida.close();
            flujo_entrada.close();
            skCliente.close();

        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
    
}
