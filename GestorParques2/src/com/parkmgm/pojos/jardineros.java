/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.parkmgm.pojos;

/**
 *
 * @author marco
 */
public class jardineros {
    
    int id_jardinero;
    String nombre_jardinero;
    String DNI;
      
    public int getIdJardinero(){ return this.id_jardinero; }
    public void setIdJardinero(int IdJardinero){ this.id_jardinero = IdJardinero; }
    public String getNombreJardinero(){ return this.nombre_jardinero; }
    public void setNombreJardinero(String nombreJardinero){ this.nombre_jardinero = nombreJardinero; }
    public String getDNI(){ return this.DNI; }
    public void setDNI(String dni){ this.DNI = dni; }
}
