/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.parkmgm.pojos;

/**
 *
 * @author marco
 */
public class parques {
    
    int id_parque;
    String nombre_parque;
    int extension;
    int id_ciudad;
            
    public int getIdParque(){ return this.id_parque; }
    public void setIdParque(int IdParque){ this.id_parque = IdParque; }
    public String getNombreParque(){ return this.nombre_parque; }
    public void setNombreParque(String nombreParque){ this.nombre_parque = nombreParque; }
    public int getExtension(){ return this.extension; }
    public void setExtension(int extn){ this.extension = extn; }
    public int getIdCiudad(){ return this.id_ciudad; }
    public void setIdCiudad(int city){ this.id_ciudad = city; }

    
    
}
