 
package com.parkmgm.daos;

import com.parkmgm.conex.conexion;
import com.parkmgm.pojos.parques;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;

/**
 * Prev: Clase de 
 * @author marco
 * Clase donde se 
 */
public class dao_parques {
    conexion conex = new conexion(); // Objeto de conexion
    Statement stmt = null; // Objeto de sentencia
    PreparedStatement pst = null; // Objeto de sentencia preparada
    ResultSet rset = null; // Objeto contenedor de resultados de sentencia
    
//Obtener id del parque segun nombre del parque    
    public int getIdParque(String nomPrk){
         String SSQL = "SELECT id_parque FROM parque WHERE nombre_parque ='"+nomPrk+"' ";
         int idPrk = 0;
         try {
            
            pst = conex.conexBase().prepareStatement(SSQL);
            rset = pst.executeQuery();
            rset.next();
            idPrk = rset.getInt("id_parque");
            conex.close();
            
        } catch (SQLException ex) {
            Logger.getLogger(dao_parques.class.getName()).log(Level.SEVERE, null, ex);
        }
       return idPrk;
    }
     
//Obtener todos los ids de ciudades segun la extension indicada
    public ResultSet getIdCiudadesParqueExtension(int ext){
        try {
            String SSQL = "SELECT id_ciudad FROM parque WHERE extension > '" + ext + "' GROUP BY id_ciudad ";
            pst = conex.conexBase().prepareStatement(SSQL);
            rset = pst.executeQuery();
            
        } catch (SQLException ex) {
            Logger.getLogger(dao_parques.class.getName()).log(Level.SEVERE, null, ex);
        }
        return rset;
    }
    
//Obtener los parques segun la ciudad y extension    
    public void getParquesCiudad_ext(JList jlistParques, JLabel lb, int id_ciudad, int ext){
    
    String SSQL = "SELECT nombre_parque FROM parque WHERE id_ciudad ='"+id_ciudad+"' AND extension > '" + ext + "'  "; 
                                                                      
    DefaultListModel modelo = new DefaultListModel();                 
    jlistParques.setModel(modelo);                                    
  
    try {
       
       pst = conex.conexBase().prepareStatement(SSQL);
       rset = pst.executeQuery();
       conex.close();
       int i = 0;
       
       while(rset.next())
            {
                modelo.addElement(rset.getString("nombre_parque"));
                i++;
            }
       lb.setText(String.valueOf(i));
        } catch (SQLException e) {  
        JOptionPane.showMessageDialog(null, e);
            }finally{
                if(conex!=null){
                    try {
                            conex.close();
                            rset.close();
            
                            conex=null;
                            rset=null;
            
                        } catch (SQLException ex) {
            
                          JOptionPane.showMessageDialog(null, ex);
        }  
    }
  }        
}
    
//Obtener todos los datos del parque segun el nombre del parque
    public parques getDatosParque(String nom_parque){
       
        String SSQL = "SELECT * FROM parque WHERE nombre_parque ='"+nom_parque+"' ";
        parques prk = new parques();                                                  
       
    try {
       
       pst = conex.conexBase().prepareStatement(SSQL);
       rset = pst.executeQuery();
       conex.close();
        
           
       while(rset.next())
            {
            prk.setNombreParque(rset.getString("nombre_parque"));
            prk.setIdCiudad(rset.getInt("id_ciudad"));
            prk.setExtension(rset.getInt("extension"));
            prk.setIdParque(rset.getInt("id_parque"));
            
           }
        
        } catch (SQLException e) {
        JOptionPane.showMessageDialog(null, e);
            }finally{
                if(conex!=null){
                    try {
                            conex.close();
                            conex=null;
                        } catch (SQLException ex) {
            
                          JOptionPane.showMessageDialog(null, ex);
        }  
    }
  }
        return prk;
    }
    
//Añadir un parque    
    public void añadirParque(String nom_prk,int ext,int id_ciu){
       try {
            String SSQL = "INSERT INTO parque (nombre_parque, extension, id_ciudad) values('"+nom_prk+"','"+ext+"','"+id_ciu+"')";
            pst = conex.conexBase().prepareStatement(SSQL);
            pst.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(dao_parques.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
//Actualizar datos parque
    public void updateDatosParque(parques prk){
       String SSQL = "UPDATE parque SET nombre_parque = '"+prk.getNombreParque()+"', id_ciudad = '"+prk.getIdCiudad()+"', extension = "+prk.getExtension()+" "
                + " WHERE id_parque = "+prk.getIdParque()+" ";
        try {
            pst = conex.conexBase().prepareStatement(SSQL);
            pst.executeUpdate();
            JOptionPane.showMessageDialog(null, "Operación (actualización) realizada correctamente");
            conex.close();
        } catch (SQLException ex) {
            Logger.getLogger(dao_parques.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
//Borrar datos parque    
    public void borrarParquesCiudad(int id_ciudad){
       try {
            stmt =  conex.conexBase().createStatement();
            stmt.executeQuery("{call parques.del_prk('"+id_ciudad+"')}");
            JOptionPane.showMessageDialog(null, "Operación (eliminar) realizada correctamente");
            conex.close();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e);
        }finally{
            if(conex!=null){
                try {
                    conex.close();
                    conex=null;
                     
                } catch (SQLException ex) {
                    
                    JOptionPane.showMessageDialog(null, ex);
                }  
            }
        }
    } 
}   