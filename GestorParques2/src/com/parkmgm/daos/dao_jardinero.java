/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.parkmgm.daos;

import com.parkmgm.conex.conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTextField;

/**
 *
 * @author marco
 */
public class dao_jardinero {
    
     conexion conex = new conexion(); //Obejto conexion
     ResultSet rset = null; //Objeto contenedor de resultados de sentencia
     PreparedStatement pst = null; //Objeto de sentencia preparada

//Obtener id jardinero segun su nombre     
    public int getIdJardinero(String nomJardi){
        
        int idJardi = 0;
        
        String SSQL = "SELECT id_jardinero FROM jardinero WHERE nombre_jardinero = '"+nomJardi+"' ";
         try {
              pst = conex.conexBase().prepareStatement(SSQL);
              rset = pst.executeQuery();
              rset.next();
              idJardi = rset.getInt("id_jardinero");
         } catch (SQLException ex) {
             Logger.getLogger(dao_jardinero.class.getName()).log(Level.SEVERE, null, ex);
         }
            
        return idJardi;
    }

//Obtener nombre jardinero segun su id    
    public ResultSet getNomJardinero(int idJardi){
        String SSQL = "SELECT nombre_jardinero FROM jardinero WHERE id_jardinero = '"+idJardi+"' ";
         try {
             pst = conex.conexBase().prepareStatement(SSQL);
             rset = pst.executeQuery();
         } catch (SQLException ex) {
             Logger.getLogger(dao_jardinero.class.getName()).log(Level.SEVERE, null, ex);
         }
         return rset;
    }
    
//Obtener  DNI jardinero segun el nombre del jardinero
    public void getDniJardi(JTextField jtfDniJardi, String nomJardi){
    String SSQL = "SELECT dni FROM jardinero WHERE nombre_jardinero = '"+nomJardi+"'";
     
         try {
             pst = conex.conexBase().prepareStatement(SSQL);
             rset = pst.executeQuery();
             rset.next();
             String dni = rset.getString("dni");
             jtfDniJardi.setText(dni);
         } catch (SQLException ex) {
             Logger.getLogger(dao_jardinero.class.getName()).log(Level.SEVERE, null, ex);
         }
}
}
