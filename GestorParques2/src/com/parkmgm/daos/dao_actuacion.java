/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.parkmgm.daos;

import com.parkmgm.conex.conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author marco
 */
public class dao_actuacion {
    
     conexion conex = new conexion(); //Obejto conexion
     ResultSet rset = null; // Objeto contenedor de resultados de sentencia
     PreparedStatement pst = null; //Objeto de sentencia preparada
         
    //Obtener todos los nombres de actuaciones tipo 
    public ResultSet getNomActuaciones(){
        try {
            String SSQL = "SELECT nombre_actuacion FROM actuaciones";
            pst = conex.conexBase().prepareStatement(SSQL);
            rset = pst.executeQuery();
           
           
        } catch (SQLException ex) {
            Logger.getLogger(dao_actuacion.class.getName()).log(Level.SEVERE, null, ex);
        }
        return rset;
}
    
    //Obtener el numero de la ultima actuacion incluida (no es la clave primaria)
    public int getUltimaActuacion(){
         int ultimaActu = 0;
         String SSQL = "SELECT MAX(id_actu) as id_actu FROM rel_paj";
         try {
             pst = conex.conexBase().prepareStatement(SSQL);
             rset = pst.executeQuery();
             rset.next();
             ultimaActu = rset.getInt("id_actu");
         } catch (SQLException ex) {
             Logger.getLogger(dao_actuacion.class.getName()).log(Level.SEVERE, null, ex);
         }
         return ultimaActu;
    }
    
    //Obtener el id de una actuacion tipo segun su nombre 
    public int getIdActuTy(String nomActuTy){
        int idActu = 0;
        String SSQL = "SELECT id_actuacion FROM actuaciones WHERE nombre_actuacion = '"+nomActuTy+"' ";
        try {
           
            pst = conex.conexBase().prepareStatement(SSQL);
            rset = pst.executeQuery();
            rset.next();
            idActu = rset.getInt("id_actuacion");
                  
        } catch (SQLException ex) {
            Logger.getLogger(dao_actuacion.class.getName()).log(Level.SEVERE, null, ex);
        }
        return idActu;
    }
    
    //Obtener la duracion de una actuacion tipo segun la id de la actuacion tipo
    public int getDuracionActuTy(int idActuTy){
        int duracion = 0;
        String SSQL = "SELECT duracion FROM actuaciones WHERE id_actuacion = '"+idActuTy+"' ";
        try {
           
            pst = conex.conexBase().prepareStatement(SSQL);
            rset = pst.executeQuery();
            rset.next();
            duracion = rset.getInt("duracion");
                  
        } catch (SQLException ex) {
            Logger.getLogger(dao_actuacion.class.getName()).log(Level.SEVERE, null, ex);
        }
        return duracion;
    }
    
    //Obtener el nombre para mostrar de las actuaciones que tiene un parque, segun el id del parque.
    public ResultSet getNomActusParques(int idPrk){
     
        String SSQL = "SELECT display_nom FROM rel_paj WHERE id_prk = '"+idPrk+"' GROUP BY display_nom ";
        
         try {
            pst = conex.conexBase().prepareStatement(SSQL);
            rset = pst.executeQuery();  
        } catch (SQLException ex) {
            Logger.getLogger(dao_actuacion.class.getName()).log(Level.SEVERE, null, ex);
        }
         return rset;
        
    }
       
    //Obtener datos de actuacion    
    public ResultSet getDatosActuacion(String dspNom){
        
         String SSQL = "SELECT id_actu_ty,fecha_i,fecha_f FROM rel_paj WHERE display_nom = '"+dspNom+"' ";
         try {
             pst = conex.conexBase().prepareStatement(SSQL);
             rset = pst.executeQuery();
         } catch (SQLException ex) {
             Logger.getLogger(dao_actuacion.class.getName()).log(Level.SEVERE, null, ex);
         }            
         return rset;      
    }
    
    //Obtener ids de jardineros de una actuacion    
    public ResultSet getIdJardiActu(String dspNom){
        
        String SSQL = "SELECT id_jardi FROM rel_paj WHERE display_nom ='"+dspNom+"' ";
         try {
             pst = conex.conexBase().prepareStatement(SSQL);
             rset = pst.executeQuery();
         } catch (SQLException ex) {
             Logger.getLogger(dao_jardinero.class.getName()).log(Level.SEVERE, null, ex);
         }
         return rset; 
    }
    
    //Añadir una actuacion
    public void añadirActu(int idPrk, int IdActu, int IdJardi,String fechaI, int idActuT ){
    String SSQL = "INSERT INTO rel_paj(id_prk,id_actu,id_jardi,fecha_i,id_actu_ty) VALUES( "+idPrk+","+IdActu+","+IdJardi+",'"+fechaI+"',"+idActuT+" )"; 
     try {
      pst = conex.conexBase().prepareStatement(SSQL);
            pst.executeUpdate();
            JOptionPane.showMessageDialog(null, "Operación (agregar:[rl_paj-2045]) realizada correctamente");
             } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e);
        }
}
}