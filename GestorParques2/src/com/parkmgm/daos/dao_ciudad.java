/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.parkmgm.daos;

import com.parkmgm.conex.conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author marco
 */
public class dao_ciudad {
    
     conexion conex = new conexion(); //Obejto conexion
     PreparedStatement pst = null; // Objeto de sentencia preparada
     ResultSet rset = null; // Objeto contenedor de resultados

    //Obtener nombre de ciudad segun el id de la ciudad
    public String getNombreCiudad(int id_ciudad){
        String nom_ciud = "";
         try {
                        
             String SSQL = "SELECT nombre_ciudad FROM ciudades WHERE id_ciudad='"+id_ciudad+"' ";
             
             pst = conex.conexBase().prepareStatement(SSQL);
             rset = pst.executeQuery();
             while(rset.next())
            {
            nom_ciud = rset.getString("nombre_ciudad");
            }   
             
         } catch (SQLException ex) {
             Logger.getLogger(dao_ciudad.class.getName()).log(Level.SEVERE, null, ex);
         }
         return nom_ciud;
    }
    
    //Obtener el Id de ciudad segun el nombre de la ciudad
    public int getIdCiudad(String nom_ciu){
       int id_ciu = 0;
        try {
             
            String SSQL = "SELECT id_ciudad FROM ciudades WHERE nombre_ciudad = '"+nom_ciu+"'";
            pst = conex.conexBase().prepareStatement(SSQL);
            rset = pst.executeQuery();
            rset.next();
            id_ciu = rset.getInt("id_ciudad");
        } catch (SQLException ex) {
            Logger.getLogger(dao_ciudad.class.getName()).log(Level.SEVERE, null, ex);
        } 
        return id_ciu;
    }
    
    //Obtener los nombres de todas las ciudades
    public ResultSet getNombreCiudades(){
        try {
                        
             String SSQL = "SELECT nombre_ciudad FROM ciudades";
             pst = conex.conexBase().prepareStatement(SSQL);
             rset = pst.executeQuery();
             
             
         } catch (SQLException ex) {
             Logger.getLogger(dao_ciudad.class.getName()).log(Level.SEVERE, null, ex);
         }
         return rset;
    }
}
