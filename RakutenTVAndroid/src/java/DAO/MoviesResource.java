
package DAO;

import java.util.ArrayList;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.core.MediaType;
import model.Pelicula;

@Path("movies")
public class MoviesResource {

    @Context
    private UriInfo context;

   
    public MoviesResource() {
    }

  
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getJson() {
         PeliculaDAO peliculaDao = new PeliculaDAO();
        Pelicula pelicula = new Pelicula();
        ArrayList<Pelicula> listPeliculas = peliculaDao.findAll(pelicula);
          return Pelicula.toArrayJSon(listPeliculas);
    }

  
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public void putJson(String content) {
    }
}
