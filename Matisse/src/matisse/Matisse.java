/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package matisse;

import com.matisse.MtDatabase;
import com.matisse.MtException;
import com.matisse.MtPackageObjectFactory;
import test.Autor;
import test.Libro;
import test.Obra;

/**
 *
 * @author marco
 */
public class Matisse {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        crearObjetos();
    }

    public static void crearObjetos() {
        try {
            MtDatabase db;
            db = new MtDatabase("localhost", "test", new MtPackageObjectFactory("", "test"));
            
            db.open();
            db.startTransaction();

            Autor autor = new Autor(db);
            autor.setNombre("Alicia");
            autor.setApellido("Martinez");
            autor.setEdad("36");

            System.out.println("Vamos a crear dos objetos Libro");

            Libro libro1 = new Libro(db);
            libro1.setTitulo("Recetas de cocina");
            libro1.setEditorial("Editorial Cocineros");
            libro1.setPaginas(365);

            Libro libro2 = new Libro(db);
            libro2.setTitulo("Inciacion a Java");
            libro2.setEditorial("Editorial Desarrolladores");
            libro2.setPaginas(1025);
            
            System.out.println("Creamos un array de obras para guardar los libros");

            Obra o1[] = new Obra[2];
            o1[0] = libro1;
            o1[1] = libro2;
            
            autor.setEscribe(o1);
            db.commit();
            db.close();

        } catch (MtException mte) {
            
            System.out.println("MtException: " + mte.getMessage());
        }
    }

}
