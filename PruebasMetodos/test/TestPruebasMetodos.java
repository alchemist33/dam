import static org.junit.Assert.*;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import pruebasmetodos.PruebasMetodos;


public class TestPruebasMetodos {

	private static PruebasMetodos est;
	private int[] sexo = {1, 1, 1, 0} ;
	double[] edad = {18, 52, 48, 32} ;
	
	@BeforeClass
	public static void Inicializa(){
		est = new PruebasMetodos();
	}
	
	@AfterClass
	public static void Vacia(){
		est = null;
	}
	
	@Test
	public void testPUserMayor50() {
		double esperado = est.pUserMayor50(edad);
		double actual = 25;
		Assert.assertEquals(esperado, actual, 0.1);
	}

	@Test
	public void testPUserMenor50() {
		double[] edad = {18, 52, 48, 32} ;
		double esperado = est.pUserMenor50(edad);
		double actual = 75;
		Assert.assertEquals(esperado, actual, 0.1);
	}

	@Test
	public void testPUserFemenino() {
		double esperado = est.pUserFemenino(sexo);
		double actual = 25;
		Assert.assertEquals(esperado, actual, 0.1);
	}

	@Test
	public void testPUserMasculino() {
		double esperado = est.pUserMasculino(sexo);
		double actual = 75;
		Assert.assertEquals(esperado, actual, 0.1);
	}

	@Test
	public void testPUserConhijos() {
		double[] hijos = {1, 3, 4, 2} ;
		double esperado = est.pUserConhijos(hijos);
		double actual = 50;
		Assert.assertEquals(esperado, actual, 0.1);
	}

}
