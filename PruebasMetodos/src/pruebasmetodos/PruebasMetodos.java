/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pruebasmetodos;

/**
 *
 * @author marco
 */
public class PruebasMetodos {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
    }
    
    
    public double pUserMayor50(double[] edad){
		int i;
		double suma = 0; 
		double porcentaje = 0;
		
        for (i = 0; i < edad.length; i++) {
        	if (edad[i] > 50){
        		suma = suma + 1;
        	}
        }
        porcentaje = suma/edad.length * 100;		
        
        System.out.printf("Porcentaje usuarios > 50 años: %.2f %n", porcentaje);
        return porcentaje;
	}
	
	public double pUserMenor50(double[] edad){
		int i;
		double suma = 0; 
		double porcentaje = 0;
		
        for (i = 0; i < edad.length; i++) {
        	if (edad[i] < 50){
        		suma = suma + 1;
        	}
        }
        porcentaje = suma/edad.length * 100;		
        
        System.out.printf("Porcentaje usuarios < 50 años: %.2f %n", porcentaje);
        return porcentaje;
	}
	
	public double pUserFemenino(int[] sexo){
		int i;
		double suma = 0;
		double porcentaje = 0;
		for (i = 0; i < sexo.length; i++) {
            if (sexo[i] == 0){
            	suma = suma + 1;
            }
        }
		porcentaje = suma/sexo.length * 100;
        System.out.printf("Porcentaje usuarios femenino: %.2f %n", porcentaje);
       
		return porcentaje;
	
	}
	
	public double pUserMasculino(int[] sexo){
		int i;
		double suma = 0;
		double porcentaje = 0;
		for (i = 0; i < sexo.length; i++) {
            if (sexo[i] == 1){
            	suma = suma + 1;
            }
        }
		porcentaje = suma/sexo.length * 100;
        System.out.printf("Porcentaje usuarios masculino: %.2f %n", porcentaje);
       
		return porcentaje;
	
	}
	
	
	public double pUserConhijos(double[] hijos){
		double suma = 0;
		double porcentaje = 0;
		
		int i;
		for (i = 0; i < hijos.length; i++) {
            if (hijos[i] >= 3){
            	suma = suma + 1;
            }
        }
		porcentaje = suma/hijos.length * 100;
        System.out.printf("Porcentaje usuarios > 3 hijos: %.2f %n", porcentaje);
       
		return porcentaje;
	
	}
    
}
