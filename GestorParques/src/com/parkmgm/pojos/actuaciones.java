/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.parkmgm.pojos;

/**
 *
 * @author marco
 */
public class actuaciones {
    
    int id_actuacion;
    public String nombre_actuacion;
    int duracion;
    
    public actuaciones(){  }
    
    public int getIdActuacion(){ return this.id_actuacion; }
    public void setIdActuacion(int IdActuacion){ this.id_actuacion = IdActuacion; }
    public String getNombreActuacion(){ return this.nombre_actuacion; }
    public void setNombreActuacion(String nombreActuacion){ this.nombre_actuacion = nombreActuacion; }
    public void setDuracion(int Dura){ this.duracion = Dura; }
    public int getDuracion(){ return this.duracion; }
    
    
}
