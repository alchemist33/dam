/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.parkmgm.pojos;

/**
 *
 * @author marco
 */
public class ciudades {
    int id_ciudad;
    String nombre_ciudad;
    int cod_ciudad;
    
    public ciudades(){  }
    
    public int getIdCiudad(){ return this.id_ciudad; }
    public String getNombreCiudad(){ return this.nombre_ciudad; }
    public void setNombreCiudad(String nombreCiudad){ this.nombre_ciudad = nombreCiudad; }
    public int getCodCiudad(){ return this.cod_ciudad; }
    public void setCodCiudad(int codCiudad){ this.cod_ciudad = codCiudad; }
}
