
package com.parkmgm.gui;
import com.parkmgm.pojos.parques;
import com.parkmgm.pojos.actuaciones;
import com.parkmgm.pojos.jardineros;
import com.parkmgm.dataDB.sqlStmts;


import javax.swing.JOptionPane;
/**
 *
 * @author marco
 * Ventana para la agregacion de actuaciones a un parque
 */
public class addActu extends javax.swing.JFrame {

    addActu() {
         initComponents();
        
    }

    /**
     *
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButton1 = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jTextField2 = new javax.swing.JTextField();
        jTextField3 = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jComboBox1 = new javax.swing.JComboBox<>();
        jLabel6 = new javax.swing.JLabel();
        jTextField4 = new javax.swing.JTextField();
        jPanel2 = new javax.swing.JPanel();
        jCheckBox1 = new javax.swing.JCheckBox();
        jCheckBox2 = new javax.swing.JCheckBox();
        jCheckBox3 = new javax.swing.JCheckBox();
        jCheckBox4 = new javax.swing.JCheckBox();
        jCheckBox5 = new javax.swing.JCheckBox();
        jCheckBox6 = new javax.swing.JCheckBox();
        jCheckBox7 = new javax.swing.JCheckBox();
        jCheckBox8 = new javax.swing.JCheckBox();
        jCheckBox9 = new javax.swing.JCheckBox();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Agregar una nueva actuacion");

        jButton1.setText("Agregar actuacion");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jPanel1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        jLabel1.setText("Nombre del parque");
        jLabel1.setToolTipText("");

        jTextField1.setDisabledTextColor(new java.awt.Color(51, 51, 51));
        jTextField1.setEnabled(false);

        jLabel2.setText("Ciudad en la que se encuentra el parque");
        jLabel2.setToolTipText("Ciudades disponibles:\n-Madrid\n-Barcelona\n-Zaragoza\n-Sevilla\n-Vizcaya\n-Cuenca\n-La Coruña\n-Valencia");

        jTextField2.setDisabledTextColor(new java.awt.Color(51, 51, 51));
        jTextField2.setEnabled(false);

        jTextField3.setDisabledTextColor(new java.awt.Color(51, 51, 51));
        jTextField3.setEnabled(false);

        jLabel3.setText("Extensión del parque");

        jLabel4.setText("m2");

        jLabel5.setText("Selecciona actuacion");

        jLabel6.setText("Id parque");

        jTextField4.setDisabledTextColor(new java.awt.Color(51, 51, 51));
        jTextField4.setEnabled(false);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(jComboBox1, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel1)
                        .addComponent(jTextField1)
                        .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jTextField2)
                        .addComponent(jLabel5))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jTextField3, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel4))
                            .addComponent(jLabel3))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel6)
                            .addComponent(jTextField4, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addGap(4, 4, 4)
                .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(jLabel6))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextField3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4)
                    .addComponent(jTextField4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 26, Short.MAX_VALUE)
                .addComponent(jLabel5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jPanel2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        jCheckBox1.setText("Maria-Jose Garcia");

        jCheckBox2.setText("Santigago Martinez");

        jCheckBox3.setText("Carlos Suarez");

        jCheckBox4.setText("Elisa Olmedo");

        jCheckBox5.setText("Cristian Gomez");

        jCheckBox6.setText("Lucia Campillo");

        jCheckBox7.setText("Alvaro Jimenez");

        jCheckBox8.setText("Victor Fernandez");

        jCheckBox9.setText("Javier Ortgea");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jCheckBox2)
                    .addComponent(jCheckBox1)
                    .addComponent(jCheckBox3)
                    .addComponent(jCheckBox4)
                    .addComponent(jCheckBox5)
                    .addComponent(jCheckBox6)
                    .addComponent(jCheckBox7)
                    .addComponent(jCheckBox8)
                    .addComponent(jCheckBox9))
                .addContainerGap(76, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jCheckBox2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jCheckBox1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jCheckBox3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jCheckBox4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jCheckBox5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jCheckBox6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jCheckBox7)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jCheckBox8)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jCheckBox9)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jButton1)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
       jardineros jardi = new jardineros();
       try {
            // TODO add your handling code here:
            parques prk = new parques();
            actuaciones act = new actuaciones();
            
            jardineros jardi1 = new jardineros();
            jardineros jardi2 = new jardineros();
            jardineros jardi3 = new jardineros();
            jardineros jardi4 = new jardineros();
            jardineros jardi5 = new jardineros();
            jardineros jardi6 = new jardineros();
            jardineros jardi7 = new jardineros();
            jardineros jardi8 = new jardineros();
            jardineros jardi9 = new jardineros();
            sqlStmts sqls = new  sqlStmts();
            int idRegActu;
            
            prk.setNombreParque(jTextField1.getText());
            prk.setCiudad(jTextField2.getText());
            prk.setExtension(Integer.parseInt(jTextField3.getText()));
            prk.setIdParque(Integer.parseInt(jTextField4.getText()));
            
            String nom_actu = jComboBox1.getItemAt(jComboBox1.getSelectedIndex());
            act.setIdActuacion(sqls.consultar_idActuaciones_nombreActu(nom_actu));
            act.setDuracion(sqls.consultar_duracion_nombreActu(nom_actu));
                   
            sqls.añadir_reg_actuacion_parque(prk, act);
            idRegActu = sqls.consultar_ultimo_id_reg_actuaciones();
            
            sqls.consultar_duracion_actu(idRegActu);
            sqls.actualizar_duracion_actu(act,idRegActu);
            sqls.actualizar_campo_reg_actus(idRegActu, nom_actu);
               if (jCheckBox1.isSelected() == true ){
                   jardi1.setNombreJardinero(jCheckBox1.getText());
                   jardi1.setIdJardinero(sqls.consultar_id_jardi_nom_jardi(jardi1.getNombreJardinero()));
                   sqls.añadir_rel_actu_jar(idRegActu, jardi1);
               }
               if (jCheckBox2.isSelected() == true ){
                   jardi2.setNombreJardinero(jCheckBox2.getText());
                   jardi2.setIdJardinero(sqls.consultar_id_jardi_nom_jardi(jardi2.getNombreJardinero()));
                   sqls.añadir_rel_actu_jar(idRegActu, jardi2);
               }
               if (jCheckBox3.isSelected() == true ){
                   jardi3.setNombreJardinero(jCheckBox3.getText());
                   jardi3.setIdJardinero(sqls.consultar_id_jardi_nom_jardi(jardi3.getNombreJardinero()));
                   sqls.añadir_rel_actu_jar(idRegActu, jardi3);
               }
                if (jCheckBox4.isSelected() == true ){
                   jardi4.setNombreJardinero(jCheckBox4.getText());
                   jardi4.setIdJardinero(sqls.consultar_id_jardi_nom_jardi(jardi4.getNombreJardinero()));
                   sqls.añadir_rel_actu_jar(idRegActu, jardi4);
               }
               if (jCheckBox5.isSelected() == true ){
                   jardi5.setNombreJardinero(jCheckBox5.getText());
                   jardi5.setIdJardinero(sqls.consultar_id_jardi_nom_jardi(jardi5.getNombreJardinero()));
                   sqls.añadir_rel_actu_jar(idRegActu, jardi5);
               }
               if (jCheckBox6.isSelected() == true ){
                   jardi6.setNombreJardinero(jCheckBox6.getText());
                   jardi6.setIdJardinero(sqls.consultar_id_jardi_nom_jardi(jardi6.getNombreJardinero()));
                   sqls.añadir_rel_actu_jar(idRegActu, jardi6);
               }
                if (jCheckBox7.isSelected() == true ){
                   jardi7.setNombreJardinero(jCheckBox7.getText());
                   jardi7.setIdJardinero(sqls.consultar_id_jardi_nom_jardi(jardi7.getNombreJardinero()));
                   sqls.añadir_rel_actu_jar(idRegActu, jardi7);
               }
               if (jCheckBox8.isSelected() == true ){
                   jardi8.setNombreJardinero(jCheckBox8.getText());
                   jardi8.setIdJardinero(sqls.consultar_id_jardi_nom_jardi(jardi8.getNombreJardinero()));
                   sqls.añadir_rel_actu_jar(idRegActu, jardi8);
               }
               if (jCheckBox9.isSelected() == true ){
                   jardi9.setNombreJardinero(jCheckBox9.getText());
                   jardi9.setIdJardinero(sqls.consultar_id_jardi_nom_jardi(jardi9.getNombreJardinero()));
                   sqls.añadir_rel_actu_jar(idRegActu, jardi9);
               }
           
            
           
            
            
            this.setVisible(false);
            
       }catch(NumberFormatException e){
           JOptionPane.showMessageDialog(null, "Error!"+e);
       }
       
       
           
        
        
    }//GEN-LAST:event_jButton1ActionPerformed
 public void autoFill(parques park){
        
        jTextField1.setText(park.getNombreParque());
        jTextField2.setText(park.getCiudad());
        jTextField3.setText(String.valueOf(park.getExtension()));
        jTextField4.setText(String.valueOf(park.getIdParque()));
        sqlStmts sqls = new  sqlStmts();
        sqls.consultar_nomActuaciones(jComboBox1);
        
 }
    /**
     * @param args the command line arguments
     */
//    public static void main(String args[]) {
//        /* Set the Nimbus look and feel */
//        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
//        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
//         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
//         */
//        try {
//            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
//                if ("Nimbus".equals(info.getName())) {
//                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
//                    break;
//                }
//            }
//        } catch (ClassNotFoundException ex) {
//            java.util.logging.Logger.getLogger(addPark.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (InstantiationException ex) {
//            java.util.logging.Logger.getLogger(addPark.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (IllegalAccessException ex) {
//            java.util.logging.Logger.getLogger(addPark.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
//            java.util.logging.Logger.getLogger(addPark.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        }
//        //</editor-fold>
//
//        /* Create and display the dialog */
//        java.awt.EventQueue.invokeLater(new Runnable() {
//            public void run() {
//                addPark dialog = new addPark(new javax.swing.JFrame(), true);
//                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
//                    @Override
//                    public void windowClosing(java.awt.event.WindowEvent e) {
//                        System.exit(0);
//                    }
//                });
//                dialog.setVisible(true);
//            }
//        });
//    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JCheckBox jCheckBox1;
    private javax.swing.JCheckBox jCheckBox2;
    private javax.swing.JCheckBox jCheckBox3;
    private javax.swing.JCheckBox jCheckBox4;
    private javax.swing.JCheckBox jCheckBox5;
    private javax.swing.JCheckBox jCheckBox6;
    private javax.swing.JCheckBox jCheckBox7;
    private javax.swing.JCheckBox jCheckBox8;
    private javax.swing.JCheckBox jCheckBox9;
    private javax.swing.JComboBox<String> jComboBox1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JTextField jTextField2;
    private javax.swing.JTextField jTextField3;
    private javax.swing.JTextField jTextField4;
    // End of variables declaration//GEN-END:variables
}
