/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.

Se importan todas los paquetes de conexion, y manejo de datos
 */
package com.parkmgm.dataDB;
import com.parkmgm.pojos.parques;
import com.parkmgm.pojos.jardineros;
import com.parkmgm.pojos.actuaciones;
import com.parkmgm.conex.conexion;
import com.parkmgm.daos.dao_parques;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import javax.swing.JComboBox;
import javax.swing.JLabel;

import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 * Se crean todas los obejtos necesarios para las obtencion de toda la informacion de la que precisa el programa. 
 * Posteriormente se migraran al paquete buss a medida que se establezcan los procedimientos almacenados correspondientes
 * @author marco
 */
public class sqlStmts {
    //Objetos comunes
    conexion conex = new conexion(); //Obejto conexion
    conexion conex2 = new conexion(); //Obejto conexion
    conexion conex3 = new conexion(); //Obejto conexion
    conexion conex4 = new conexion(); //Obejto conexion
    conexion conex5 = new conexion(); //Obejto conexion
    conexion conex6 = new conexion(); //Obejto conexion
    conexion conex7 = new conexion(); //Obejto conexion
    conexion conex8 = new conexion(); //Obejto conexion
    conexion conex9 = new conexion(); //Obejto conexion
    conexion conex10 = new conexion(); //Obejto conexion
    ResultSet rset = null; // Objeto contenedor de resultados
    ResultSet rset2 = null; // Objeto contenedor de resultados
    ResultSet rset3 = null; // Objeto contenedor de resultados
    ResultSet rset4 = null; // Objeto contenedor de resultados
    Statement stmt = null; // Onjeto de sentencia
    PreparedStatement pst = null; // Objeto de sentencia
    PreparedStatement pst2 = null; // Objeto de sentencia
 
    /**
     *
     * @param cboxCiudades
     * @param ext
     */
    
public void consultar_ciudades_extension(JComboBox cboxCiudades,Integer ext){
    //Creamos la Consulta SQL
    String SSQL = "SELECT ciudad FROM parque WHERE extension > '" + ext + "' GROUP BY ciudad ";

    //Establecemos bloque try-catch-finally
    try {
 
       //Preparamos la consulta SQL y la ejecutamos
       
       pst = conex.conexBase().prepareStatement(SSQL);
       rset = pst.executeQuery();
       conex.close();
       //LLenamos nuestro ComboBox
       cboxCiudades.addItem("Seleccione una opción");
        while(rset.next())
            {
            cboxCiudades.addItem(rset.getString("ciudad"));
            }   
        } catch (SQLException e) {
        JOptionPane.showMessageDialog(null, e);
            }finally{
                if(conex!=null){
                    try {
                            conex.close();
                            rset.close();
            
                            conex=null;
                            rset=null;
            
                        } catch (SQLException ex) {
            
                          JOptionPane.showMessageDialog(null, ex);
        }   
    }
}  
}
public void consultar_parques_ciudad(JList jlistParques, JLabel lb, String ciudad, Integer ext){
    
    String SSQL = "SELECT * FROM parque WHERE ciudad ='"+ciudad+"' AND extension > '" + ext + "'  "; //Creamos la Consulta SQL   
    parques prk = new parques();                                      //Creamos el objeto parque para rellenarlo con los datos de la SQL
    DefaultListModel modelo = new DefaultListModel();                 //Establecemos un modelo de items para un objeto lista
    jlistParques.setModel(modelo);                                    //Asignamos el modelo al objeto lista que nos trae el método
  
    //Establecemos bloque try-catch-finally
    try {
       //Preparamos la consulta SQL y la ejecutamos
       pst = conex.conexBase().prepareStatement(SSQL);
       rset = pst.executeQuery();
       conex.close();
        int i = 0;
       //Rellenamos el objeto parque
       while(rset.next())
            {
            prk.setNombreParque(rset.getString("nombre_parque"));
            prk.setCiudad(rset.getString("ciudad"));
            prk.setExtension(rset.getInt("extension"));
            
            //LLenamos nuestro JList
            modelo.addElement(prk.getNombreParque());
            
            i++;
            }
 lb.setText(String.valueOf(i));
        } catch (SQLException e) {  
        JOptionPane.showMessageDialog(null, e);
            }finally{
                if(conex!=null){
                    try {
                            conex.close();
                            rset.close();
            
                            conex=null;
                            rset=null;
            
                        } catch (SQLException ex) {
            
                          JOptionPane.showMessageDialog(null, ex);
        }  
    }
  }        
} 
public void añadir_parque_ciudad(parques prk){
    if( !"Madrid".equals(prk.getCiudad()) && !"Sevilla".equals(prk.getCiudad()) &&
            !"La Coruña".equals(prk.getCiudad()) && !"Cuenca".equals(prk.getCiudad()) && !"Barcelona".equals(prk.getCiudad()) &&
            !"Zaragoza".equals(prk.getCiudad()) && !"Vizcaya".equals(prk.getCiudad()) && !"Valencia".equals(prk.getCiudad()) &&
            !"Salamanca".equals(prk.getCiudad()) ){
        JOptionPane.showMessageDialog(null, "Error: agrega una ciudad disponible");
    }else{
        
        
        String SSQL = "INSERT INTO parque(nombre_parque,ciudad,extension) VALUES( '"+prk.getNombreParque()+"','"+prk.getCiudad()+"','"+prk.getExtension()+"' )"; //Creamos la Consulta SQL
        
        //Establecemos bloque try-catch-finally
        try {
            //Preparamos la consulta SQL y la ejecutamos
            pst = conex.conexBase().prepareStatement(SSQL);
            pst.executeUpdate();
            JOptionPane.showMessageDialog(null, "Operación (agregar:[prk-3]) realizada correctamente");
            conex.close();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e);
        }finally{
            if(conex!=null){
                try {
                    conex.close();
                    conex=null;
                    
                    
                } catch (SQLException ex) {
                    
                    JOptionPane.showMessageDialog(null, ex);
                }  
            }
        }
    }
}
public parques consultar_datos_parque(String nombre_parque){
  
    String SSQL = "SELECT * FROM parque WHERE nombre_parque ='"+nombre_parque+"' "; //Creamos la Consulta SQL   
    parques park = new parques();                                                  //Creamos el objeto parque para rellenarlo con los datos de la SQL
   
    //Establecemos bloque try-catch-finally
    try {
       //Preparamos la consulta SQL y la ejecutamos
       pst = conex.conexBase().prepareStatement(SSQL);
       rset = pst.executeQuery();
       conex.close();
       //Rellenamos el objeto parque
      
           
       while(rset.next())
            {
            park.setNombreParque(rset.getString("nombre_parque"));
            park.setCiudad(rset.getString("ciudad"));
            park.setExtension(rset.getInt("extension"));
            park.setIdParque(rset.getInt("id_parque"));
            
           }
        
        } catch (SQLException e) {
        JOptionPane.showMessageDialog(null, e);
            }finally{
                if(conex!=null){
                    try {
                            conex.close();
                            conex=null;
                        } catch (SQLException ex) {
            
                          JOptionPane.showMessageDialog(null, ex);
        }  
    }
  }        
        return park;
} 
public void actualizar_parque(parques prk, String nom_prk){
    
    if( !"Madrid".equals(prk.getCiudad()) && !"Sevilla".equals(prk.getCiudad()) && !"Salamanca".equals(prk.getCiudad()) &&
            !"La Coruña".equals(prk.getCiudad()) && !"Cuenca".equals(prk.getCiudad()) && !"Barcelona".equals(prk.getCiudad()) &&
            !"Zaragoza".equals(prk.getCiudad()) && !"Vizcaya".equals(prk.getCiudad()) && !"Valencia".equals(prk.getCiudad()) ){
        JOptionPane.showMessageDialog(null, "Error: agrega una ciudad disponible");
    }else{
        
        String SSQL; //Creamos la Consulta SQL
        SSQL = "UPDATE parque SET nombre_parque = '"+prk.getNombreParque()+"', ciudad = '"+prk.getCiudad()+"', extension = '"+prk.getExtension()+"'"
                + " WHERE nombre_parque = '"+nom_prk+"' ";
     
        //Establecemos bloque try-catch-finally
        try {
            //Preparamos la consulta SQL y la ejecutamos
            pst = conex.conexBase().prepareStatement(SSQL);
            pst.executeUpdate(); 
            
            JOptionPane.showMessageDialog(null, "Operación (actualización) realizada correctamente");
            conex.close();
            
            
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e);
        }finally{
            if(conex!=null){
                try {
                    conex.close();
                    conex=null;
                    
                    
                } catch (SQLException ex) {
                    
                    JOptionPane.showMessageDialog(null, ex);
                }  
            }
        }
    }
}
public void borrar_parques_ciudad(String ciudad){
        String SSQL = " DELETE FROM parque WHERE ciudad = '"+ciudad+"' "; //Creamos la Consulta SQL
        
        //Establecemos bloque try-catch-finally
        try {
            //Preparamos la consulta SQL y la ejecutamos
            pst = conex.conexBase().prepareStatement(SSQL);
            pst.executeUpdate();
            JOptionPane.showMessageDialog(null, "Operación (eliminar) realizada correctamente");
            conex.close();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e);
        }finally{
            if(conex!=null){
                try {
                    conex.close();
                    conex=null;
                    
                    
                } catch (SQLException ex) {
                    
                    JOptionPane.showMessageDialog(null, ex);
                }  
            }
        }
    }
public int consultar_idActuaciones_nombreActu(String nombre_actuacion){
            int idActu = 0;
        
            String SSQL = "SELECT * FROM actuaciones WHERE nombre_actuacion = '"+nombre_actuacion+"'";
             try {
             pst = conex.conexBase().prepareStatement(SSQL);
             rset = pst.executeQuery();
             while(rset.next()){
                 idActu = rset.getInt("id_actuacion");
             }
             
             conex.close();
            
        }catch(SQLException e){
                 JOptionPane.showMessageDialog(null, e);
            }finally{
                if(conex!=null){
                    try {
                            conex.close();
                            conex=null;
                        } catch (SQLException ex) {
            
                          JOptionPane.showMessageDialog(null, ex);
             }
            
                }
             }return idActu;
        }
public int consultar_duracion_nombreActu(String nombre_actuacion){
    int d = 0;
     String SSQL = "SELECT duracion FROM actuaciones WHERE nombre_actuacion = '"+nombre_actuacion+"'";
     try {
             pst = conex10.conexBase().prepareStatement(SSQL);
             rset = pst.executeQuery();
             rset.next();
                 d = rset.getInt("duracion");
            
             
             conex10.close();
            
        }catch(SQLException e){
                 JOptionPane.showMessageDialog(null, e);
            }finally{
                if(conex10!=null){
                    try {
                            conex10.close();
                            conex10=null;
                        } catch (SQLException ex) {
            
                          JOptionPane.showMessageDialog(null, ex);
             }
            
                }
             }
        return d;
}
ResultSet consultar_idActuaciones_parque(Integer id_park){
    String SSQL = "SELECT * FROM reg_actuaciones WHERE id_parque = "+id_park+" ";
    
    //Establecemos bloque try-catch-finally
    try {
       //Preparamos la consulta SQL y la ejecutamos
       pst = conex.conexBase().prepareStatement(SSQL);
       rset = pst.executeQuery();
       conex.close();
              
        } catch (SQLException e) {
        JOptionPane.showMessageDialog(null, e);
            }finally{
                if(conex!=null){
                    try {
                            conex.close();
                            conex=null;
                        } catch (SQLException ex) {
            
                          JOptionPane.showMessageDialog(null, ex);
        }  
    }
  }
        return rset;
}
public void consultar_nomActuaciones(JComboBox cboxActuaciones){
        
       
            
                String SSQL = "SELECT * FROM actuaciones "; //Creamos la Consulta SQL
                                                //Creamos el objeto parque para rellenarlo con los datos de la SQL
                //Asignamos el modelo al objeto lista que nos trae el método
                               //Establecemos un modelo de items para un objeto lista
                cboxActuaciones.addItem("Selecciona una actuación");
                //Establecemos bloque try-catch-finally
                try {
                    //Preparamos la consulta SQL y la ejecutamos
                    pst = conex.conexBase().prepareStatement(SSQL);
                    rset = pst.executeQuery();
                    conex.close();
                    
                    //Rellenamos el objeto parque
                    while(rset.next())
                    {
                        cboxActuaciones.addItem(rset.getString("nombre_actuacion"));
                        
                    }
                    
                } catch (SQLException e) {
                    JOptionPane.showMessageDialog(null, e);
                }finally{
                    if(conex!=null){
                        try {
                            conex.close();
                            rset.close();
            
                            conex=null;
                            rset=null;
            
                        } catch (SQLException ex) {
                            
                            JOptionPane.showMessageDialog(null, ex);
                        }
                    }
                }
                
                
        
       
} 
public void consultar_nomActuaciones_parque(JList jlistActuaciones, Integer id_park){
        try {
            rset2 = consultar_idActuaciones_parque(id_park);
            
            DefaultListModel modelo = new DefaultListModel();                 
            jlistActuaciones.setModel(modelo);
            
            while(rset2.next()){
                   modelo.addElement(rset2.getString("nom_id_reg"));
                 }  
        } catch (SQLException ex) {
            Logger.getLogger(sqlStmts.class.getName()).log(Level.SEVERE, null, ex);
        }
} 
public void añadir_reg_actuacion_parque(parques prk, actuaciones actu){
   
        String SSQL = "INSERT INTO reg_actuaciones(id_actu, id_parque) VALUES("+actu.getIdActuacion()+","+prk.getIdParque()+")"; //Creamos la Consulta SQL
        
        //Establecemos bloque try-catch-finally
        try {
            //Preparamos la consulta SQL y la ejecutamos
            pst = conex2.conexBase().prepareStatement(SSQL);
            pst.executeUpdate();
          
            JOptionPane.showMessageDialog(null, "Operación (agregar:[act958]) realizada correctamente");
            conex2.close();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e);
            }finally{
            if(conex2!=null){
                try {
                    conex2.close();
                    conex2=null;
                    
                    
                } catch (SQLException ex) {
                    
                    JOptionPane.showMessageDialog(null, ex);
                }  
            }
        }

}
public int consultar_ultimo_id_reg_actuaciones(){
    int idReg = 0;
    String SSQL = "SELECT MAX(id_reg_actu) FROM reg_actuaciones";
     try {
            //Preparamos la consulta SQL y la ejecutamos
            pst = conex3.conexBase().prepareStatement(SSQL);
            rset = pst.executeQuery();
            rset.next();
            idReg = rset.getInt("max(id_reg_actu)");
            JOptionPane.showMessageDialog(null, "Operación (consulta:[qry-23]) realizada correctamente");
            conex3.close();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e);
        }finally{
            if(conex3!=null){
                try {
                    conex3.close();
                    conex3=null;
                    
                    
                } catch (SQLException ex) {
                    
                    JOptionPane.showMessageDialog(null, ex);
                }  
            }
        }
    return idReg;
}
public void actualizar_campo_reg_actus(int id_reg,String nom_actu){
    String nomf = nom_actu +"-"+ String.valueOf(id_reg);
    String SSQL = "UPDATE reg_actuaciones SET nom_id_reg = '"+nomf+"' WHERE id_reg_actu = '"+id_reg+"' "; //Creamos la Consulta SQL
    try {
      pst = conex8.conexBase().prepareStatement(SSQL);
            pst.executeUpdate();
            JOptionPane.showMessageDialog(null, "Operación (actualizar:[nmir-3591]) realizada correctamente");
             } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e+"Testigo");
        }
        
}
public int consultar_id_jardi_nom_jardi(String nomJardi){
    int idJardi = 0;
    String SSQL = "SELECT id_jardinero FROM jardinero WHERE nombre_jardinero = '"+ nomJardi +"' ";
     try {
      pst = conex4.conexBase().prepareStatement(SSQL);
       rset =  pst.executeQuery();
       while(rset.next()){
       idJardi = rset.getInt("id_jardinero");
       }
            JOptionPane.showMessageDialog(null, "Operación (consulta:[jrd-748]) realizada correctamente");
             } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e);
        }
    return idJardi;
}
public void añadir_rel_actu_jar(int idRegActu , jardineros jardi){
    String SSQL = "INSERT INTO rel_reg_jar(id_reg_actu,id_jar,nom_jar) VALUES( '"+idRegActu+"','"+jardi.getIdJardinero()+"','"+jardi.getNombreJardinero()+"' )"; //Creamos la Consulta SQL
     try {
      pst = conex5.conexBase().prepareStatement(SSQL);
            pst.executeUpdate();
            JOptionPane.showMessageDialog(null, "Operación (agregar:[rljr-2045]) realizada correctamente");
             } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e);
        }
}
public void consultar_jardis_actu(JList jListJardis, int idRegActu){
        try {
            String SSQL = "SELECT nom_jar FROM rel_reg_jar WHERE id_reg_actu = "+idRegActu+" ";                 
            DefaultListModel modelo = new DefaultListModel();
            jListJardis.setModel(modelo);
            pst = conex.conexBase().prepareStatement(SSQL);
            rset = pst.executeQuery();
            while(rset.next()){
                modelo.addElement(rset.getString("nom_jar"));
            }  
        } catch (SQLException ex) {
            Logger.getLogger(sqlStmts.class.getName()).log(Level.SEVERE, null, ex);
        }
    
}
public void consultar_id_reg_actu(JTextField jtfIdRegActu,JTextField jtfDura, String nomIdReg){
        try {
            String SSQL = "SELECT id_reg_actu FROM reg_actuaciones WHERE nom_id_reg = '"+nomIdReg+"'";
            pst = conex.conexBase().prepareStatement(SSQL);
            rset = pst.executeQuery();
            rset.next();
            String r = String.valueOf(rset.getInt("id_reg_actu"));
            String r2 = String.valueOf(consultar_duracion_actu(rset.getInt("id_reg_actu")));
            
            jtfIdRegActu.setText(r);
            jtfDura.setText(r2);
          
        } catch (SQLException ex) {
            Logger.getLogger(sqlStmts.class.getName()).log(Level.SEVERE, null, ex);
        }
    
}
public int consultar_duracion_actu(int idRegActu){
     int duracion = 0;
        try {
            String SSQL = "SELECT duracion from reg_actuaciones WHERE id_reg_actu = '"+ idRegActu+"' ";
            
            pst = conex9.conexBase().prepareStatement(SSQL);
            rset = pst.executeQuery();
            rset.next();
            duracion = rset.getInt("duracion");
        } catch (SQLException ex) {
            Logger.getLogger(sqlStmts.class.getName()).log(Level.SEVERE, null, ex);
        }
        return duracion;
}
public void actualizar_duracion_actu(actuaciones act, int id_reg){
    
    String SSQL = "UPDATE reg_actuaciones SET duracion = '"+act.getDuracion()+"' WHERE id_reg_actu = '"+id_reg+"' "; //Creamos la Consulta SQL
    try {
      pst = conex8.conexBase().prepareStatement(SSQL);
            pst.executeUpdate();
            JOptionPane.showMessageDialog(null, "Operación (actualizar:[nmir-3591]) realizada correctamente");
             } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e+"Testigo");
        }
    
}
public void consultar_dni_jardi(JTextField jtfDniJardi, String nomJardi){
    String SSQL = "SELECT dni FROM jardinero WHERE nombre_jardinero = '"+nomJardi+"'";
     try {
            pst = conex.conexBase().prepareStatement(SSQL);
            rset = pst.executeQuery();
           
            rset.next();
            String dni = rset.getString("dni");
            jtfDniJardi.setText(dni);
        } catch (SQLException ex) {
            Logger.getLogger(sqlStmts.class.getName()).log(Level.SEVERE, null, ex);
        }
}

 
 public ResultSet versionDB() {
        try {
            
            stmt = conex.conexBase().createStatement();
            rset = stmt.executeQuery("select BANNER from SYS.V_$VERSION");
            
                while (rset.next())
                {
                 System.out.println (rset.getString(1));  // Print col 1
                }  
           
            }catch(SQLException ex) {
                       Logger.getLogger(conexion.class.getName()).log(Level.SEVERE, null, ex);
            }finally{
            
                if(conex!=null){
                    try {
                            conex.close();
                            conex=null;
                                        
                        } catch (SQLException ex) 
                            {
                                JOptionPane.showMessageDialog(null, ex);
                            }
                        }
                    }
    return rset;
    }
 public void dbtest(){
        
        try {
            
            DriverManager.registerDriver(new oracle.jdbc.driver.OracleDriver());
            Connection conn = conex.conexBase();
            CallableStatement cst = conn.prepareCall("{call saludo}");
            cst.execute();
            System.out.println("procedimiento ejecutado!!");
        } catch (SQLException ex) {
            Logger.getLogger(dao_parques.class.getName()).log(Level.SEVERE, null, ex);
        }
}
}