/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serversock;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

/**
 *
 * @author marco
 */
public class ServerSock {
    
    static final int PUERTO = 5000;
    
    public ServerSock() {
        try {
            ServerSocket skServidor = new ServerSocket(PUERTO);
            System.out.println("Escuchando el puerto: " + PUERTO);
            
            for (int numCli = 0; numCli < 3; numCli++) {
                try (Socket skCliente = skServidor.accept()) {
                    System.out.println("Sirvo al cliente: " + numCli);
                    OutputStream aux = skCliente.getOutputStream();
                    try (DataOutputStream flujo = new DataOutputStream(aux)) {
                        flujo.writeUTF("Hola cliente: " + numCli);
                        skCliente.close();
                    }
                }
            }
            
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        new ServerSock();
    }
    
}
