
package DAO;

import java.util.ArrayList;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import model.Pelicula;
import model.Usuario;


@Path("peliculasws")
public class PeliculaswsResource {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of PeliculaswsResource
     */
    public PeliculaswsResource() {
    }

    /**
     * Retrieves representation of an instance of DAO.PeliculaswsResource
     * @return an instance of java.lang.String
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getJson() {
        PeliculaDAO peliculaDao = new PeliculaDAO();
        ArrayList<Pelicula> peliculas = peliculaDao.
                findAll(null);
        return Pelicula.toArrayJSon(peliculas);
    }
    
    /**
     * PUT method for updating or creating an instance of PeliculaswsResource
     * @param content representation for the resource
     */
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public void putJson(String content) {
    }
    
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String add(Pelicula pelicula){
        PeliculaDAO peliculaDao = new PeliculaDAO();
        int resp = peliculaDao.add(pelicula);
        return Pelicula.toObjectJson(pelicula);
    }
    
    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{id}")
    public String delete(@PathParam("id") String id ){
        PeliculaDAO peliculaDao = new PeliculaDAO();
        int resp = peliculaDao.delete(Integer.parseInt(id));
        //return Pelicula.toObjectJson(pelicula);
        return "";
    }
}
