/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicioecuacion;

import java.util.Scanner;

/**
 *
 * @author marco
 */
public class Ecuacion {
    double a,b,c;
    
    Scanner sc = new Scanner(System.in);
    
    
    public void leerCoeficientes(){
        
        System.out.println("Introduce el primer coeficiente (a):");
        a=sc.nextDouble();
        System.out.println("Introduce el primer coeficiente (b):");
        b=sc.nextDouble();
        System.out.println("Introduce el primer coeficiente (c):");
        c=sc.nextDouble();
    }
    
    public String calcularSoluciones(){
        double x1,x2,d;
        String mensaje;
         d=((b*b)-4*a*c);
        if(d<0){
            mensaje = "No existen soluciones reales";
        
        }else{
            x1=(-b+Math.sqrt(d))/(2*a);
            x2=(-b-Math.sqrt(d))/(2*a);
            mensaje="Solucion: "+x1;
            mensaje+=" | Solucion: "+x2;
        }
        
        return mensaje;
    }
}
