/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vehiculos;
import java.util.Scanner;
/**
 *
 * @author marco
 */
public class Coche {
    
    private String matricula;
    public String modelo;
    private String color;
    
    private static int numCoches;
    static {
        numCoches = 0;
    }
    public Coche(){
       numCoches++; 
    }
    public Coche(String matricula, String modelo, String color){
        this.matricula = matricula;
        this.modelo = modelo;
        this.color = color;
        numCoches++;
    }
    public Coche(Coche otroCoche){
        this.color = otroCoche.color;
        this.matricula = otroCoche.matricula;
        this.modelo = otroCoche.modelo;
        numCoches++;
    }
    
    public String getMatricula(){
        return matricula;
    }
    public void setMatricula(String matricula){
        this.matricula = matricula;
    }
    
    public void leer(){
        Scanner sc = new Scanner(System.in);
        System.out.println("Introduce el numero de la matricula:");
        matricula = sc.next();
        System.out.println("Introduce el modelo:");
        modelo = sc.next();
        System.out.println("Introduce el color:");
        color = sc.next();
    }
    
    public void ImprimeTotalCoches(){
         System.out.println("Número total de coches: " + numCoches);
    }
    public int getTotalCoches() {
        return numCoches;
    }
}
