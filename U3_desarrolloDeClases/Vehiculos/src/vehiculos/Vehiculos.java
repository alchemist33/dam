
package vehiculos;
import java.util.Scanner;
/**
 *
 * @author marco
 */
public class Vehiculos {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Coche C1 = new Coche();
        Coche coche1 = new Coche();
        Coche coche2 = new Coche("1234","Ibiza","Azul");
        Coche coche3 = new Coche(coche2);

        
        Scanner sc = new Scanner(System.in);
        C1.leer();
        System.out.println(C1.modelo);
        System.out.println("Introduce otro modelo para C1:");
        C1.modelo = sc.next();
        System.out.println(C1.modelo);
        
        System.out.println(C1.getMatricula());
        System.out.println("modelo coche1 "+ coche1.modelo);
        System.out.println("modelo coche2 "+ coche2.modelo);
        System.out.println("modelo coche3 "+ coche3.modelo);
        coche2.modelo = "Arosa";
        System.out.println("modelo coche1 "+ coche1.modelo);
        System.out.println("modelo coche2 "+ coche2.modelo);
        System.out.println("modelo coche3 "+ coche3.modelo);
        
        C1.ImprimeTotalCoches();
        System.out.println("Numero total de coches: " + C1.getTotalCoches());
    }
}
