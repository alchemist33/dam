/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package equalsytostring;

/**
 *
 * @author marco
 */
public class Persona {
 private String nombre;   
 private String apellido;   
 private int edad;   
 
 public Persona(String nombre,String apellido, int edad){
     this.nombre = nombre;
     this.apellido = apellido;
     this.edad   = edad;
 }
 @Override
 public String toString(){
    String aux;
    aux = "Me llamo: "+nombre+" "+apellido+" y mi edad es "+ edad;
    return aux;
}
 @Override
 public boolean equals(Object obj){
     if (this == obj){
         return true;
     }
     if (obj == null){
         return false;
     }
     if (getClass() != obj.getClass()){
         return false;
     }
     
     Persona objP =(Persona) obj;
     if (this.edad != objP.edad){
         return false;
     }
     if (!this.nombre.equals(objP.nombre)){
         return false;
     }
     if (!this.apellido.equals(objP.apellido)){
         return false;
     }
     return true;
 }
}
