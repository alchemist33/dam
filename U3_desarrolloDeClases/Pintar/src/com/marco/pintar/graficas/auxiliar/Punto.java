/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.marco.pintar.graficas.auxiliar;



/**
 *
 * @author marco
 */
public class Punto {
    public double x,y;
    
    public Punto(){
        x = 0;
        y = 0;
    }
    public Punto(double x,double y){
        this.x = x;
        this.y = y;
    }
    public Punto(Punto otroPunto){
        this.x = otroPunto.x;
        this.y = otroPunto.y;
    }
    
    public double getX(){
        return x;
    }
    public double getY(){
        return y;
    }
    public void setX(double x){
        this.x=x;
    }
    public void setY(double y){
        this.y=y;
    }
    
}
