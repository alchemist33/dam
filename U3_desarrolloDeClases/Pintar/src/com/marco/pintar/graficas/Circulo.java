/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.marco.pintar.graficas;
import com.marco.pintar.graficas.auxiliar.Punto; 
/**
 *
 * @author marco
 */
public class Circulo {
    public Punto centro;
    double radio;
    
    
    private Circulo(){
        centro = new Punto(0,0);
        radio = 0;
    }
    public Circulo (Punto p){
        this();
        this.centro.x = p.x;
        this.centro.x = p.y;
    }
    public Circulo(Punto centro,double radio){
        this.radio = radio;
        this.centro = new Punto(centro);
    }
    public Circulo(Circulo otroCirculo){
        
        this.centro = new Punto(otroCirculo.centro);
        this.radio = otroCirculo.radio;
    }
    
    public void Pintar(){
        
    }
    public void Pintar(Punto p){
        
    }
}
