/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.marco.pintar.graficas;
import com.marco.pintar.graficas.auxiliar.Punto; 
/**
 *
 * @author marco
 */
public class Triangulo {
    double lado,altura;
    
    public Triangulo(){
        lado = 0;
        altura = 0;
    }
    public Triangulo(double lado,double altura){
        this.lado = lado;
        this.altura = altura;
    }
    public Triangulo(Triangulo otroTriangulo){
        this.lado = otroTriangulo.lado;
        this.altura = otroTriangulo.altura;
    }
    
    public void Pintar(){
        
    }
    public void Pintar(Punto p){
        
    }
    
}
