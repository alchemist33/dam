/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package existdb;

//import org.exist.Database;
//import org.exist.collections.Collection;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xquery.XQException;


import javax.xml.xquery.XQPreparedExpression;
import javax.xml.xquery.XQResultSequence;
//

import org.xmldb.api.DatabaseManager;
import org.xmldb.api.base.Collection;
import org.xmldb.api.base.Database;
import org.xmldb.api.base.Resource;
import org.w3c.dom.Node;
import org.xmldb.api.base.ResourceIterator;
import org.xmldb.api.base.ResourceSet;
import org.xmldb.api.base.XMLDBException;
import org.xmldb.api.modules.CollectionManagementService;
import org.xmldb.api.modules.XMLResource;
import org.xmldb.api.modules.XPathQueryService;


import net.xqj.exist.ExistXQDataSource;
import javax.xml.xquery.XQDataSource;
import javax.xml.xquery.XQConnection;




/**
 *
 * @author marco
 */
public class ExistDB {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        //ejemploEmpleadosDpto10();
        //crearDoc();
        //borrarDoc();
        //borrarColeccion();
        //listaRecursosColecciones();
        //bajarDocumeto();
        //ejecutarConsultaFichero("./miniconsulta.xq");
        consultaXJQ();
    }

    public static void ejemploEmpleadosDpto10() {
        String driver = "org.exist.xmldb.DatabaseImpl";
        Collection col = null;
        Collection col2 = null;
        String URI = "xmldb:exist://localhost:8080/exist/xmlrpc/db";
        String usu = "admin";
        String pass = "admin";

        try {
            Class c1 = Class.forName(driver);
            Database database = (Database) c1.newInstance();
            DatabaseManager.registerDatabase(database);

            col = DatabaseManager.getCollection(URI, usu, pass);
            col2 = DatabaseManager.getCollection(URI, usu, pass);

            if (col == null) {
                System.out.println("LA COLECCION NO EXISTE");
            }

            //Servicio Xpath
            XPathQueryService servicio = (XPathQueryService) col.getService("XPathQueryService", "1.0");
            ResourceSet result = servicio.query("for $emp in /EMPLEADOS/fila_emple[DEPT_NO=10] return $emp");

            //Servicio ColletionManager
            CollectionManagementService mgtService = (CollectionManagementService) col2.getService("CollectionManagementService", "1.0");
            mgtService.createCollection("NUEVA_COLECCION2");
            System.out.println("COLECCION CREADA: " + col2);

            ResourceIterator i;
            i = result.getIterator();
            if (!i.hasMoreResources()) {
                System.out.println("LA CONSULTA NO DEVUELVE NADA");
            }
            while (i.hasMoreResources()) {
                Resource r = i.nextResource();
                System.out.println((String) r.getContent());
            }

        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException | XMLDBException e) {

            System.out.println("Error al inicializar la BD eXist");
            e.printStackTrace();
        }

    }

    public static void crearDoc() {

        try {
            String driver = "org.exist.xmldb.DatabaseImpl";
            Collection col = null;
            String URI = "xmldb:exist://localhost:8080/exist/xmlrpc/db/NUEVA_COLECCION";
            String usu = "admin";
            String pass = "admin";

            Class c1 = Class.forName(driver);
            Database database = (Database) c1.newInstance();
            DatabaseManager.registerDatabase(database);
            col = DatabaseManager.getCollection(URI, usu, pass);
            File archivo = new File("C://UD5/NUEVAS_ZONAS.xml");

            if (!archivo.canRead()) {
                System.out.println("ERROR AL CREAR EL FICHERO");
            } else {
                Resource nuevoRecurso = col.createResource(archivo.getName(), "XMLResource");
                nuevoRecurso.setContent(archivo);
                col.storeResource(nuevoRecurso);
                System.out.println("FICHERO AÑADIDO");
            }

        } catch (ClassNotFoundException | XMLDBException | InstantiationException | IllegalAccessException ex) {
            Logger.getLogger(ExistDB.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public static void borrarDoc() {

        try {
            String driver = "org.exist.xmldb.DatabaseImpl";
            Collection col = null;
            String URI = "xmldb:exist://localhost:8080/exist/xmlrpc/db/NUEVA_COLECCION";
            String usu = "admin";
            String pass = "admin";

            Class c1 = Class.forName(driver);
            Database database = (Database) c1.newInstance();
            DatabaseManager.registerDatabase(database);
            col = DatabaseManager.getCollection(URI, usu, pass);
            File fichero = new File("C://UD5/NUEVAS_ZONAS.xml");

            Resource recursoParaBorrar = col.getResource("NUEVAS_ZONAS.xml");
            col.removeResource(recursoParaBorrar);
            System.out.println("FICHERO BORRADO");
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | XMLDBException ex) {
            Logger.getLogger(ExistDB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void borrarColeccion() {

        try {
            String driver = "org.exist.xmldb.DatabaseImpl";
            Collection col = null;
            String URI = "xmldb:exist://localhost:8080/exist/xmlrpc/db";
            String usu = "admin";
            String pass = "admin";

            Class c1 = Class.forName(driver);
            Database database = (Database) c1.newInstance();
            DatabaseManager.registerDatabase(database);
            col = DatabaseManager.getCollection(URI, usu, pass);

            CollectionManagementService mgtService = (CollectionManagementService) col.getService("CollectionManagementService", "1.0");
            mgtService.removeCollection("NUEVA_COLECCION");
            System.out.println("COLECCION borrada: " + col.getName());

        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | XMLDBException ex) {
            Logger.getLogger(ExistDB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void listaRecursosColecciones() {

        try {
            String driver = "org.exist.xmldb.DatabaseImpl";
            Collection col = null;
            String URI = "xmldb:exist://localhost:8080/exist/xmlrpc/db/";
            String usu = "admin";
            String pass = "admin";

            Class c1 = Class.forName(driver);
            Database database = (Database) c1.newInstance();
            DatabaseManager.registerDatabase(database);
            col = DatabaseManager.getCollection(URI, usu, pass);

            System.out.println("Numero de colecciones: " + col.getChildCollectionCount());

            String[] colecciones = col.listChildCollections();
            for (int j = 0; j < colecciones.length; j++) {
                System.out.println("------------------------------");
                System.out.println(colecciones[j]);
                Collection colecc = col.getChildCollection(colecciones[j]);

                String[] lista = colecc.listResources();
                for (int i = 0; i < lista.length; i++) {
                    Resource res = (Resource) colecc.getResource(lista[i]);
                    System.out.println("ID del documento: " + res.getId());
                    System.out.println("Contenido del documento:\n " + res.getContent());
                }
            }

        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | XMLDBException ex) {
            Logger.getLogger(ExistDB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void bajarDocumeto() {

        try {
            String driver = "org.exist.xmldb.DatabaseImpl";
            Collection col = null;
            String URI = "xmldb:exist://localhost:8080/exist/xmlrpc/db/ColeccionPruebas";
            String usu = "admin";
            String pass = "admin";

            Class c1 = Class.forName(driver);
            Database database = (Database) c1.newInstance();
            DatabaseManager.registerDatabase(database);
            col = DatabaseManager.getCollection(URI, usu, pass);

            XMLResource res = (XMLResource) col.getResource("zonas.xml");

            if (res == null) {
                System.out.println("NO EXISTE EL DOCUMENTO");
            } else {
                System.out.println("ID del documento: " + res.getDocumentId());
                Node document = res.getContentAsDOM();
                Source source = new DOMSource(document);
                Transformer transformer = TransformerFactory.newInstance().newTransformer();
                Result console = new StreamResult(System.out);
                transformer.transform(source, console);

                Result fichero = new StreamResult(new File("./zonas.xml"));
                transformer.transform(source, fichero);
            }

        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | XMLDBException | TransformerConfigurationException ex) {
            Logger.getLogger(ExistDB.class.getName()).log(Level.SEVERE, null, ex);
        } catch (TransformerException ex) {
            Logger.getLogger(ExistDB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void ejecutarConsultaFichero(String fichero) {

        try {
            String driver = "org.exist.xmldb.DatabaseImpl";
            Collection col = null;
            String URI = "xmldb:exist://localhost:8080/exist/xmlrpc/db/ColeccionPruebas";
            String usu = "admin";
            String pass = "admin";

            Class c1 = Class.forName(driver);
            Database database = (Database) c1.newInstance();
            DatabaseManager.registerDatabase(database);
            col = DatabaseManager.getCollection(URI, usu, pass);
            System.out.println("Convirtiendo el fichero a cadena...");
            BufferedReader entrada = new BufferedReader(new FileReader(fichero));
            String linea = null;
            StringBuilder stringBuilder = new StringBuilder();
            String salto = System.getProperty("line.separator");

            while ((linea = entrada.readLine()) != null) {
                stringBuilder.append(linea);
                stringBuilder.append(salto);
            }

            String consulta = stringBuilder.toString();

            System.out.println("Consulta: " + consulta);
            XPathQueryService servicio = (XPathQueryService) col.getService("XPathQueryService", "1.0");
            ResourceIterator i;
            ResourceSet result = servicio.query(consulta);
            i = result.getIterator();
            if (!i.hasMoreResources()) {
                System.out.println("LA CONSULTA NO DEVUELVE NADA");
            }

            while (i.hasMoreResources()) {
                Resource r = i.nextResource();
                System.out.println("Elemento: " + (String) r.getContent());
            }

        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | XMLDBException | FileNotFoundException ex) {
            Logger.getLogger(ExistDB.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ExistDB.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public static void consultaXJQ() {

        try {
            XQDataSource server = new ExistXQDataSource();
            server.setProperty("serverName", "localhost");
            server.setProperty("port", "8080");
            server.setProperty("user", "admin");
            server.setProperty("password", "admin");
            XQConnection conn = (XQConnection) server.getConnection();

            XQPreparedExpression consulta;
            XQResultSequence resultado;

            consulta = conn.prepareExpression("/EMPLEADOS/EMP_ROW[DEPT_NO=10]");
            resultado = consulta.executeQuery();
            while (resultado.next()) {
                System.out.println("Elemento: " + resultado.getSequenceAsString(null));
            }

            conn.close();

        } catch (XQException ex) {
            Logger.getLogger(ExistDB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
