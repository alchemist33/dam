package caracterisitcasfichero;

import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import objects.Persona;
import objects.utils.CustomObjectOutPutStream;

/**
 *
 * @author marco
 */
public class EscrituraYLecturaFicherosBinarios {

    public void EscribirFichero() {
        try {
            File fichero = new File("ficheroBytes.dat");

            FileOutputStream fileout = new FileOutputStream(fichero, true);

            FileInputStream filein = new FileInputStream(fichero);
            int i;
            for (i = 1; i < 100; i++) {
                fileout.write(i);
            }
            fileout.close();

            while ((i = filein.read()) != -1) {
                System.out.println(i);

            }

        } catch (IOException ioe) {
            System.err.println(ioe.getMessage());
        }
    }

    public void escribirDatosPrimitivos() {
        try {
            File fichero = new File("escribeFicheroData.dat");
            DataOutputStream dataOS = new DataOutputStream(new FileOutputStream(fichero));

            String nombres[] = {"Alex", "Marco", "Maria", "David", "Juan", "Javier", "Ana", "Luis", "Pedro", "Antonio"};

            int edades[] = {29, 25, 31, 28, 21, 18, 19, 23, 22, 27};

            for (int i = 0; i < edades.length; i++) {
                dataOS.writeUTF(nombres[i]);
                dataOS.writeInt(edades[i]);
            }
            dataOS.close();
        } catch (IOException ioe) {
            System.err.println(ioe.getMessage());
        }

    }

    public void leerDatosPrimitivos() {
        File fichero = new File("leerFicheroData.dat");
        FileInputStream filein = null;
        try {
            filein = new FileInputStream(fichero);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(EscrituraYLecturaFicherosBinarios.class.getName()).log(Level.SEVERE, null, ex);
        }
        DataInputStream dataIS = new DataInputStream(filein);

        String nombre;
        int edad;
        try {

            while (true) {
                nombre = dataIS.readUTF();
                edad = dataIS.readInt();
                System.out.printf("Nombre: %s, edad: %d %n", nombre, edad);
            }
        } catch (EOFException EOFex) {
            System.err.println("Fin de la lectura");
            if (EOFex.getMessage() == null) {
                System.out.println("MensajeEOF: Se ha alcanzado el final del fichero.");
            } else {
                System.err.println("MensajeEOF: " + EOFex.getMessage());
            }
            System.out.println("======LOG======");
            Logger.getLogger(EscrituraYLecturaFicherosBinarios.class.getName()).log(Level.INFO, "Lectura correcta", EOFex);

        } catch (FileNotFoundException fn) {
            System.err.println("MensajeFN: " + fn.getMessage());
            Logger.getLogger(EscrituraYLecturaFicherosBinarios.class.getName()).log(Level.SEVERE, null, fn);

        } catch (IOException ioe) {
            System.err.println("MensajeIOE: " + ioe.getMessage());
            Logger.getLogger(EscrituraYLecturaFicherosBinarios.class.getName()).log(Level.SEVERE, null, ioe);
        } finally {
            try {
                System.out.println("Cerrando Flujo.....");
                dataIS.close();
                System.out.println("Flujo cerrado");
            } catch (IOException ioe) {
                System.err.println("MensajeIOE2: " + ioe.getMessage());
                Logger.getLogger(EscrituraYLecturaFicherosBinarios.class.getName()).log(Level.SEVERE, null, ioe);

            }
        }
    }

    public void escribeObjetoEnFichero() {
        try {
            File fichero = new File("FicheroPersonas.dat");
            FileOutputStream fileout = new FileOutputStream(fichero);
            ObjectOutputStream dataOS = new ObjectOutputStream(fileout);

            String nombres[] = {"Alex", "Marco", "Maria", "David", "Juan", "Javier", "Ana", "Luis", "Pedro", "Antonio"};
            int edades[] = {29, 25, 31, 28, 21, 18, 19, 23, 22, 27};

            for (int i = 0; i < edades.length; i++) {
                Persona persona = new Persona(nombres[i], edades[i]);
                dataOS.writeObject(persona);
            }
            dataOS.close();
        } catch (IOException ioe) {
            System.err.println("Mensaje: " + ioe.getMessage());
            Logger.getLogger(EscrituraYLecturaFicherosBinarios.class.getName()).log(Level.SEVERE, null, ioe);

        }

    }

    public void escribeObjetoEnFicheroV2() {
        try {
            File fichero = new File("ficheroObjetos.dat");
            ObjectOutputStream dataOS;
            if (!fichero.exists()) {
                FileOutputStream fileOS;
                fileOS = new FileOutputStream(fichero);
                dataOS = new ObjectOutputStream(fileOS);
            } else {
                dataOS = new CustomObjectOutPutStream(new FileOutputStream(fichero, true));
            }
            String nombres[] = {"Alex", "Marco", "Maria", "David", "Juan", "Javier", "Ana", "Luis", "Pedro", "Antonio"};
            int edades[] = {29, 25, 31, 28, 21, 18, 19, 23, 22, 27};

            for (int i = 0; i < edades.length; i++) {
                Persona persona = new Persona(nombres[i], edades[i]);
                dataOS.writeObject(persona);
            }

        } catch (FileNotFoundException FNex) {
            System.err.println("Mensaje: " + FNex.getMessage());
            Logger.getLogger(EscrituraYLecturaFicherosBinarios.class.getName()).log(Level.SEVERE, null, FNex);
        } catch (IOException IOex) {
            System.err.println("Mensaje: " + IOex.getMessage());
            Logger.getLogger(EscrituraYLecturaFicherosBinarios.class.getName()).log(Level.SEVERE, null, IOex);
        }

    }

    public void LeerObjetosFicheroBinario() {
        try {
            File fichero = new File("ficheroObjetos.dat");
            Persona persona;
            ObjectInputStream dataIS = new ObjectInputStream(new FileInputStream(fichero));

            try {

                while (true) {
                    persona = (Persona) dataIS.readObject();
                    System.out.printf("Nombre: %s edad: %d %n", persona.getNombre(), persona.getEdad());
                }
            } catch (EOFException EOFex) {
                System.err.println("Fin de la lectura");
                if (EOFex.getMessage() == null) {
                    System.out.println("MensajeEOF: Se ha alcanzado el final del fichero ");
                } else {
                    System.err.println("MensajeEOF: " + EOFex.getMessage());
                }
                System.out.println("======LOG======");
                Logger.getLogger(EscrituraYLecturaFicherosBinarios.class.getName()).log(Level.INFO, "Lectura correcta", EOFex);

            } catch (IOException | ClassNotFoundException MULTIex) {
                System.err.println("MensajeMULTI: " + MULTIex.getMessage());
                System.out.println("======LOG======");
                Logger.getLogger(EscrituraYLecturaFicherosBinarios.class.getName()).log(Level.SEVERE, null, MULTIex);
            }
            dataIS.close();

        } catch (IOException IOex) {
            System.err.println("MensajeIO: " + IOex.getMessage());
            System.out.println("======LOG======");
            Logger.getLogger(EscrituraYLecturaFicherosBinarios.class.getName()).log(Level.SEVERE, null, IOex);
        }

    }
}
