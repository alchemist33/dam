package caracterisitcasfichero;

import org.w3c.dom.*;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;
import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.xml.sax.SAXException;

/**
 *
 * @author marco
 */
public class AccesoDOM {

    public void creaXML() {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {

            DocumentBuilder builder = factory.newDocumentBuilder();
            DOMImplementation implementation = builder.getDOMImplementation();
            Document document = implementation.createDocument(null, "Empleados", null);
            document.setXmlVersion("1.0");

            Element raiz = document.createElement("empleado"); //nodo empleado
            document.getDocumentElement().appendChild(raiz);

            Element elem = document.createElement("id");
            Text text = document.createTextNode(Integer.toString(33)); // damos valor
            raiz.appendChild(elem); // pegamos el elemento hijo a la raiz
            elem.appendChild(text); // pegamos el valor

            Source source = new DOMSource(document);
            Result result = new StreamResult(new File("DAMdocs", "Empleados.xml"));
            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            transformer.transform(source, result);

        } catch (ParserConfigurationException | TransformerException MULTIex) {
            Logger.getLogger(AccesoDOM.class.getName()).log(Level.SEVERE, null, MULTIex);
        }

    }

    public void LeerXML() {

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document document = builder.parse(new File("DAMdocs", "Empleados.xml"));

            NodeList empleados = document.getElementsByTagName("empleado");

            for (int i = 0; i < empleados.getLength(); i++) {

                Node emple = empleados.item(i);
                if (emple.getNodeType() == Node.ELEMENT_NODE) {
                    Element elemento = (Element) emple;
                    System.out.printf("ID: %s %n", elemento.getElementsByTagName("id").item(0).getTextContent());
                }
            }

        } catch (IOException | ParserConfigurationException | DOMException | SAXException MULTIex) {
            Logger.getLogger(AccesoDOM.class.getName()).log(Level.SEVERE, null, MULTIex);
        }
    }

}
