package caracterisitcasfichero;

import java.util.ArrayList;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import objects.Libro;
import objects.utils.Libreria;

/**
 *
 * @author marco
 */
@XmlRootElement(name = "MiClaseRaiz")
@XmlType(name = "Otronombre")
public class AccesoJAXB {

    private String atributo;

    @XmlElement(name = "Atributo1")

    public String getAtributo() {
        return this.atributo;
    }

    @XmlElementWrapper(name = "ListaLibro")
    @XmlElement(name = "Libro")
    
    public ArrayList<Libro> getListaLibro() {
        return listaLibro;
    }

}
