package caracterisitcasfichero;

import com.thoughtworks.xstream.XStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import objects.Persona;
import objects.utils.ListaPersonas;

/**
 *
 * @author marco
 */
public class ManipulacionXstream {

    public ManipulacionXstream() {

    }

    public void creaXMLXStream() {
        XStream xstream = new XStream();
        xstream.alias("ListaPersonasMunicipio", ListaPersonas.class);
        xstream.alias("DatosPersona", Persona.class);
        xstream.aliasField("Nombre_alumno", Persona.class, "nombre");
        xstream.aliasField("Edad_alumno", Persona.class, "edad");
        xstream.addImplicitCollection(ListaPersonas.class, "lista");

       ListaPersonas listaPersonas = new ListaPersonas();
        Persona per = new Persona("Marco", 32);
       listaPersonas.add(per);

        

        try {
            
            xstream.toXML(listaPersonas, new FileOutputStream("Personas2.xml"));
            ListaPersonas listaTodas = (ListaPersonas) xstream.fromXML(new FileInputStream("Personas.xml"));
            xstream.toXML(listaTodas, new FileOutputStream("Personas2Todas.xml"));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ManipulacionXstream.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
