package caracterisitcasfichero;


import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.helpers.XMLReaderFactory;

/**
 *
 * @author marco
 */
public class AccesoSAX extends DefaultHandler {

    public AccesoSAX() {
        super();
    }

    @Override
    public void characters(char[] ch, int inicio, int longitud) throws SAXException {
        String car = new String(ch,  inicio,longitud);
        super.characters(ch, inicio, longitud); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void endElement(String uri, String nombre, String nombreC) throws SAXException {
        System.out.printf("\t Fin procesado etiqueta: %s %n", nombre);
        super.endElement(uri, nombre, nombreC); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void startElement(String uri, String nombre, String nombreC, Attributes atts) throws SAXException {
        System.out.printf("\t Principio procesado etiqueta: %s %n", nombre);
        super.startElement(uri, nombre, nombreC, atts); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void endDocument() throws SAXException {
        System.out.println("Final del procesado del documentoXML");
        super.endDocument(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void startDocument() throws SAXException {
        System.out.println("Comienzo del procesado del documentoXML");
        super.startDocument(); //To change body of generated methods, choose Tools | Templates.
    }

    public void leerXMLSAX() {

        try {
            AccesoSAX gestor = new AccesoSAX();
            XMLReader procesadorXML = XMLReaderFactory.createXMLReader();
            procesadorXML.setContentHandler(gestor);
            InputSource fileXML = new InputSource("build.xml");
            procesadorXML.parse(fileXML);
                      
        } catch (SAXException | IOException MULTIex) {
            Logger.getLogger(AccesoSAX.class.getName()).log(Level.SEVERE, null, MULTIex);
        }  
    }
}
