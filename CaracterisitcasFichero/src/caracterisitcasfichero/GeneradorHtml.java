package caracterisitcasfichero;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

/**
 *
 * @author marco
 */
public class GeneradorHtml {
    
    public void creaHTML() {
        
        try {
            String hojaEstilo = "plantillaEmpleados.xsl";
            String datosAlumnos = "datosEmpleados.xml";
            File pagHtml = new File("mipagina.html");
            FileOutputStream os = new FileOutputStream(pagHtml);
            
            Source estilos = new StreamSource(hojaEstilo);
            Source datos = new StreamSource(datosAlumnos);
            Result result = new StreamResult(os);
            
            Transformer transformer = TransformerFactory.newInstance().newTransformer(estilos);
            transformer.transform(datos, result);
            os.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(GeneradorHtml.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException | TransformerConfigurationException ex) {
            Logger.getLogger(GeneradorHtml.class.getName()).log(Level.SEVERE, null, ex);
        } catch (TransformerException ex) {
            Logger.getLogger(GeneradorHtml.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
}
