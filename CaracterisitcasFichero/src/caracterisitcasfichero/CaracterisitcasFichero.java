package caracterisitcasfichero;

public class CaracterisitcasFichero {
 // <editor-fold defaultstate="collapsed" desc="Objetos de Clases">
    private static final EscrituraYLecturaFicherosCaracteres EYLC = new EscrituraYLecturaFicherosCaracteres();
    private static final EscrituraYLecturaFicherosBinarios EYLB = new EscrituraYLecturaFicherosBinarios();
    private static final EscrituraYLecturaAleatoria EYLAL = new EscrituraYLecturaAleatoria();
    private static final AccesoDOM AD = new AccesoDOM();
    private static final AccesoSAX AS = new AccesoSAX();
    private static final ManipulacionXstream MPXS = new ManipulacionXstream();
    private static final GeneradorHtml GHTML = new GeneradorHtml();
// </editor-fold>
    public static void main(String[] args) {

        //InicializarEYLC();
        //InicializarEYLB();
        //InicializarEYAL();
        //InicializarADYAS();
        //InicializarMPXS();
        InicializarGHTML();
    }

    private static void InicializarEYLC() {
//        EYLC.VerAtributos();
//        EYLC.VerAtribistosLista();
//        EYLC.CrearDirectorio();
//        EYLC.EscribeFicheroTexto();
//        EYLC.AñadeTexto();
//        EYLC.AñadeLinea();
//        EYLC.AñadeLineaPrint();
//        EYLC.LeerFichero();
//        EYLC.LeerLinea();
    }

    private static void InicializarEYLB() {

        //EYLB.EscribirFichero();
        //EYLB.escribirDatosPrimitivos();
        //EYLB.leerDatosPrimitivos();
        //EYLB.escribeObjetoEnFichero();
        //EYLB.escribeObjetoEnFicheroV2();
        //EYLB.LeerObjetosFicheroBinario();
    }

    private static void InicializarEYAL() {

        //EYLAL.EscribirFicheroAleatorio();
        //EYLAL.LeerFicheroAleatorio();
    }

    private static void InicializarADYAS() {
        //AD.creaXML();
        //AD.LeerXML();
        //AS.leerXMLSAX();

    }

    private static void InicializarMPXS() {
        //MPXS.creaXMLXStream();
    }

    private static void InicializarGHTML() {
        GHTML.creaHTML();
    }

}
