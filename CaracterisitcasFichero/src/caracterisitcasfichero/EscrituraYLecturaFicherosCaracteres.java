package caracterisitcasfichero;

import java.io.*;

/**
 *
 * @author marco
 */
public class EscrituraYLecturaFicherosCaracteres {

    // <editor-fold defaultstate="collapsed" desc="Escritura en fichero">
    public void VerAtributos() {
        try {
            System.out.println("INFORMACION SOBRE EL FICHERO");

            File f = new File("src\\ad_ud1\\VerAtributos.java");
            if (f.exists()) {
                System.out.println("Nombre del fichero: " + f.getName());
                System.out.println("Tamaño: " + f.length());
                System.out.println("Ruta relativa: " + f.getPath());
                System.out.println("Ruta absoluta: " + f.getAbsolutePath());
                System.out.println("Ruta del directorio: " + f.getParent());
                System.out.println("Permisos de lectura: " + f.canRead());
                System.out.println("Permisos de lectura: " + f.canWrite());
                System.out.println("Es directorio: " + f.isDirectory());
                System.out.println("Es fichero: " + f.isFile());

            } else {
                System.out.println("El fichero no existe");
            }

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public void VerAtribistosLista() {
        String dir = ".";
        File f = new File(dir);

        String[] archivos = f.list();
        System.out.printf("Ficheros en el directorio actual: %d %n", archivos.length);
        for (String archivo : archivos) {
            File f2 = new File(f, archivo);
            System.out.printf("Nombre: %s, Esfichero: %b, EsDirectorio: %b %n", archivo, f2.isFile(), f2.isDirectory());
        }

    }

    public void CrearDirectorio() {
        File d = new File("NuevoDir");
        File f1 = new File(d, "Fichero1.txt");
        File f2 = new File(d, "Fichero2.txt");

        d.mkdir();
        try {
            if (f1.createNewFile()) {
                System.out.println("Fichero 1 creado correctamente");
            } else {
                System.out.println("No se ha podido crear el Fichero 1");
            }

            if (f2.createNewFile()) {
                System.out.println("Fichero 2 creado correctamente");
            } else {
                System.out.println("No se ha podido crear el Fichero 2");
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        f1.renameTo(new File(d, "Fichero1Nuevo"));

        try {
            File f3 = new File("NuevoDir/Fichero3.txt");
            f3.createNewFile();
        } catch (Exception e) {
        }
    }

    public void EscribeFicheroTexto() {
        String texto1 = "Contenido de mi fichero";
        String texto2 = "\nMas Contenido";
        String nombreFichero = "escribreFichero.txt";

        try {

            FileWriter fw = new FileWriter(nombreFichero);
            fw.write(texto1);
            fw.append(texto2);
            fw.close();
            System.out.printf("Fichero: %s escrito! %n", nombreFichero);
        } catch (IOException ioe) {
            System.out.printf("Error al acceder al fichero: %s", nombreFichero);
        }

    }

    public void AñadeTexto() {
        String texto1 = "Contenido de mi fichero";
        String texto2 = "\nMas Contenido";
        String nombreFichero = "escribreFichero.txt";

        try {

            FileWriter fw = new FileWriter(nombreFichero, true);

            fw.append(texto2);
            fw.close();
            System.out.printf("Fichero: %s escrito! %n", nombreFichero);
        } catch (IOException ioe) {
            System.out.printf("Error al acceder al fichero: %s", nombreFichero);
        }
    }

    public void AñadeLinea() {
        try {
            BufferedWriter fichero = new BufferedWriter(new FileWriter("escribreFicheroBuffer.txt"));

            for (int i = 0; i < 4; i++) {
                fichero.write("Esta es la linea " + i);
                fichero.newLine();
            }
            fichero.close();

        } catch (FileNotFoundException fn) {
            System.out.println("No se encuentra el fichero");
        } catch (IOException ioe) {
            System.out.println("Error de E/S");
        }
    }

    public void AñadeLineaPrint() {
        try {
            PrintWriter fichero = new PrintWriter(new FileWriter("escribeFicheroPrint.txt"));

            for (int i = 0; i < 4; i++) {
                fichero.println("Esta es la linea " + i);
            }
            fichero.close();

        } catch (FileNotFoundException fn) {
            System.out.println("No se encuentra el fichero");
        } catch (IOException ioe) {
            System.out.println("Error de E/S");
        }
    }
// </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Lectura en fichero">
    public void LeerFichero() {
        String nombre = "leerFichero.txt";
        File fichero = new File(nombre);
        try {
            FileReader fic = new FileReader(fichero);
            //char b[] = new char[32]; Para leer caracteres en bloques de 32
            int i;
            while ((i = fic.read(/*b*/)) != -1) {
                System.out.print((char) i);
                //System.out.print((b));
            }

            fic.close();
        } catch (IOException ioe) {
            System.err.println("error al acceder al fichero");
        }
    }

    public void LeerLinea() {
        String nombreF = "leerFichero.txt";

        File fic = new File(nombreF);
        try {

            BufferedReader fichero = new BufferedReader(new FileReader(fic));
            String linea;
            while ((linea = fichero.readLine()) != null) {
                System.out.println(linea);
            }

        } catch (FileNotFoundException fn) {
            System.out.println("No se encuentra el fichero");
        } catch (IOException ioe) {
            System.err.println("error de E/S");
        }
    }
    // </editor-fold>

}
