package caracterisitcasfichero;

import java.io.EOFException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author marco
 */
public class EscrituraYLecturaAleatoria {

    public void EscribirFicheroAleatorio() {
        try {
            File fichero = new File("ficheroAleatorio.dat");
            RandomAccessFile acceso = new RandomAccessFile(fichero, "rw");

            String apellido[] = {"Garcia", "Memba", "Lopez", "Fuentes", "Torres", "Sanchez", "Martinez"};
            int edades[] = {29, 25, 31, 28, 21, 18, 19};
            Double salario[] = {1500.50, 2100.60, 3000.00, 1200.00, 1500.00, 2000.00, 3100.33};
            StringBuffer buffer = null;

            int n = apellido.length;

            for (int i = 0; i < n; i++) {
                acceso.writeInt(i + 1);
                buffer = new StringBuffer(apellido[i]);
                buffer.setLength(8);
                acceso.writeChars(buffer.toString());
                acceso.write(edades[i]);
                acceso.writeDouble(salario[i]);
            
            }
            acceso.close();
        } catch (FileNotFoundException FNex) {
            Logger.getLogger(EscrituraYLecturaAleatoria.class.getName()).log(Level.SEVERE, null, FNex);
        } catch (IOException IOex) {
            Logger.getLogger(EscrituraYLecturaAleatoria.class.getName()).log(Level.SEVERE, null, IOex);
        }

    }

    public void LeerFicheroAleatorio() {

        try {
            File fichero = new File("ficheroAleatorio.dat");
            RandomAccessFile file = new RandomAccessFile(fichero, "r");

            int id, edad, posicion;
            double salario;
            char apellido[] = new char[8], aux;

            posicion = 0;

            while (file.getFilePointer() != file.length()) {
                file.seek(posicion);
                id = file.readInt();
                for (int i = 0; i < apellido.length; i++) {
                    aux = file.readChar();
                    apellido[i] = aux;
                }
                String apellidos = new String(apellido);
                edad = file.readInt();
                salario = file.readDouble();
                System.out.printf("ID: %s, Apellido: %s, Edad: %d, Salario: %.2f %n",
                        id, apellidos.trim(), edad, salario);

                posicion = posicion + 27;

            }
            file.close();
        } catch (EOFException EOFex) {
            System.err.println("Fin de la lectura");
            if (EOFex.getMessage() == null) {
                System.out.println("MensajeEOF: Se ha alcanzado el final del fichero ");
            } else {
                System.err.println("MensajeEOF: " + EOFex.getMessage());
            }
            System.out.println("======LOG======");
            Logger.getLogger(EscrituraYLecturaFicherosBinarios.class.getName()).log(Level.INFO, "Lectura correcta", EOFex);
        } catch (FileNotFoundException FNex) {
            Logger.getLogger(EscrituraYLecturaAleatoria.class.getName()).log(Level.SEVERE, null, FNex);
        } catch (IOException IOex) {
            Logger.getLogger(EscrituraYLecturaAleatoria.class.getName()).log(Level.SEVERE, null, IOex);
        }

    }

}
