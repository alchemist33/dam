package objects.utils;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;

/**
 *
 * @author marco
 */
public class CustomObjectOutPutStream extends ObjectOutputStream {

    public CustomObjectOutPutStream(OutputStream out) throws IOException {
        super(out);
    }

    protected CustomObjectOutPutStream() throws IOException, SecurityException {
        super();
    }

    @Override
    protected void writeStreamHeader() throws IOException {
    }
}
