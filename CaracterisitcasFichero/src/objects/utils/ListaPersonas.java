
package objects.utils;

import java.util.ArrayList;
import java.util.List;
import objects.Persona;

/**
 *
 * @author marco
 */
public class ListaPersonas {
private final List<Persona> lista = new ArrayList<>();
    public ListaPersonas() {
    }
    
    public void add(Persona per){
        lista.add(per);
    }
    
    public List<Persona> getListaPersonas(){
        return lista;
    }
    
    
}
