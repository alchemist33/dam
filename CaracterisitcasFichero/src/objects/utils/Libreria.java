package objects.utils;

import java.util.ArrayList;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import objects.Libro;

/**
 *
 * @author marco
 */
@XmlRootElement()
public class Libreria {

    private ArrayList<Libro> listaLibro;
    private String nombre;

    public Libreria() {
    }

    public void setListaLibro(ArrayList<Libro> listaLibro) {
        this.listaLibro = listaLibro;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Libreria(ArrayList<Libro> listaLibro, String nombre) {
        super();
        this.listaLibro = listaLibro;
        this.nombre = nombre;
    }

    @XmlElementWrapper(name="ListaLibros")
    @XmlElement(name="Libro")
    public ArrayList<Libro> getListaLibro() {
        return listaLibro;
    }

    public String getNombre() {
        return nombre;
    }

}
