/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ejerciciofeedback.BiblioMultimedia.gestorInformacion;


import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import com.ejerciciofeedback.BiblioMultimedia.base.objects.*;

/**
 * Clase destinada a la lecuta de datos.
 * @author marco
 */
public class FileReader {
    
    
    public void leerArchivo() throws IOException, ClassNotFoundException{
        
        ObjectInputStream ois = null;
        ois = new ObjectInputStream(new FileInputStream("data.dat"));
        
        CD_Fotos CdFotos = null;
        CD_Videos CdVideos = null;
        Disco disco = null;
        Pelicula pelicula = null;
        
        
        CdFotos = (CD_Fotos)ois.readObject();
        CdVideos = (CD_Videos)ois.readObject();
        pelicula = (Pelicula)ois.readObject();
        disco = (Disco)ois.readObject();
        
        
        System.out.println(CdFotos);
        System.out.println(CdVideos);
        System.out.println(pelicula);
        System.out.println(disco);
        
        
    }
}
