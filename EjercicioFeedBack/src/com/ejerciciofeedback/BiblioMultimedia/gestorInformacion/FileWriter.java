/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ejerciciofeedback.BiblioMultimedia.gestorInformacion;

import com.ejerciciofeedback.BiblioMultimedia.base.objects.*;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

/**
 * Clase destina a la escritura de datos
 * @author marco
 */
public class FileWriter {
       
    
    
    public void escribeArchivo() throws IOException{
        
       
        CD_Fotos CdFotos = new CD_Fotos("Galeria de Fotos", "Marco Memba",325.54,300);
        CD_Videos CdVideos = new CD_Videos("Galeria de Videos", "Marco memba", 548.25, 150);
        Pelicula peli = new Pelicula("Malditos Bastardos","Tarantino",125.59,"Brad Pitt");
        Disco disco = new Disco("Hybrid Theory","Linkin Park",120.30,"New Age");
        
        ObjectOutputStream oos = null;
        oos = new ObjectOutputStream(new FileOutputStream("data.dat"));
              
        oos.writeObject(CdFotos);
        oos.writeObject(CdVideos);
        oos.writeObject(peli);
        oos.writeObject(disco);
        
       
    }
    public void salidaConsola() {
        
        CD_Fotos CdFotos = new CD_Fotos("Galeria de Fotos", "Marco Memba",325.54,300);
        CD_Videos CdVideos = new CD_Videos("Galeria de Videos", "Marco memba", 548.25, 150);
        Pelicula peli = new Pelicula("Malditos Bastardos","Tarantino",125.59,"Brad Pitt");
        Disco disco = new Disco("Hybrid Theory","Linkin Park",120.30,"New Age");
        
        System.out.print(CdFotos);
        System.out.print(CdVideos);
        System.out.print(peli);
        System.out.print(disco);
    }
}
