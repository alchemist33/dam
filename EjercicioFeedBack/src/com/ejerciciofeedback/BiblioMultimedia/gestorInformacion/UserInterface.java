/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ejerciciofeedback.BiblioMultimedia.gestorInformacion;

import java.io.IOException;


/**
 * Interfaz de Usuario que se presenta como modo para seleccionar las opciones disponibles.
 * @author marco
 */
public class UserInterface {
    
      
    public void entrada() throws IOException, ClassNotFoundException{
     

        FileWriter Fw = new FileWriter();
        FileReader Fr = new FileReader();
        System.out.println("---------------------------------------------------"); 
        System.out.println("Bienvenido a la Biblioteca, selecciona una opción:"); 
        System.out.println("1: Ver elementos de la biblioteca");
        System.out.println("2: Escribir elementos de la biblioteca en archivo data.dat en modo binario");
        System.out.println("3: Leer el archivo data.dat");
        System.out.println("0: Salir");System.out.println("---------------------------------------------------"); 
        System.out.print("Tu selección----->>> ");
        int opcion = System.in.read();

        switch(opcion){
            case '1':  
                System.out.println("Cargando en pantalla los volaroes iniciales......"); 
                Fw.salidaConsola();
                System.out.println("---------------Fin de instruccion------------------------------------"); 
                //entrada();
                break;
            
            case '2':
                Fw.escribeArchivo();
                System.out.println("El archivo data.dat se escribió correctamente");
                System.out.println("---------------Fin de instruccion------------------------------------"); 
                //entrada();
                break;
            
            case '3': System.out.println("Leyendo los datos del fichero \"data.dat\"......");
                Fr.leerArchivo();
                System.out.println("---------------Fin de instruccion------------------------------------"); 
                //entrada();
                break;
            case '0':
                System.out.println("---------------Fin del programa------------------------------------"); 
                break;
        }
         
        
    }
    
}
