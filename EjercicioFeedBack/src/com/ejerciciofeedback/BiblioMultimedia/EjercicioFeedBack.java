/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ejerciciofeedback.BiblioMultimedia;
import com.ejerciciofeedback.BiblioMultimedia.base.BiblioStart;
import com.ejerciciofeedback.BiblioMultimedia.gestorInformacion.UserInterface;
import java.io.IOException;

/**
 * Se iniciliza la Biblioteca dandole unos valores con el inicio del programa. 
 * Se cargan los datos pero no se escribe en el fichero en el momento de inicio.
 * La opción 1 lee los objetos almacenados en la memoria de carga del programa.
 * La opcion 2 escribe en binario en el fichero, se consigue serializando los objetos (el fichero se envia vacio).
 * La opción 3 por medio de la serialización transforma el fichero binario y lo presenta en formato legible por consola.
 * @author marco
 */
public class EjercicioFeedBack {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        // TODO code application logic here
        
        BiblioStart st = new BiblioStart();
        UserInterface ui = new UserInterface();
              
        ui.entrada();
        
       
    }

    
    
}
