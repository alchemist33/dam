/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ejerciciofeedback.BiblioMultimedia.base;
import com.ejerciciofeedback.BiblioMultimedia.base.objects.*;

/**
 * Se produce en el inicio del programa y dota al programa de objetos de tipo propio.
 * @author marco
 */
public class BiblioStart {
    
    public BiblioStart(){
        Estanteria_Multimedia estanteria = new Estanteria_Multimedia(4);
        
        CD_Fotos CdFotos = new CD_Fotos("Galeria de Fotos", "Marco Memba",325.54,300);
        CD_Videos CdVideos = new CD_Videos("Galeria de Videos", "Marco memba", 548.25, 150);
        Pelicula peli = new Pelicula("Malditos Bastardos","Tarantino",125.59,"Brad Pitt");
        Disco disco = new Disco("Hybrid Theory","Linkin Park",120.30,"New Age");
          
        estanteria.add(CdFotos);
        estanteria.add(CdVideos);
        estanteria.add(peli);
        estanteria.add(disco);
                
    }
}
