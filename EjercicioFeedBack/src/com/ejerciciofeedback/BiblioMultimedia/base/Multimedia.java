/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ejerciciofeedback.BiblioMultimedia.base;

import java.io.Serializable;

/**
 * Se implementa la clase Serializable en la clase base, para que por medio de la herencia, 
 * la capacidad de serialización se extendienda a las clases hijas, de este modo no es necesario 
 * implementar la serialización en cada objeto.
 * @author marco
 */
public class Multimedia implements Serializable{
    
    public String titulo;
    public String autor;
    public double duracion;
    
    public Multimedia(String titulo, String autor, double duracion){ //se inicializan los objetos
        
        this.titulo = titulo;
        this.autor = autor;
        this.duracion = duracion;
    }
            
    
    public String getTitulo(){
        return titulo;
    }
    public void setTitulo(String titulo){
        this.titulo = titulo;
    } 
    public String getAutor(){
        return autor;
    }
    public void setAutor(String autor){
        this.autor = autor;
    } 
    public double getDuracion(){
        return duracion;
    }
    public void setDuracion(double duracion){
        this.duracion = duracion;
    }
    
    @Override
   public String toString(){
        return "\nTitulo: " + getTitulo() + "\nAutor: " + getAutor() + "\nDuracion: " + getDuracion()+ " minutos";
    }
   
   public Boolean equals(Multimedia multimedia){
       return titulo.equals(multimedia.getTitulo()) && autor.equals(multimedia.getAutor());
   }
   
}

