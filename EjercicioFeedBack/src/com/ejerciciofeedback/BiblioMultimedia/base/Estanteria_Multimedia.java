/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ejerciciofeedback.BiblioMultimedia.base;

/**
 * Clase contenedora de objetos de tipo propio
 * @author marco
 */
public class Estanteria_Multimedia {
    
    Multimedia[] listaMultimedia;
    int contador;
    
    public Estanteria_Multimedia(int tamañoMaximo){
        
        listaMultimedia = new Multimedia[tamañoMaximo];
        contador = 0;
    }
    
    public int size(){
        
        return contador;
    }
    public boolean llena(){
         return contador == listaMultimedia.length;
    }
    public boolean add(Multimedia m){
       
         if(llena()){
             return false;
         }else{
             listaMultimedia[contador] = m;
             contador++;
             return true;
         }
    }
    public Multimedia get(int posicion){
        return listaMultimedia[posicion];
    }
    
    public int indexOf(Multimedia m){
        for(int i =0; i < contador; i++){
            if(m.equals(listaMultimedia[i])){
            return i;
        }
        }
      
        return -1;
        
    }
    
    @Override
   public String toString(){
        return "Los objetos son:" + listaMultimedia[contador].toString();
    }
}
