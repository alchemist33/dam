/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ejerciciofeedback.BiblioMultimedia.base.objects;

import com.ejerciciofeedback.BiblioMultimedia.base.Multimedia;

/**
 *
 * @author marco
 */
public class CD_Videos extends Multimedia {
    
    public int NumVideos;
    
    public CD_Videos(String titulo, String autor,double duracion, int numVideos){
        super(titulo,autor,duracion);
        this.NumVideos = numVideos;
    }
    
    public Integer getNumVideos(){
        return NumVideos;
    }
    public void setNumVideos(int numVideos){
        this.NumVideos = numVideos;
    }
    
    @Override
    public String toString(){
        return super.toString() + "\nNumero de Videos: " + getNumVideos()+"\n";
    }
}
