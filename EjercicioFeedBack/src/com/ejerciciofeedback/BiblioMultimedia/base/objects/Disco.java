/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ejerciciofeedback.BiblioMultimedia.base.objects;
import com.ejerciciofeedback.BiblioMultimedia.base.Multimedia;



/**
 *
 * @author marco
 */
public class Disco extends Multimedia {
    
    public String genero;
    
    
    public Disco (String titulo, String autor, double duracion, String genero){
        super(titulo,autor,duracion);
        this.genero = genero;
    }
    
    public String getGenero(){
        return genero;
    }
    public void setGenero(String genero){
        this.genero = genero;
    }
    
    @Override
    public String toString(){
        return super.toString() + "\nGenero: " + getGenero()+"\n";
    }
}
