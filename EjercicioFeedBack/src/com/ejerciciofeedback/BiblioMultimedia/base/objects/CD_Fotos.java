/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ejerciciofeedback.BiblioMultimedia.base.objects;

import com.ejerciciofeedback.BiblioMultimedia.base.Multimedia;

/**
 *
 * @author marco
 */
public class CD_Fotos extends Multimedia{
    
    public int numFotos;
    
    public CD_Fotos(String titulo, String autor, double duracion,int numFotos){
       super(titulo,autor,duracion);
       this.numFotos = numFotos;
    }
    
    public Integer getNumFotos(){
        return numFotos;
    }
    public void setNumfotos(int NumFotos){
        this.numFotos = NumFotos;
    }
            
    @Override
    public String toString(){
        return super.toString() + "\nNumero de fotos: " + getNumFotos()+"\n";
    }
}
