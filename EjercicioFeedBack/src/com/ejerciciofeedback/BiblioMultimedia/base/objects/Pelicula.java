/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ejerciciofeedback.BiblioMultimedia.base.objects;

import com.ejerciciofeedback.BiblioMultimedia.base.Multimedia;



/**
 *
 * @author marco
 */
public class Pelicula extends Multimedia{
       
    public String actorPrincipal;
       
       
       public Pelicula(String titulo, String autor, double duracion, String AcPrincipal){
           super(titulo,autor,duracion);
           this.actorPrincipal = AcPrincipal;
       }
       
       public String getActorPrincipal(){
          return actorPrincipal;
       }
       public void setActorPrincipal(String actorPrincipal){
           this.actorPrincipal = actorPrincipal;
       }
       
       @Override
       public String toString(){
           String actores = "\nProtagonizado por: ";
                   if(actorPrincipal != null){
                       actores += getActorPrincipal();
                   }
          return super.toString() + actores+"\n";
       }
}
