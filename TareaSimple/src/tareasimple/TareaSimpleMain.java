/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tareasimple;

/**
 *
 * @author marco
 */
public class TareaSimpleMain {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        Thread t1 = new Thread(new TareaSimple("soy A"));
        Thread t2 = new Thread(new TareaSimple("soy B"));
        Thread t3 = new Thread(new TareaSimple("soy C"));
        Thread t4 = new Thread(new TareaSimple("soy D"));
        Thread t5 = new Thread(new TareaSimple("soy E"));
        t1.start();
        t1.interrupt();
        t2.start();
        t3.start();
        t4.start();
        t5.start();
        
        System.out.println("Fin programa principal");
    }
    
}
