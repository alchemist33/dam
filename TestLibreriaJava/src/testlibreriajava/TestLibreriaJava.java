/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testlibreriajava;

import beans.*;

/**
 *
 * @author marco
 */
public class TestLibreriaJava {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
      
        Producto producto = new Producto(1,"Melocotones", 10, 3, 2);
        
        Pedido pedido = new Pedido();
        pedido.setProducto(producto);
        producto.addPropertyChangeListener(pedido);
        producto.setStockactual(2);
        
    }
    
}
