
import java.util.logging.Level;
import java.util.logging.Logger;
import ops.HiloPrimario;
import ops.ExecPrimaria;

/**
 *
 * @author marco
 */
public class Entrada {

    public static void main(String[] args) {
        HiloPrimario hp = new HiloPrimario(10);
        HiloPrimario hp2 = new HiloPrimario();

        ExecPrimaria exp = new ExecPrimaria(100);

        Thread hilo2 = new Thread(new ExecPrimaria());
        try {
            Thread.sleep(100);
        } catch (InterruptedException ex) {
            Logger.getLogger(Entrada.class.getName()).log(Level.SEVERE, null, ex);
        }
        hp.start();
        hp2.start();

        exp.run();
        hilo2.start();
        //System.out.println("Hilo");
    }
}
