/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arrays;

/**
 *
 * @author marco
 */
public class Arrays {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        int[] numeros = new int[10];
         int[] numeros2 = new int[10];
        for (int i = 0; i < 10; i++){
            numeros[i] = i;
            numeros2[i] = i;
            System.out.println(numeros[i]);
            
        }
        System.out.println("La longitud del arrays es: "+numeros.length);
        //comparaciones de tipo de objeto, no de los valores alojados dentro del array
        System.out.println("Los arrays n y n2 son iguales: "+ numeros.equals(numeros2));
        System.out.println("Los arrays n y n son iguales: "+ numeros.equals(numeros));
        //Metodo toString, escribe la direccion de memeoria del objeto
        System.out.println("Metodo toString n y n2 \nn:"+numeros.toString()+"\nn2:"+ numeros2.toString());
        
        //Clase estatica java.util.Arrays
        
        //forma de comparar valores alojados dentro del array
        System.out.println("Los arrays n y n2 son iguales: "+ java.util.Arrays.equals(numeros, numeros2));
        //Imprimir valores alojados dentro del array
        System.out.println("Metodo toString n y n2 \nn:"+java.util.Arrays.toString(numeros)+"\nn2:"+ java.util.Arrays.toString(numeros2));
        
    }
    
}
